package com.schoolreviewer.exam;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.schoolreviewer.BaseActivity;
import com.schoolreviewer.R;

public class VideoPlayActivity extends BaseActivity implements View.OnClickListener {

    private VideoView videoView;
    private MediaController mediaController;
    private TextView textDone;
    private ProgressDialog pdWaiting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videoplay);

        textDone = (TextView) findViewById(R.id.textDone);
        textDone.setOnClickListener(this);

        showProgress();
        videoView = (VideoView) findViewById(R.id.videoCityGuide);
        mediaController = new MediaController(this);
        videoView.setVideoURI(Uri.parse(getIntent().getStringExtra("videourl")));
        mediaController = new MediaController(VideoPlayActivity.this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.start();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                hideProgress();
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                finish();
                return false;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    @Override
    public void onClick(View v) {
        if (v.equals(textDone)) {
            finish();
        }
    }

    public void hideProgress() {
        if (pdWaiting.isShowing())
            pdWaiting.dismiss();
    }

    public void showProgress() {
        pdWaiting = new ProgressDialog(this);
        pdWaiting.setMessage("Please wait...");
        pdWaiting.setCancelable(true);
        pdWaiting.setCanceledOnTouchOutside(false);
        if (!pdWaiting.isShowing())
            pdWaiting.show();
    }
}