package com.schoolreviewer.exam;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExamFragment extends BaseFragment {

    private TabLayout tabs;
    private ViewPager viewpager;
    private ViewPagerAdapter viewPagerAdapter;
    private TextView textHeaderTitle;
    private HashMap<String, String> stringHashMap;
    private List<String> listLevel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return View.inflate(getActivity(), R.layout.fragment_exam, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initControl(view);
    }

    private void initControl(View view) {
        listLevel = new ArrayList<>();

        textHeaderTitle = (TextView) getActivity().findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(R.string.lbl_exam_paper);

        tabs = (TabLayout) view.findViewById(R.id.tabs);
        viewpager = (ViewPager) view.findViewById(R.id.viewpager);

        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(), listLevel);
        viewpager.setAdapter(viewPagerAdapter);

        tabs.setupWithViewPager(viewpager);

        if (utility.checkInternetConnection())
            getAllLevel();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void getAllLevel() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("country", Constant.COUNTRY);

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 46) {
                        JSONArray arrLevels = jsonObject.getJSONObject("message").getJSONArray("levels");
                        for (int i = 0; i < arrLevels.length(); i++) {
                            listLevel.add(arrLevels.get(i).toString());
                        }
                        viewPagerAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_SEARCH_EXAMS);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private List<String> listLevel;

        public ViewPagerAdapter(FragmentManager fm, List<String> listLevel) {
            super(fm);
            this.listLevel = listLevel;
        }

        @Override
        public Fragment getItem(int position) {
            return ExamPaperListFragment.newInstance(position, listLevel.get(position));
        }

        @Override
        public int getCount() {
            return listLevel.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return listLevel.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }
    }
}
