package com.schoolreviewer.exam;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.schoolreviewer.BaseActivity;
import com.schoolreviewer.R;
import com.schoolreviewer.adapter.ExamQuestionAdapter;
import com.schoolreviewer.bean.ExamQuestionBean;
import com.schoolreviewer.bean.ItemBean;
import com.schoolreviewer.paypal.PayPalIntegrationActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExamPaperActivity extends BaseActivity implements View.OnClickListener {

    private TextView textHeaderTitle, textLevel, textExamPrice, textExamTitle, textExamDesc;
    private ListView lvQuestion;
    private Button btnTakeCourse;
    private ExamQuestionAdapter examQuestionAdapter;
    private LinearLayout llTakeCourse;
    private String examUid;
    private HashMap<String, String> stringHashMap;
    private List<ExamQuestionBean> listExamQuestion;
    private ItemBean itemBean;
    private String price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_paper);
        initControl();
    }

    private void initControl() {
        itemBean = new ItemBean();
        listExamQuestion = new ArrayList<>();

        examUid = getIntent().getStringExtra("examuid");
        itemBean.setUid(examUid);

        textHeaderTitle = (TextView) findViewById(R.id.textHeaderTitle);
        textLevel = (TextView) findViewById(R.id.textLevel);
        textExamPrice = (TextView) findViewById(R.id.textExamPrice);
        textExamTitle = (TextView) findViewById(R.id.textExamTitle);
        textExamDesc = (TextView) findViewById(R.id.textExamDesc);

        textHeaderTitle.setText(R.string.lbl_exam_paper);

        btnTakeCourse = (Button) findViewById(R.id.btnTakeCourse);
        btnTakeCourse.setOnClickListener(this);

        llTakeCourse = (LinearLayout) findViewById(R.id.llTakeCourse);

        lvQuestion = (ListView) findViewById(R.id.lvQuestion);
        lvQuestion.setFocusable(false);

        lvQuestion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (listExamQuestion.get(position).getMustbuy().equals("1")) {
                    utility.showMessageDialog("Please buy this exam to watch our videos");
                } else {
                    if (utility.checkInternetConnection()) {
                        checkUserKeyAsyncCall(new onTaskComplete() {
                            @Override
                            public void onComplete(String response) {
                                stringHashMap.clear();

                                stringHashMap.put("key", sharedPreferences.getString("key", ""));
                                stringHashMap.put("country", Constant.COUNTRY);
                                stringHashMap.put("uidquestion", listExamQuestion.get(position).getUid());
                                stringHashMap.put("uidexam", listExamQuestion.get(position).getExamUid());

                                new AllAPICall(ExamPaperActivity.this, stringHashMap, null, new onTaskComplete() {
                                    @Override
                                    public void onComplete(String response) {
                                        startActivity(new Intent(ExamPaperActivity.this, VideoPlayActivity.class).putExtra("videourl", listExamQuestion.get(position).getVimeoid()));
                                    }
                                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_UPDATE_EXAM_WATCHED);
                            }
                        });
                    }
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (utility.checkInternetConnection()) {
            listExamQuestion.clear();
            getAllExamDetailsApiCall();
        }
    }

    private void getAllExamDetailsApiCall() {
        stringHashMap = new HashMap<>();

        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("uid", examUid);

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 49) {
                        JSONObject objMessage = jsonObject.getJSONObject("message");
                        itemBean.setTitle(objMessage.getString("name"));
                        textExamTitle.setText(objMessage.getString("name"));
                        textLevel.setText(objMessage.getString("level"));
                        itemBean.setPrice(objMessage.getString("price"));

                        price = objMessage.getString("price");

                        textExamPrice.setText(Constant.POUND + price);
                        if (price.equals("0.00")) {
                            textExamPrice.setText("FREE");
                            btnTakeCourse.setText("START THIS COURSE");
                        }
//                        textExamDesc.setText(objMessage.getString("description").replace(", ", "\n"));
                        textExamDesc.setText(Html.fromHtml(objMessage.getString("description")));

                        // 1 = Purchased && 0 = not purchased

                        if (objMessage.getInt("hasbought") == 1 && !price.equals("0.00")) {
                            llTakeCourse.setVisibility(View.INVISIBLE);
                        } else {
                            llTakeCourse.setVisibility(View.VISIBLE);
                        }

                        JSONArray arrayQuestion = objMessage.getJSONArray("questions");
                        for (int i = 0; i < arrayQuestion.length(); i++) {
                            JSONObject objExam = arrayQuestion.getJSONObject(i);

                            ExamQuestionBean examQuestionBean = new ExamQuestionBean();
                            examQuestionBean.setExamUid(objMessage.getString("uid"));
                            examQuestionBean.setUid(objExam.getString("uid"));
                            examQuestionBean.setName(objExam.getString("name"));
                            examQuestionBean.setSummary(objExam.getString("summary"));
                            examQuestionBean.setCando(objExam.getString("cando"));
                            examQuestionBean.setMustbuy(objExam.getString("mustbuy"));
                            examQuestionBean.setVimeoid(objExam.getString("vimeoid"));
                            examQuestionBean.setHaswatched(objExam.getString("haswatched"));

//                            if (objExam.getString("mustbuy").equals("0"))
                            listExamQuestion.add(examQuestionBean);
                        }
                        examQuestionAdapter = new ExamQuestionAdapter(ExamPaperActivity.this, listExamQuestion, sharedPreferences.getString("key", ""));
                        lvQuestion.setAdapter(examQuestionAdapter);
                        examQuestionAdapter.notifyDataSetChanged();
                        setListViewHeightBasedOnChildren(lvQuestion);
                    } else {
                        Toast.makeText(ExamPaperActivity.this, "Exam not found", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_GET_EXAM);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnTakeCourse)) {
            checkUserKeyAsyncCall(new onTaskComplete() {
                @Override
                public void onComplete(String response) {
                    if (response.equals("yes")) {
                        if (price.equals("0.00")) {
                            if (utility.checkInternetConnection()) {
                                stringHashMap.clear();

                                stringHashMap.put("key", sharedPreferences.getString("key", ""));
                                stringHashMap.put("country", Constant.COUNTRY);
                                stringHashMap.put("uidquestion", listExamQuestion.get(0).getUid());
                                stringHashMap.put("uidexam", listExamQuestion.get(0).getExamUid());

                                new AllAPICall(ExamPaperActivity.this, stringHashMap, null, new onTaskComplete() {
                                    @Override
                                    public void onComplete(String response) {
                                        startActivity(new Intent(ExamPaperActivity.this, VideoPlayActivity.class).putExtra("videourl", listExamQuestion.get(0).getVimeoid()));
                                    }
                                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_UPDATE_EXAM_WATCHED);
                            }
                        } else {
                            startActivity(new Intent(ExamPaperActivity.this, PayPalIntegrationActivity.class).putExtra("item", itemBean));
                        }
                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
//            if (sharedPreferences.contains("key")) {
//                startActivity(new Intent(ExamPaperActivity.this, PayPalIntegrationActivity.class).putExtra("item", itemBean));
//            }
//        }
    }

    private void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.setLayoutParams(new ViewGroup.LayoutParams(0, 0));
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + 10;
        listView.setLayoutParams(params);
    }
}
