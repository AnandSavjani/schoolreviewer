package com.schoolreviewer.exam;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.schoolreviewer.R;
import com.schoolreviewer.adapter.ExamAdapter;
import com.schoolreviewer.bean.ExamBean;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.MyUtility;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExamPaperListFragment extends Fragment {

    int position;
    private GridView gvExamPapers;
    private ExamAdapter examAdapter;
    private HashMap<String, String> stringHashMap;
    private MyUtility myUtility;
    private List<ExamBean> listExams;
    private String level;
    private TextView textNoExam;

    static public ExamPaperListFragment newInstance(int position, String level) {
        ExamPaperListFragment fragment = new ExamPaperListFragment();
        Bundle b = new Bundle();
        b.putInt("pos", position);
        b.putString("level", level);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("pos");
        level = getArguments().getString("level");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return View.inflate(getActivity(), R.layout.fragment_exam_papers_list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initControl(view);
    }

    private void initControl(View view) {
        myUtility = new MyUtility(getActivity());
        listExams = new ArrayList<>();

        textNoExam = (TextView) view.findViewById(R.id.textNoExam);

        gvExamPapers = (GridView) view.findViewById(R.id.gvExamPapers);
        examAdapter = new ExamAdapter(getActivity(), listExams);

        gvExamPapers.setAdapter(examAdapter);

        if (myUtility.checkInternetConnection())
            getExamLevels();
    }

    private void getExamLevels() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("level", level);

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 47) {
                        JSONObject objMessage = jsonObject.getJSONObject("message");
                        JSONArray arrayResult = objMessage.getJSONArray("results");
                        for (int i = 0; i < arrayResult.length(); i++) {
                            JSONObject objResult = arrayResult.getJSONObject(i);
                            ExamBean examBean = new ExamBean();

                            examBean.setUid(objResult.getString("uid"));
                            examBean.setName(objResult.getString("name"));
                            examBean.setLevel(objResult.getString("level"));
                            examBean.setSubject(objResult.getString("subject"));
                            examBean.setDescription(objResult.getString("description"));
                            examBean.setCertification(objResult.getString("certification"));
                            examBean.setPrice(objResult.getString("price"));
                            examBean.setNumberpapers(objResult.getString("numberpapers"));
                            examBean.setTimetaken(objResult.getString("timetaken"));
                            examBean.setLanguage(objResult.getString("language"));
                            examBean.setYear(objResult.getString("year"));
                            examBean.setTutorname(objResult.getString("tutorname"));
                            examBean.setUidexamimage(objResult.getString("uidexamimage"));
                            examBean.setUrlexamimage(objResult.getString("urlexamimage"));
                            examBean.setUidtutorimage(objResult.getString("uidtutorimage"));
                            examBean.setUrltutorimage(objResult.getString("urltutorimage"));

                            listExams.add(examBean);
                        }
                        if (arrayResult.length() > 0) {
                            textNoExam.setVisibility(View.GONE);
                            gvExamPapers.setVisibility(View.VISIBLE);
                        } else {
                            textNoExam.setVisibility(View.VISIBLE);
                            gvExamPapers.setVisibility(View.GONE);
                        }
                    }
                    examAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_SEARCH_EXAMS);
    }
}
