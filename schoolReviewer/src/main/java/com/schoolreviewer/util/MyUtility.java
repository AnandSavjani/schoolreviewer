package com.schoolreviewer.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.schoolreviewer.MainActivity;
import com.schoolreviewer.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyUtility {
    private Context context;
    private ProgressDialog pdWaiting;
    SharedPreferences preferenceRemember;

    public MyUtility(Context context) {
        this.context = context;
    }

    public void rememberMe(Context mContext, String username, String password) {
        preferenceRemember = mContext.getSharedPreferences(Constant.REMEMBER_PREFERENCE, Context.MODE_PRIVATE);
        Editor rememberEditor = preferenceRemember.edit();
        rememberEditor.clear();
        rememberEditor.putString("name", username);
        rememberEditor.putString("password", password);
        rememberEditor.commit();
    }

    public String getTimeStamp() {
        return (DateFormat.format("ddMMyyyy_hhmmss", new java.util.Date()).toString());
    }

    public void removeRememberMe() {
        preferenceRemember = context.getSharedPreferences(Constant.REMEMBER_PREFERENCE, Context.MODE_PRIVATE);
        preferenceRemember.edit().clear().commit();
    }

    public void hideSoftKeyboard() {
        if (context == null)
            return;
        Activity activity = (Activity) context;
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (StackOverflowError | Exception e) {
            e.printStackTrace();
        }
    }

    public String parseDate(String strDate) {
        Date mDate = null;
        SimpleDateFormat serverFormat = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            mDate = format.parse(strDate);
            serverFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert serverFormat != null;
        return serverFormat.format(mDate);
    }

    public String getIMEI() {
        TelephonyManager TelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return TelephonyMgr.getDeviceId();
    }

    public boolean checkInternetConnection() {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected())
            return true;
        else {
            Toast.makeText(context, context.getResources().getString(R.string.msg_internet_connection), Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public String dateDifference(String askedTime, String currentTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date askedAt, current;
        long different = 0;
        try {
            askedAt = simpleDateFormat.parse(askedTime);
            current = simpleDateFormat.parse(currentTime);
            different = current.getTime() - askedAt.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;

        if (elapsedDays == 0) {
            return "" + elapsedHours + " Hours Ago";
        }
        return "" + elapsedDays + " Days Ago";
    }


    public void setAlertMessage(String msg) {
        if (context == null)
            return;

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(context.getResources().getString(R.string.app_name));
        dialog.setMessage(msg);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        if (context != null)
            dialog.show();
    }

    public int measurePasswordStrength(String pass) {
        final int DIALOG_LOW = 0;
        final int DIALOG_MID = 1;
        final int DIALOG_HIGH = 2;

        int up = 0, low = 0, no = 0, spl = 0, xtra = 0, len = 0, points = 0, max = 8;
        char c;
        int valueOfProgress = 0;
        len = pass.length();

        if (len <= 6)
            points++;
        else if (len <= 10)
            points += 2;
        else
            points += 3;
        for (int i = 0; i < len; i++) {
            c = pass.charAt(i);
            if (c >= 'a' && c <= 'z') {
                if (low == 0)
                    points++;
                low = 1;
            } else {
                if (c >= 'A' && c <= 'Z') {
                    if (up == 0)
                        points++;
                    up = 1;
                } else {
                    if (c >= '0' && c <= '9') {
                        if (no == 0)
                            points++;
                        no = 1;
                    } else {
                        if (c == '_' || c == '@') {
                            if (spl == 0)
                                points += 1;
                            spl = 1;
                        } else {
                            if (xtra == 0)
                                points += 2;
                            xtra = 1;

                        }
                    }
                }
            }
        }
        if (points <= 3) {
            valueOfProgress = DIALOG_LOW;
        } else if (points <= 6) {
            valueOfProgress = DIALOG_MID;
        } else if (points <= 8) {
            valueOfProgress = DIALOG_HIGH;
        }

        return valueOfProgress;
    }

    public void showProgress() {
        pdWaiting = new ProgressDialog(context);
        pdWaiting.setMessage("Please wait...");
        pdWaiting.setCancelable(false);
        pdWaiting.setCanceledOnTouchOutside(false);
        if (!pdWaiting.isShowing())
            pdWaiting.show();
    }

    public void hideProgress() {
        if (pdWaiting.isShowing())
            pdWaiting.dismiss();
    }

    public boolean checkPlayServices() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (status != ConnectionResult.SUCCESS) {
            if (!GooglePlayServicesUtil.isUserRecoverableError(status)) {
                Toast.makeText(context, "This device is not supported.", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return true;
    }

    public String checkDisplayName(String displayname) {
        if (displayname.equalsIgnoreCase("anon"))
            displayname = "Annonymous";
        return displayname;
    }

    public Bitmap rotateImages(String imagePath) {
        ExifInterface ei;
        Bitmap bitmap;
        if (imagePath == null) {
            Toast.makeText(context, "Unable to fetch image.Please try another image.", Toast.LENGTH_LONG).show();
        }
        try {
            ei = new ExifInterface(imagePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            bitmap = setImageToImageView(imagePath);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return RotateBitmap(bitmap, 90);
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return RotateBitmap(bitmap, 180);
                default:
                    return bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    private Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public Bitmap setImageToImageView(String filePath) {
        // Decode image size
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = options.outWidth, height_tmp = options.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeFile(filePath, o2);
    }

    public File resize(String path) {
        try {
            File file = new File(path);
            file.getParentFile().createNewFile();
            Bitmap bitmapOriginal = rotateImages(path);
            OutputStream outStream = new FileOutputStream(file);
            bitmapOriginal.compress(Bitmap.CompressFormat.JPEG, 60, outStream);
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean validateEmail(View edtx) {

        String regExpn = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
        Pattern patternObj = Pattern.compile(regExpn);

        Matcher matcherObj = patternObj.matcher((((EditText) edtx).getText().toString()).trim());
        return matcherObj.matches();
    }

    public void showMessageDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public boolean validateEmail(View edtx, String errorMessage) {

        String regExpn = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
        Pattern patternObj = Pattern.compile(regExpn);

        Matcher matcherObj = patternObj.matcher((((EditText) edtx).getText().toString()).trim());
        if (matcherObj.matches())
            return true;
        else {
            showMessageDialog(errorMessage);
            return false;
        }
    }

    public boolean validateText(View comp, String errorMessage) {

        boolean flag = false;
        try {
            if (comp instanceof EditText) {
                final EditText box = (EditText) comp;
                if (box.getText() != null && box.getText().toString().trim().length() > 0) {
                    flag = true;
                } else {
                    showMessageDialog(errorMessage);
                    box.requestFocus();
                }
            } else if (comp instanceof SeekBar) {
                final SeekBar box = (SeekBar) comp;
                if (box.getProgress() != 0)
                    flag = true;
                else
                    showMessageDialog(errorMessage);
            } else if (comp instanceof RatingBar) {
                final RatingBar box = (RatingBar) comp;
                if (box.getRating() != 0)
                    flag = true;
                else
                    showMessageDialog(errorMessage);
            } else if (comp instanceof Spinner) {
                final Spinner box = (Spinner) comp;
                if (box.getSelectedItemId() != box.getCount() && box.getSelectedItem().toString().trim().length() > 0)
                    flag = true;
                else
                    showMessageDialog(errorMessage);
            } else if (comp == null)
                showMessageDialog(errorMessage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return flag;
    }

    public boolean validName(View comp, String errorMessage) {
        String regExpn = "^[a-zA-Z]+$";
        Pattern patternObj = Pattern.compile(regExpn);

        Matcher matcherObj = patternObj.matcher(((EditText) comp).getText().toString().trim());
        if (matcherObj.matches())
            return true;
        else {
            showMessageDialog(errorMessage);
            return false;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void sendNotification(int code, String msg) {
        NotificationManager notificationManager;
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentTitle(context.getString(R.string.app_name));
        builder.setContentText(msg);
        Intent resultIntent = new Intent(context, MainActivity.class);
//        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
//        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.putExtra("msg", msg);
        PendingIntent pending = PendingIntent.getActivity(context.getApplicationContext(), 0, resultIntent, 0);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(R.drawable.app_icon);
            builder.setColor(context.getResources().getColor(R.color.color_header));
        } else
            builder.setSmallIcon(R.drawable.ic_launcher);

        builder.setAutoCancel(true);
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        builder.build().flags |= Notification.FLAG_AUTO_CANCEL;
        builder.setAutoCancel(true);
        builder.setStyle(new Notification.BigTextStyle().bigText(msg));
//        builder.setContentIntent(pending);
        Notification notification = builder.build();
        notificationManager.notify(code, notification);
    }

}
