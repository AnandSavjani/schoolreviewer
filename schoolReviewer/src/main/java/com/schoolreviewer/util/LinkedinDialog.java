package com.schoolreviewer.util;

import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.schoolreviewer.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LinkedinDialog extends BaseActivity {

    private HashMap<String, String> stringHashMap;
    private LISessionManager liSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name)";

        final APIHelper apiHelper = APIHelper.getInstance(LinkedinDialog.this);
        liSessionManager = LISessionManager.getInstance(this);
        liSessionManager.init(this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                apiHelper.getRequest(LinkedinDialog.this, url, new ApiListener() {
                    @Override
                    public void onApiSuccess(ApiResponse apiResponse) {
                        LoginAsyncCall(apiResponse.getResponseDataAsString());
                    }

                    @Override
                    public void onApiError(LIApiError liApiError) {
                        Log.i("linked in error", liApiError.toString());
                    }
                });
            }

            @Override
            public void onAuthError(LIAuthError error) {
                Log.i("linked in error", error.toString());
            }
        }, true);
    }

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        liSessionManager.onActivityResult(this, requestCode, resultCode, data);
        Log.i("result code", "" + resultCode);
    }

    private void LoginAsyncCall(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            stringHashMap = new HashMap<>();
            stringHashMap.put("socialid", jsonObject.getString("id"));
            stringHashMap.put("socialnetwork", "linkedin");
            stringHashMap.put("socialdisplayname", jsonObject.getString("firstName") + " " + jsonObject.getString("lastName"));
            stringHashMap.put("platform", "android");
            stringHashMap.put("devicetoken", sharedPreferences.getString("regid", ""));

        } catch (Exception e) {
            e.printStackTrace();
        }

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjLogin = jsonArray.getJSONObject(0);
                    if (jObjLogin.has("key")) {
                        if (jObjLogin.getInt("code") == 1) {
                            Editor mEditor = sharedPreferences.edit();
                            mEditor.putString("key", jObjLogin.getString("key"));
                            mEditor.putString("uiduser", jObjLogin.getString("uiduser"));
                            mEditor.putString("social", "yes");
                            mEditor.apply();
                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_LOGIN);
    }
}