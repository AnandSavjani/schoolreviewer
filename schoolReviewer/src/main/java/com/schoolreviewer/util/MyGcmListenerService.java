package com.schoolreviewer.util;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

public class MyGcmListenerService extends GcmListenerService {

    private MyUtility utility;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);
        utility = new MyUtility(this);
        try {
            String msg = data.toString();
            Log.i("msg", msg);
//            JSONObject jsonObject = new JSONObject(msg);
//            utility.sendNotification(jsonObject.getInt("badge"), jsonObject.getString("alert"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
