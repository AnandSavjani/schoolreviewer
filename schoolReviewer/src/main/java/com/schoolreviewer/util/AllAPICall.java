package com.schoolreviewer.util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.util.HashMap;

public class AllAPICall extends AsyncTask<String, String, String> {

    onTaskComplete mOnTaskComplete;
    GetJsonUrl jsonParser;
    Context context;
    MyUtility utility;
    HashMap<String, String> pairListString;
    HashMap<String, File> pairListFile;
    String response;
    boolean isShow = true;
    boolean isGetMethod = false;

    public AllAPICall(Context context, HashMap<String, String> pairListString, HashMap<String, File> pairListFile, onTaskComplete taskComplete) {
        this.mOnTaskComplete = taskComplete;
        this.context = context;
        utility = new MyUtility(context);
        this.pairListString = pairListString;
        this.pairListFile = pairListFile;
        isShow = true;
    }

    /*
    For get method
     */
    public AllAPICall(Context context, HashMap<String, String> pairListString, HashMap<String, File> pairListFile, boolean isGetMethod, onTaskComplete taskComplete, boolean isShow) {
        this.mOnTaskComplete = taskComplete;
        this.context = context;
        utility = new MyUtility(context);
        this.pairListString = pairListString;
        this.pairListFile = pairListFile;
        this.isShow = isShow;
        this.isGetMethod = isGetMethod;
    }

    public AllAPICall(Context context, HashMap<String, String> pairListString, HashMap<String, File> pairListFile, onTaskComplete taskComplete, boolean isShow) {
        this.mOnTaskComplete = taskComplete;
        this.context = context;
        utility = new MyUtility(context);
        this.pairListString = pairListString;
        this.pairListFile = pairListFile;
        this.isShow = isShow;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        jsonParser = new GetJsonUrl();
        if (isShow)
            utility.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
        Log.i("URL : ", params[0]);
        if (pairListString != null && pairListFile == null) {
            Log.i("params", pairListString.toString());
            response = jsonParser.getJSONResponseFromUrl(params[0], pairListString, null, isGetMethod);
        } else if (pairListFile != null && pairListString != null) {
            Log.i("params", pairListString.toString() + " " + pairListFile.toString());
            response = jsonParser.getJSONResponseFromUrl(params[0], pairListString, pairListFile, isGetMethod);
        } else
            response = jsonParser.getJSONResponseFromUrl(params[0], null, null, isGetMethod);

        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (isShow) {
            utility.hideProgress();
        }
        if (result != null && !result.equals("")) {
            Log.i("response", result);
            mOnTaskComplete.onComplete(result);
        } else {
            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();
        }
    }
}
