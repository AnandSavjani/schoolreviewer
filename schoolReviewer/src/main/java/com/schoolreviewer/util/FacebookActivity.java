package com.schoolreviewer.util;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.schoolreviewer.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class FacebookActivity extends BaseActivity {

    private HashMap<String, String> stringHashMap;
    private CallbackManager callbackManager;
    private LoginButton loginButton;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        loginButton = new LoginButton(this);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    JSONObject jsonObject = object.getJSONObject("picture");
                                    JSONObject objData = jsonObject.getJSONObject("data");
                                    LoginAsyncCall(object.getString("id"), "facebook", object.getString("name"));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,picture");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                finish();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(FacebookActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        if (utility.checkInternetConnection())
            loginButton.performClick();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void LoginAsyncCall(String id, String network, String name) {
        stringHashMap = new HashMap<>();
        stringHashMap.put("socialid", id);
        stringHashMap.put("socialnetwork", network);
        stringHashMap.put("socialdisplayname", name);
        stringHashMap.put("platform", "android");
        stringHashMap.put("devicetoken", sharedPreferences.getString("regid", ""));

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjLogin = jsonArray.getJSONObject(0);
                    if (jObjLogin.has("key")) {
                        if (jObjLogin.getInt("code") == 1) {
                            LoginManager.getInstance().logOut();
                            SharedPreferences.Editor mEditor = sharedPreferences.edit();
                            mEditor.putString("key", jObjLogin.getString("key"));
                            mEditor.putString("uiduser", jObjLogin.getString("uiduser"));
                            mEditor.putString("social", "yes");
                            mEditor.apply();
                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_LOGIN);
    }
}
