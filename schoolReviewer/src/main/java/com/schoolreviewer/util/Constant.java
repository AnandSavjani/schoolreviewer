package com.schoolreviewer.util;

public class Constant {
    public static final int SPLASH_TIME_OUT = 10;

    public static String COUNTRY = "uk";
    public static String POUND = "£";
    public static String GCM_KEY = "AIzaSyCm7PXbEjnuDJKBajNYEHI1scWOCiQgbyc";

    public static int flag = 0;

        public static String BASE_URL = "http://www.srloadtest.co.uk/api/v103/";
//    public static String BASE_URL = "http://www.schoolreviewer.co.uk/api/v103/";
        public static String BASE_IMAGE_URL = "http://www.srloadtest.co.uk/";
//    public static String BASE_IMAGE_URL = "http://www.schoolreviewer.co.uk/";

    public static String URL_SEARCH = BASE_URL + "srapi.search.php";
    public static String URL_LOGIN = BASE_URL + "srapi.startsession.php";
    public static String URL_REGISTER = BASE_URL + "srapi.register.php";
    public static String URL_GET_USER_DETAILS = BASE_URL + "srapi.getuserdetails.php";
    public static String URL_CHANGE_PASSWORD = BASE_URL + "srapi.updateuserpassword.php";
    public static String URL_UPDATE_USER_DETAILS = BASE_URL + "srapi.updateuserdetails.php";
    public static String URL_GET_USER_SUBSCRIPTION = BASE_URL + "srapi.getusersubscriptions.php";
    public static String URL_UNSUBSCRIBE = BASE_URL + "srapi.unsubscribefromschool.php";
    public static String URL_GET_USER_REVIEW = BASE_URL + "srapi.getuserreviews.php";
    public static String URL_SCHOOL_DETAILS = BASE_URL + "srapi.getschool.php";
    public static String URL_SUBSCRIBE_TO_SCHOOL = BASE_URL + "srapi.subscribetoschool.php";
    public static String URL_LOG_OUT = BASE_URL + "srapi.endsession.php";
    public static String URL_ADD_REVIEW = BASE_URL + "srapi.addreview.php";
    public static String URL_ADD_COMPLAINT = BASE_URL + "srapi.addcomplaint.php";
    public static String URL_AUTO_COMPLETE = BASE_URL + "srapi.autocomplete.php";
    public static String URL_USER_QUESTION = BASE_URL + "srapi.getuserquestions.php";
    public static String URL_ADD_QUESTION = BASE_URL + "srapi.addquestion.php";
    public static String URL_ADD_ANSWER = BASE_URL + "srapi.addanswer.php";
    public static String URL_FORGOT_PASSWORD = BASE_IMAGE_URL + "password-reset.php";
    public static String URL_GET_ALL_SUBJECT = BASE_URL + "srapi.gettutorsubjects.php";
    public static String URL_SEARCH_TUTOR = BASE_URL + "srapi.searchtutors.php";
    public static String URL_GET_TUTOR = BASE_URL + "srapi.gettutor.php";
    public static String URL_GET_SCHOOL_FORUM = BASE_URL + "srapi.getschoolforum.php";
    public static String URL_GET_SCHOOL_ITEMS = BASE_URL + "srapi.getschoolitems.php";
    public static String URL_ADD_ADVERT_MESSAGE = BASE_URL + "srapi.addadvertmessage.php";
    public static String URL_USER_ITEMS_PURCHASED = BASE_URL + "srapi.getuseritemspurchased.php";
    public static String URL_UPDATE_ITEM_SALE = BASE_URL + "srapi.updateitemforsale.php";
    public static String URL_UPDATE_ITEM_AS_SOLD = BASE_URL + "srapi.updateitemforsaleassold.php";
    public static String URL_UPDATE_ITEM_AS_UNSOLD = BASE_URL + "srapi.updateitemforsaleasunsold.php";
    public static String URL_DELETE_ITEM = BASE_URL + "srapi.deleteitemforsale.php";
    public static String URL_ADD_ITEM_FOR_SALE = BASE_URL + "srapi.additemforsale.php";
    public static String URL_USER_ITEMS_FOR_SALE = BASE_URL + "srapi.getuseritemsforsale.php";
    public static String URL_ADD_IMAGE_FOR_SELL = BASE_URL + "srapi.addimagetoitemforsaleext.php";
    public static String URL_DELETE_IMAGE_FOR_SELL = BASE_URL + "srapi.deleteimagefromitemforsale.php";

    public static String URL_SEARCH_EXAMS = BASE_URL + "srapi.searchexams.php";
    public static String URL_GET_EXAM = BASE_URL + "srapi.getexam.php";

    public static String URL_PAYPAL_ITEM_PURCHASE = BASE_URL + "srapi.paypalitempurchase.php";
    public static String URL_PAYPAL_EXAM_PURCHASE = BASE_URL + "srapi.paypalexampurchase.php";
    public static String URL_GET_PAYPAL_RULES = BASE_URL + "srapi.getpaypalrules.php";
    public static String URL_UPDATE_EXAM_WATCHED = BASE_URL + "srapi.updateexamaswatched.php";
    public static String URL_ADD_TUTOR_MESSAGE = BASE_URL + "srapi.addtutormessage.php";

    public static String LOGIN_PREFERENCE = "loginPreference";
    public static String REMEMBER_PREFERENCE = "rememberPreference";

    public static String URL_BLOG_LIST = "http://www.schoolreviewer.co.uk/resources/wp-json/posts?page=";
}