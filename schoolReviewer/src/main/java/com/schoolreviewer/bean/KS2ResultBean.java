package com.schoolreviewer.bean;

public class KS2ResultBean
{
	String year, KS2_PTREADWRITTAMATBX, KS2_PTREADWRITTAMATX, KS2_PTREADWRITTAMAT4B, KS2_PTREADWRITTAMATAX, KS2_TAPS,
			KS2_AVGLEVEL;

	public String getYear()
	{
		return year;
	}

	public void setYear(String year)
	{
		this.year = year;
	}

	public String getKS2_PTREADWRITTAMATBX()
	{
		return KS2_PTREADWRITTAMATBX;
	}

	public void setKS2_PTREADWRITTAMATBX(String kS2_PTREADWRITTAMATBX)
	{
		if (kS2_PTREADWRITTAMATBX.equals(""))
			KS2_PTREADWRITTAMATBX = "N/A";
		else
			KS2_PTREADWRITTAMATBX = kS2_PTREADWRITTAMATBX;
	}

	public String getKS2_PTREADWRITTAMATX()
	{
		return KS2_PTREADWRITTAMATX;
	}

	public void setKS2_PTREADWRITTAMATX(String kS2_PTREADWRITTAMATX)
	{
		if (kS2_PTREADWRITTAMATX.equals(""))
			KS2_PTREADWRITTAMATX = "N/A";
		else
			KS2_PTREADWRITTAMATX = kS2_PTREADWRITTAMATX;
	}

	public String getKS2_PTREADWRITTAMAT4B()
	{
		return KS2_PTREADWRITTAMAT4B;
	}

	public void setKS2_PTREADWRITTAMAT4B(String kS2_PTREADWRITTAMAT4B)
	{
		if (kS2_PTREADWRITTAMAT4B.equals(""))
			KS2_PTREADWRITTAMAT4B = "N/A";
		else
			KS2_PTREADWRITTAMAT4B = kS2_PTREADWRITTAMAT4B;
	}

	public String getKS2_PTREADWRITTAMATAX()
	{
		return KS2_PTREADWRITTAMATAX;
	}

	public void setKS2_PTREADWRITTAMATAX(String kS2_PTREADWRITTAMATAX)
	{
		if (kS2_PTREADWRITTAMATAX.equals(""))
			KS2_PTREADWRITTAMATAX = "N/A";
		else
			KS2_PTREADWRITTAMATAX = kS2_PTREADWRITTAMATAX;
	}

	public String getKS2_TAPS()
	{
		return KS2_TAPS;
	}

	public void setKS2_TAPS(String kS2_TAPS)
	{
		if (kS2_TAPS.equals(""))
			KS2_TAPS = "N/A";
		else
			KS2_TAPS = kS2_TAPS;
	}

	public String getKS2_AVGLEVEL()
	{
		return KS2_AVGLEVEL;
	}

	public void setKS2_AVGLEVEL(String kS2_AVGLEVEL)
	{
		if (kS2_AVGLEVEL.equals(""))
			KS2_AVGLEVEL = "N/A";
		else
			KS2_AVGLEVEL = kS2_AVGLEVEL;
	}

}
