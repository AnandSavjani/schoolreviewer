package com.schoolreviewer.bean;


import java.io.Serializable;
import java.util.List;

public class ItemBean implements Serializable {

    private String uid, category, title, seller, uidseller, condition, price,
            pricewithdelivery, currency, description, forcollection,
            fordelivery, foronlinepayment, sellerpaypal, contactemail, contactphone,
            formanualpayment, sold;

    private boolean isPurchase;

    private List<ImagesBean> listImages;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getUidseller() {
        return uidseller;
    }

    public void setUidseller(String uidseller) {
        this.uidseller = uidseller;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPricewithdelivery() {
        return pricewithdelivery;
    }

    public void setPricewithdelivery(String pricewithdelivery) {
        this.pricewithdelivery = pricewithdelivery;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getForcollection() {
        return forcollection;
    }

    public void setForcollection(String forcollection) {
        this.forcollection = forcollection;
    }

    public String getFordelivery() {
        return fordelivery;
    }

    public void setFordelivery(String fordelivery) {
        this.fordelivery = fordelivery;
    }

    public String getForonlinepayment() {
        return foronlinepayment;
    }

    public void setForonlinepayment(String foronlinepayment) {
        this.foronlinepayment = foronlinepayment;
    }

    public String getSellerpaypal() {
        return sellerpaypal;
    }

    public void setSellerpaypal(String sellerpaypal) {
        this.sellerpaypal = sellerpaypal;
    }

    public List<ImagesBean> getListImages() {
        return listImages;
    }

    public void setListImages(List<ImagesBean> listImages) {
        this.listImages = listImages;
    }

    public String getContactemail() {
        return contactemail;
    }

    public void setContactemail(String contactemail) {
        this.contactemail = contactemail;
    }

    public String getContactphone() {
        return contactphone;
    }

    public void setContactphone(String contactphone) {
        this.contactphone = contactphone;
    }

    public String getFormanualpayment() {
        return formanualpayment;
    }

    public void setFormanualpayment(String formanualpayment) {
        this.formanualpayment = formanualpayment;
    }

    public String getSold() {
        return sold;
    }

    public void setSold(String sold) {
        this.sold = sold;
    }

    public boolean isPurchase() {
        return isPurchase;
    }

    public void setPurchase(boolean purchase) {
        isPurchase = purchase;
    }
}
