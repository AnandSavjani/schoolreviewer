package com.schoolreviewer.bean;

public class SubscriptionBean
{

	String uid, uidschool, schoolname;

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getUidschool()
	{
		return uidschool;
	}

	public void setUidschool(String uidschool)
	{
		this.uidschool = uidschool;
	}

	public String getSchoolname()
	{
		return schoolname;
	}

	public void setSchoolname(String schoolname)
	{
		this.schoolname = schoolname;
	}
}
