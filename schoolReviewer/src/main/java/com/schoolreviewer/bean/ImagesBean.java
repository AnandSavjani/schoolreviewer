package com.schoolreviewer.bean;

import java.io.Serializable;

public class ImagesBean implements Serializable{

    private String uid, url;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
