package com.schoolreviewer.bean;

import java.io.Serializable;


public class ExamBean implements Serializable {

    private String uid, name, level, subject, description, certification, price, numberpapers, timetaken, language, year,
            tutorname, uidexamimage, urlexamimage, uidtutorimage, urltutorimage;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCertification() {
        return certification;
    }

    public void setCertification(String certification) {
        this.certification = certification;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNumberpapers() {
        return numberpapers;
    }

    public void setNumberpapers(String numberpapers) {
        this.numberpapers = numberpapers;
    }

    public String getTimetaken() {
        return timetaken;
    }

    public void setTimetaken(String timetaken) {
        this.timetaken = timetaken;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTutorname() {
        return tutorname;
    }

    public void setTutorname(String tutorname) {
        this.tutorname = tutorname;
    }

    public String getUidexamimage() {
        return uidexamimage;
    }

    public void setUidexamimage(String uidexamimage) {
        this.uidexamimage = uidexamimage;
    }

    public String getUrlexamimage() {
        return urlexamimage;
    }

    public void setUrlexamimage(String urlexamimage) {
        this.urlexamimage = urlexamimage;
    }

    public String getUidtutorimage() {
        return uidtutorimage;
    }

    public void setUidtutorimage(String uidtutorimage) {
        this.uidtutorimage = uidtutorimage;
    }

    public String getUrltutorimage() {
        return urltutorimage;
    }

    public void setUrltutorimage(String urltutorimage) {
        this.urltutorimage = urltutorimage;
    }
}
