package com.schoolreviewer.bean;

import java.io.Serializable;

public class ReviewBean implements Serializable {
    private static final long serialVersionUID = 1L;
    String uid, uidreviewer;
    String title;
    String displayname;
    float scoreOfReview;
    String fromFragment;
    String reviewedat, reviewyear;
    String further;
    String LoH;
    String QoT;
    String AoS;
    String SaS;
    String sports;
    String music;
    String food;
    String reply;
    String relationship;

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getUidreviewer() {
        return uidreviewer;
    }

    public void setUidreviewer(String uidreviewer) {
        this.uidreviewer = uidreviewer;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFromFragment() {
        return fromFragment;
    }

    public void setFromFragment(String fromFragment) {
        this.fromFragment = fromFragment;
    }

    public String getReviewyear() {
        return reviewyear;
    }

    public void setReviewyear(String reviewyear) {
        this.reviewyear = reviewyear;
    }

    public String getReviewedat() {
        return reviewedat;
    }

    public void setReviewedat(String reviewedat) {
        this.reviewedat = reviewedat;
    }

    public String getFurther() {
        return further;
    }

    public void setFurther(String further) {
        this.further = further;
    }

    public String getLoH() {
        return LoH;
    }

    public void setLoH(String loH) {
        LoH = loH;
    }

    public String getQoT() {
        return QoT;
    }

    public void setQoT(String qoT) {
        QoT = qoT;
    }

    public String getAoS() {
        return AoS;
    }

    public void setAoS(String aoS) {
        AoS = aoS;
    }

    public String getSaS() {
        return SaS;
    }

    public void setSaS(String saS) {
        SaS = saS;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public float getScoreOfReview() {
        return scoreOfReview;
    }

    public void setScoreOfReview(float scoreOfReview) {
        this.scoreOfReview = scoreOfReview;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

}
