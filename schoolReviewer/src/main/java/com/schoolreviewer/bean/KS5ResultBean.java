package com.schoolreviewer.bean;

public class KS5ResultBean
{
	private String year, KS5_PTPASS3LV3_ALEVCOH, KS5_TALLPPEGRD_ALEVA;

	public String getYear()
	{
		return year;
	}

	public void setYear(String year)
	{
		this.year = year;
	}

	public String getKS5_PTPASS3LV3_ALEVCOH()
	{
		return KS5_PTPASS3LV3_ALEVCOH;
	}

	public void setKS5_PTPASS3LV3_ALEVCOH(String kS5_PTPASS3LV3_ALEVCOH)
	{
		if (kS5_PTPASS3LV3_ALEVCOH.equals(""))
			KS5_PTPASS3LV3_ALEVCOH = "N/A";
		else
			KS5_PTPASS3LV3_ALEVCOH = kS5_PTPASS3LV3_ALEVCOH;
	}

	public String getKS5_TALLPPEGRD_ALEVA()
	{
		return KS5_TALLPPEGRD_ALEVA;
	}

	public void setKS5_TALLPPEGRD_ALEVA(String kS5_TALLPPEGRD_ALEVA)
	{
		if (kS5_TALLPPEGRD_ALEVA.equals(""))
			KS5_TALLPPEGRD_ALEVA = "N/A";
		else
			KS5_TALLPPEGRD_ALEVA = kS5_TALLPPEGRD_ALEVA;
	}
}
