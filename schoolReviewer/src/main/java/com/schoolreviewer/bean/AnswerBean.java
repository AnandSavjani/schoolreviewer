package com.schoolreviewer.bean;

import java.io.Serializable;

public class AnswerBean implements Serializable {

    String uid;
    String uidansweredby;
    String displayname;
    String numberlikes;
    String numberdislikes;
    String answeredat;
    String relationship;
    String answeryear;
    String answer, timenow;
    String category;
    boolean canvoteanswer;
    String uidschool;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnsweryear() {
        return answeryear;
    }

    public void setAnsweryear(String answeryear) {
        this.answeryear = answeryear;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getAnsweredat() {
        return answeredat;
    }

    public void setAnsweredat(String answeredat) {
        this.answeredat = answeredat;
    }

    public String getNumberdislikes() {
        return numberdislikes;
    }

    public void setNumberdislikes(String numberdislikes) {
        this.numberdislikes = numberdislikes;
    }

    public String getNumberlikes() {
        return numberlikes;
    }

    public void setNumberlikes(String numberlikes) {
        this.numberlikes = numberlikes;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getUidansweredby() {
        return uidansweredby;
    }

    public void setUidansweredby(String uidansweredby) {
        this.uidansweredby = uidansweredby;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTimenow() {
        return timenow;
    }

    public void setTimenow(String timenow) {
        this.timenow = timenow;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUidschool() {
        return uidschool;
    }

    public void setUidschool(String uidschool) {
        this.uidschool = uidschool;
    }
}
