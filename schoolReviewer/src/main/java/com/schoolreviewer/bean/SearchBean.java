package com.schoolreviewer.bean;

import java.util.ArrayList;

public class SearchBean {

    String pageno, actualstart, norows, actualend, nopages;

    ArrayList<SearchItemBean> searchitem = new ArrayList<SearchItemBean>();

    public String getPageno() {
        return pageno;
    }

    public void setPageno(String pageno) {
        this.pageno = pageno;
    }

    public String getActualstart() {
        return actualstart;
    }

    public void setActualstart(String actualstart) {
        this.actualstart = actualstart;
    }

    public String getNorows() {
        return norows;
    }

    public void setNorows(String norows) {
        this.norows = norows;
    }

    public String getActualend() {
        return actualend;
    }

    public void setActualend(String actualend) {
        this.actualend = actualend;
    }

    public String getNopages() {
        return nopages;
    }

    public void setNopages(String nopages) {
        this.nopages = nopages;
    }

    public ArrayList<SearchItemBean> getSearchitem() {
        return searchitem;
    }

    public void setSearchitem(ArrayList<SearchItemBean> searchitem) {
        this.searchitem = searchitem;
    }

}
