package com.schoolreviewer.bean;

import java.io.Serializable;

/**
 * Created by sotsys-219 on 16/8/16.
 */
public class BlogBean implements Serializable {

    private String title;
    private String content;
    private String guid;
    private String date;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
