package com.schoolreviewer.bean;

public class FEResultBean {
    String year, FE_RANK, FE_OVERALL_SCORE;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getFE_RANK() {
        return FE_RANK;
    }

    public void setFE_RANK(String fE_RANK) {
        if (fE_RANK.equals(""))
            FE_RANK = "N/A";
        else
            FE_RANK = fE_RANK;
    }

    public String getFE_OVERALL_SCORE() {
        return FE_OVERALL_SCORE;
    }

    public void setFE_OVERALL_SCORE(String fE_OVERALL_SCORE) {
        if (fE_OVERALL_SCORE.equals(""))
            FE_OVERALL_SCORE = "N/A";
        else
            FE_OVERALL_SCORE = fE_OVERALL_SCORE;
    }
}
