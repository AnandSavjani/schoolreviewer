package com.schoolreviewer.bean;

public class KS4ResultBean {
    private String year, KS4_PTGAC5EM_PTQ, KS4_AVGRDPEPPCP_PTQ;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getKS4_PTGAC5EM_PTQ() {
        return KS4_PTGAC5EM_PTQ;
    }

    public void setKS4_PTGAC5EM_PTQ(String kS4_PTGAC5EM_PTQ) {
        if (kS4_PTGAC5EM_PTQ.equals(""))
            KS4_PTGAC5EM_PTQ = "N/A";
        else
            KS4_PTGAC5EM_PTQ = kS4_PTGAC5EM_PTQ;

    }

    public String getKS4_AVGRDPEPPCP_PTQ() {
        return KS4_AVGRDPEPPCP_PTQ;
    }

    public void setKS4_AVGRDPEPPCP_PTQ(String kS4_AVGRDPEPPCP_PTQ) {
        if (kS4_AVGRDPEPPCP_PTQ.equals(""))
            KS4_AVGRDPEPPCP_PTQ = "N/A";
        else
            KS4_AVGRDPEPPCP_PTQ = kS4_AVGRDPEPPCP_PTQ;
    }
}
