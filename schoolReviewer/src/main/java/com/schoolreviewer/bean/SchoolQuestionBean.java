package com.schoolreviewer.bean;

import java.io.Serializable;
import java.util.List;

public class SchoolQuestionBean implements Serializable {

    private String uid;
    private String uidaskedby;
    private String title;
    private String question;
    private String displayname;
    private String askedat;
    private String relationship;
    private String questionyear;
    private String currtime;
    private String category;
    private List<AnswerBean> listAnswerBean;

    public String getCurrtime() {
        return currtime;
    }

    public void setCurrtime(String currtime) {
        this.currtime = currtime;
    }

    public String getQuestionyear() {
        return questionyear;
    }

    public void setQuestionyear(String questionyear) {
        this.questionyear = questionyear;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getAskedat() {
        return askedat;
    }

    public void setAskedat(String askedat) {
        this.askedat = askedat;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUidaskedby() {
        return uidaskedby;
    }

    public void setUidaskedby(String uidaskedby) {
        this.uidaskedby = uidaskedby;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<AnswerBean> getListAnswerBean() {
        return listAnswerBean;
    }

    public void setListAnswerBean(List<AnswerBean> listAnswerBean) {
        this.listAnswerBean = listAnswerBean;
    }
}
