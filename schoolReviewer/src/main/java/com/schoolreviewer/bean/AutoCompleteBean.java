package com.schoolreviewer.bean;

public class AutoCompleteBean {
    String estaName, estaType;

    public String getEstaName() {
        return estaName;
    }

    public void setEstaName(String estaName) {
        this.estaName = estaName;
    }

    public String getEstaType() {
        return estaType;
    }

    public void setEstaType(String estaType) {
        this.estaType = estaType;
    }
}
