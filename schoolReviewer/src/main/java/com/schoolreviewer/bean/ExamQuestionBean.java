package com.schoolreviewer.bean;

import java.io.Serializable;


public class ExamQuestionBean implements Serializable {

    private String uid, name, summary, cando, mustbuy, vimeoid, haswatched, examUid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getCando() {
        return cando;
    }

    public void setCando(String cando) {
        this.cando = cando;
    }

    public String getMustbuy() {
        return mustbuy;
    }

    public void setMustbuy(String mustbuy) {
        this.mustbuy = mustbuy;
    }

    public String getVimeoid() {
        return vimeoid;
    }

    public void setVimeoid(String vimeoid) {
        this.vimeoid = vimeoid;
    }

    public String getHaswatched() {
        return haswatched;
    }

    public void setHaswatched(String haswatched) {
        this.haswatched = haswatched;
    }

    public String getExamUid() {
        return examUid;
    }

    public void setExamUid(String examUid) {
        this.examUid = examUid;
    }
}
