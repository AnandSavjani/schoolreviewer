package com.schoolreviewer.bean;

import java.io.Serializable;

public class MyQuestionBean implements Serializable {

    String uid;
    String uidschool;
    String schoolname;
    String title;
    String question;
    String askedat;
    String questionyear;
    String numberviews;
    String numberlikes;
    String numberdislikes;
    String relationship;
    String timenow;
    String category;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUidschool() {
        return uidschool;
    }

    public void setUidschool(String uidschool) {
        this.uidschool = uidschool;
    }

    public String getSchoolname() {
        return schoolname;
    }

    public void setSchoolname(String schoolname) {
        this.schoolname = schoolname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAskedat() {
        return askedat;
    }

    public void setAskedat(String askedat) {
        this.askedat = askedat;
    }

    public String getQuestionyear() {
        return questionyear;
    }

    public void setQuestionyear(String questionyear) {
        this.questionyear = questionyear;
    }

    public String getNumberviews() {
        return numberviews;
    }

    public void setNumberviews(String numberviews) {
        this.numberviews = numberviews;
    }

    public String getNumberlikes() {
        return numberlikes;
    }

    public void setNumberlikes(String numberlikes) {
        this.numberlikes = numberlikes;
    }

    public String getNumberdislikes() {
        return numberdislikes;
    }

    public void setNumberdislikes(String numberdislikes) {
        this.numberdislikes = numberdislikes;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getTimenow() {
        return timenow;
    }

    public void setTimenow(String timenow) {
        this.timenow = timenow;
    }
}
