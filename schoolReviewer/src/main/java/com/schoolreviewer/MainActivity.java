package com.schoolreviewer;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.schoolreviewer.blog.BlogBaseFragment;
import com.schoolreviewer.buysell.BuySellListFragment;
import com.schoolreviewer.exam.ExamFragment;
import com.schoolreviewer.home.HomeFragment;
import com.schoolreviewer.home.SchoolProfileFragment;
import com.schoolreviewer.home.SearchFragment;
import com.schoolreviewer.home.SearchResultFragment;
import com.schoolreviewer.me.MeFragment;
import com.schoolreviewer.me.RegisterFragment;
import com.schoolreviewer.parentforum.ParentForumListFragment;
import com.schoolreviewer.tutor.TutorBaseFragment;
import com.schoolreviewer.tutor.TutorFragment;
import com.schoolreviewer.tutor.TutorSearchResultFragment;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private TextView textSchools, textTutor, textExam, textBuySell, textParentForum, textBlog, textHeaderTitle,
            textRegister, textLogin;
    private DrawerLayout drawerLayout;
    private ImageView imgSlider;
    private FrameLayout flDrawer;
    private ImageView imgShare, imgFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initControl();
    }

    private void initControl() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerLayout.addDrawerListener(drawerListener);

        textSchools = (TextView) findViewById(R.id.textSchools);
        textTutor = (TextView) findViewById(R.id.textTutor);
        textExam = (TextView) findViewById(R.id.textExam);
        textBuySell = (TextView) findViewById(R.id.textBuySell);
        textParentForum = (TextView) findViewById(R.id.textParentForum);
        textBlog = (TextView) findViewById(R.id.textBlog);
        textRegister = (TextView) findViewById(R.id.textRegister);
        textLogin = (TextView) findViewById(R.id.textLogin);
        textHeaderTitle = (TextView) findViewById(R.id.textHeaderTitle);

        textSchools.setOnClickListener(this);
        textTutor.setOnClickListener(this);
        textExam.setOnClickListener(this);
        textBuySell.setOnClickListener(this);
        textParentForum.setOnClickListener(this);
        textBlog.setOnClickListener(this);
        textRegister.setOnClickListener(this);
        textLogin.setOnClickListener(this);

        imgSlider = (ImageView) findViewById(R.id.imgSlider);
        imgShare = (ImageView) findViewById(R.id.imgShare);
        imgFilter = (ImageView) findViewById(R.id.imgFilter);

        imgSlider.setOnClickListener(this);
        imgShare.setOnClickListener(onClickListener);

        flDrawer = (FrameLayout) findViewById(R.id.flDrawer);

        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        HomeFragment homeFragment = new HomeFragment();
        fragmentTransaction.replace(R.id.flRealtabcontent, homeFragment).commit();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {

        if (v != imgSlider) {
            findViewById(R.id.imgAbuseReport).setVisibility(View.GONE);
            getFragmentManager().popBackStackImmediate();
            FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
        }

        if (v == imgSlider) {
            drawerLayout.openDrawer(flDrawer);
            utility.hideSoftKeyboard();
        } else if (v == textSchools) {
            FragmentManager mFragmentManager = getSupportFragmentManager();
            FragmentTransaction mFragmentTrasaction = mFragmentManager.beginTransaction();
            HomeFragment homeFragment = new HomeFragment();
            mFragmentTrasaction.replace(R.id.flRealtabcontent, homeFragment).commit();

        } else if (v == textRegister) {
            if (sharedPreferences.contains("key")) {
                FragmentManager mFragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                MeFragment meFragment = new MeFragment();
                fragmentTransaction.replace(R.id.flRealtabcontent, meFragment).commit();
            } else {
                Bundle bundle = new Bundle();
                FragmentManager mFragmentManager = getSupportFragmentManager();
                FragmentTransaction mFragmentTrasaction = mFragmentManager.beginTransaction();
                RegisterFragment registerFragment = new RegisterFragment();
                bundle.putString("header", getResources().getString(R.string.txt_register));
                registerFragment.setArguments(bundle);
                mFragmentTrasaction.replace(R.id.flRealtabcontent, registerFragment).commit();
            }

        } else if (v == textLogin) {
            if (sharedPreferences.contains("key")) {
                textLogin.setText(R.string.lbl_logout);
                if (utility.checkInternetConnection()) {
                    logoutAsyncCall();
                }
            } else {
                FragmentManager mFragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                MeFragment meFragment = new MeFragment();
                fragmentTransaction.replace(R.id.flRealtabcontent, meFragment).commit();
            }

        } else if (v == textTutor) {
            FragmentManager mFragmentManager = getSupportFragmentManager();
            FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
            TutorBaseFragment tutorBaseFragment = new TutorBaseFragment();
            mFragmentTransaction.replace(R.id.flRealtabcontent, tutorBaseFragment).commit();
        } else if (v == textExam) {
            FragmentManager mFragmentManager = getSupportFragmentManager();
            FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
            ExamFragment examFragment = new ExamFragment();
            mFragmentTransaction.replace(R.id.flRealtabcontent, examFragment).commit();
        } else if (v == textBuySell) {
            Bundle bundle = new Bundle();
            bundle.putString("uid", "national");
            FragmentManager mFragmentManager = getSupportFragmentManager();
            FragmentTransaction mFragmentTrasaction = mFragmentManager.beginTransaction();
            BuySellListFragment buySellListFragment = new BuySellListFragment();
            buySellListFragment.setArguments(bundle);
            mFragmentTrasaction.replace(R.id.flRealtabcontent, buySellListFragment).commit();
        } else if (v == textParentForum) {
            Bundle bundle = new Bundle();
            bundle.putString("uid", "national");
            FragmentManager mFragmentManager = getSupportFragmentManager();
            FragmentTransaction mFragmentTrasaction = mFragmentManager.beginTransaction();
            ParentForumListFragment parentForumListFragment = new ParentForumListFragment();
            parentForumListFragment.setArguments(bundle);
            mFragmentTrasaction.replace(R.id.flRealtabcontent, parentForumListFragment, "result").commit();
        } else if (v == textBlog) {
            Bundle bundle = new Bundle();
            FragmentManager mFragmentManager = getSupportFragmentManager();
            FragmentTransaction mFragmentTrasaction = mFragmentManager.beginTransaction();
            BlogBaseFragment blogBaseFragment = new BlogBaseFragment();
            blogBaseFragment.setArguments(bundle);
            mFragmentTrasaction.replace(R.id.flRealtabcontent, blogBaseFragment, "result").commit();
        }

        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawers();
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("result");
        if (fragment instanceof SchoolProfileFragment) {
            Constant.flag = 1;
        }

        if (drawerLayout.isDrawerOpen(flDrawer)) {
            drawerLayout.closeDrawer(flDrawer);
        } else
            super.onBackPressed();

        Fragment myFragment = getSupportFragmentManager().findFragmentByTag("fragment");

        if (myFragment instanceof SearchFragment && myFragment.isVisible()) {
            textHeaderTitle.setText(R.string.txt_title_home);
        } else if (myFragment instanceof SearchResultFragment && myFragment.isVisible()) {
            textHeaderTitle.setText(R.string.txt_search_result);
        } else if (myFragment instanceof SchoolProfileFragment && myFragment.isVisible()) {
            textHeaderTitle.setText(R.string.txt_school_profile);
        } else if (myFragment instanceof TutorFragment) {
            textHeaderTitle.setText(R.string.lbl_tutors);
        } else if (myFragment instanceof TutorSearchResultFragment) {
            textHeaderTitle.setText(R.string.txt_search_result);
        }

        if (myFragment instanceof SearchResultFragment && myFragment.isVisible() ||
                myFragment instanceof TutorSearchResultFragment && myFragment.isVisible()) {
            imgFilter.setVisibility(View.VISIBLE);
        } else {
            imgFilter.setVisibility(View.INVISIBLE);
        }
    }

    DrawerLayout.DrawerListener drawerListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            if (sharedPreferences.contains("key")) {
                textRegister.setText(R.string.txt_my_account);
                textRegister.setCompoundDrawablesWithIntrinsicBounds(R.drawable.myaccount, 0, 0, 0);

                textLogin.setText(R.string.lbl_logout);
                textLogin.setCompoundDrawablesWithIntrinsicBounds(R.drawable.logout, 0, 0, 0);

            } else {
                textRegister.setText(R.string.txt_register);
                textRegister.setCompoundDrawablesWithIntrinsicBounds(R.drawable.register, 0, 0, 0);

                textLogin.setText(R.string.btn_login);
                textLogin.setCompoundDrawablesWithIntrinsicBounds(R.drawable.logout, 0, 0, 0);
            }
        }

        @Override
        public void onDrawerOpened(View drawerView) {

        }

        @Override
        public void onDrawerClosed(View drawerView) {

        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    };

    private void shareIntent() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareTitle = "Check out School Reviewer, it allows you to review and read reviews on all schools in the UK";
        String link = "www.schoolreviewer.co.uk";

        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(sharingIntent, 0);
        if (!resInfo.isEmpty()) {
            List<Intent> targetedShareIntents = new ArrayList<Intent>();
            Intent targetedShareIntent = null;

            for (ResolveInfo resolveInfo : resInfo) {
                String packageName = resolveInfo.activityInfo.packageName;
                targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);
                targetedShareIntent.setType("text/plain");

                // Find twitter: com.twitter.android...
                if ("com.twitter.android".equals(packageName)) {
                    targetedShareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareTitle);
                    targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, Uri.encode(link));
                } else if ("com.google.android.gm".equals(packageName)) {
                    targetedShareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareTitle);
                    targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, Uri.encode(link));
                } else if ("com.android.email".equals(packageName)) {
                    targetedShareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareTitle);
                    targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, Uri.encode(link));
                } else {
                    targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, Uri.encode(link));
                }

                targetedShareIntent.setPackage(packageName);
                targetedShareIntents.add(targetedShareIntent);
            }
            Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share via");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
            startActivityForResult(chooserIntent, 0);
        }
    }

    private void logoutAsyncCall() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("key", sharedPreferences.getString("key", null));

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    sharedPreferences.edit().clear().apply();
                    FragmentManager mFragmentManager = getSupportFragmentManager();
                    FragmentTransaction mFragmentTrasaction = mFragmentManager.beginTransaction();
                    HomeFragment homeFragment = new HomeFragment();
                    mFragmentTrasaction.replace(R.id.flRealtabcontent, homeFragment).commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_LOG_OUT);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            shareIntent();
        }
    };
}