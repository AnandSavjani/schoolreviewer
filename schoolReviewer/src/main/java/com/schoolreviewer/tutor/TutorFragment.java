package com.schoolreviewer.tutor;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.FontHelper;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TutorFragment extends BaseFragment implements View.OnClickListener, View.OnKeyListener, SeekBar.OnSeekBarChangeListener {

    private TextView textHeaderTitle, textDistance;
    private EditText editLocation;
    private Button btnAdvanceOption, btnReset, btnSearch;
    private LinearLayout llAdvanceTutor;
    private boolean isAdvanceSearch = false;
    private View mView;
    private HashMap<String, String> stringHashMap;
    private List<String> listSubjects;
    private AutoCompleteTextView actSubject;
    private ArrayAdapter<String> actAdapter;
    private Bundle bundle;
    private SeekBar sbDistance;
    private FragmentTransaction fragmentTransaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = View.inflate(getActivity(), R.layout.fragment_tutor, null);
        mView.setOnKeyListener(this);
        return mView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initControl(view);
        FontHelper.applyFont(getActivity(), btnAdvanceOption, "opensansbold.ttf");
        FontHelper.applyFont(getActivity(), btnReset, "opensansbold.ttf");
        FontHelper.applyFont(getActivity(), btnSearch, "opensansbold.ttf");
    }

    private void initControl(View view) {
        bundle = new Bundle();

        listSubjects = new ArrayList<>();

        mView.setFocusableInTouchMode(true);
        mView.requestFocus(R.layout.fragment_tutor);

        textHeaderTitle = (TextView) getActivity().findViewById(R.id.textHeaderTitle);
        textDistance = (TextView) view.findViewById(R.id.textDistance);
        textHeaderTitle.setText(R.string.lbl_tutors);

        editLocation = (EditText) view.findViewById(R.id.editLocation);
        editLocation.setOnKeyListener(this);

        actSubject = (AutoCompleteTextView) view.findViewById(R.id.actSubject);
        actSubject.setThreshold(1);
        actSubject.setOnKeyListener(this);

        sbDistance = (SeekBar) view.findViewById(R.id.sbDistance);
        sbDistance.setOnSeekBarChangeListener(this);

        btnAdvanceOption = (Button) view.findViewById(R.id.btnAdvanceOption);
        btnReset = (Button) view.findViewById(R.id.btnReset);
        btnSearch = (Button) view.findViewById(R.id.btnSearch);

        btnAdvanceOption.setOnClickListener(this);
        btnReset.setOnClickListener(this);
        btnSearch.setOnClickListener(this);

        llAdvanceTutor = (LinearLayout) view.findViewById(R.id.llAdvanceTutor);

        if (utility.checkInternetConnection()) {
            if (listSubjects.isEmpty()) {
                loadAllSubjects();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdvanceOption:
                isAdvanceSearch = true;
                llAdvanceTutor.setVisibility(View.VISIBLE);
                btnAdvanceOption.setVisibility(View.GONE);
                break;
            case R.id.btnReset:
                resetAllField();
                break;
            case R.id.btnSearch:
                checkValidation();
                break;
        }
    }

    private void checkValidation() {
        bundle.putSerializable("listSubjects", (Serializable) listSubjects);

        bundle.putString("place", editLocation.getText().toString());
        bundle.putString("subject", actSubject.getText().toString());

        bundle.putInt("distance", sbDistance.getProgress());

        if (sbDistance.getProgress() == 0 && TextUtils.isEmpty(editLocation.getText().toString()))
            utility.setAlertMessage("Please enter location or distance");
        else if (TextUtils.isEmpty(editLocation.getText().toString())) {
            editLocation.setError("Please enter location");
            editLocation.requestFocus();
        } else {
            utility.hideSoftKeyboard();
            TutorSearchResultFragment tutorSearchResultFragment = new TutorSearchResultFragment();
            final FragmentManager fragmentManager = getFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.llTutorSearch, tutorSearchResultFragment, "fragment");
            tutorSearchResultFragment.setArguments(bundle);
            fragmentTransaction.addToBackStack("tutor").commit();
        }
    }

    private void resetAllField() {
        editLocation.setText("");
        sbDistance.setProgress(0);
        actSubject.setText("");
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && isAdvanceSearch && getFragmentManager().getBackStackEntryCount() == 0) {
            btnAdvanceOption.setVisibility(View.VISIBLE);
            isAdvanceSearch = false;
            llAdvanceTutor.setVisibility(View.GONE);
            return true;
        } else
            return false;
    }

    void loadAllSubjects() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("country", Constant.COUNTRY);

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    JSONObject objMessage = jsonObject.getJSONObject("message");
                    JSONArray arraySubject = objMessage.getJSONArray("subjects");
                    for (int i = 0; i < arraySubject.length(); i++) {
                        listSubjects.add(arraySubject.getJSONObject(i).getString("subjectname"));
                    }
                    actAdapter = new ArrayAdapter<>(getActivity(), R.layout.textview_row, listSubjects);
                    actSubject.setAdapter(actAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_GET_ALL_SUBJECT);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (progress == 0)
            textDistance.setText("" + progress + " mile");
        else
            textDistance.setText("" + progress + " miles");
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
