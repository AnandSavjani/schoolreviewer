package com.schoolreviewer.tutor;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;

public class TutorBaseFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_home, null);
        FragmentManager mFragmentManager = getFragmentManager();
        mFragmentManager.popBackStack();
        FragmentTransaction mFragmentTrasaction = mFragmentManager.beginTransaction();
        TutorFragment tutorFragment = new TutorFragment();
        Log.i("base  count", "" + getFragmentManager().getBackStackEntryCount());
        mFragmentTrasaction.add(R.id.flHomeContent, tutorFragment, "fragment").commit();
        return view;
    }
}
