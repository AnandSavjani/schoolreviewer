package com.schoolreviewer.tutor;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;
import com.schoolreviewer.buysell.ContactSellerActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class TutorDetailsFragment extends BaseFragment implements View.OnClickListener {

    private TextView textTutorName, textHeaderAbout, textDistance,
            textHeaderExperience, textExperience, textHeaderAvailability, textAvailability, textHeaderTitle,
            textTutorRate, textDescription, textHeaderSubject, textPrice, textWorkPlace;
    private HashMap<String, String> stringHashMap;
    private Bundle bundle;
    private LinearLayout llAboutTutor, llSubject, llExperience, llAvailability, llSub;
    private ImageView imgTutorPic;
    private ScrollView scrollView;
    private Button btnContactTutorInner, btnContactTutor;
    private String mailID, uid;
    private ImageView imgFilter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return View.inflate(context, R.layout.fragment_tutor_detail, null);
    }

    private void focusOnView(final TextView textView) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                scrollView.smoothScrollTo(0, textView.getTop() - 30);
            }
        });
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initControl(view);
    }

    private void initControl(View view) {
        stringHashMap = new HashMap<>();
        bundle = getArguments();

        uid = bundle.getString("uid");

        textTutorName = (TextView) view.findViewById(R.id.textTutorName);
        textHeaderExperience = (TextView) view.findViewById(R.id.textHeaderExperience);
        textExperience = (TextView) view.findViewById(R.id.textExperience);
        textHeaderAvailability = (TextView) view.findViewById(R.id.textHeaderAvailability);
        textAvailability = (TextView) view.findViewById(R.id.textAvailability);
        textHeaderAbout = (TextView) view.findViewById(R.id.textHeaderAbout);
        textDistance = (TextView) view.findViewById(R.id.textDistance);
        textDescription = (TextView) view.findViewById(R.id.textDescription);
        textTutorRate = (TextView) view.findViewById(R.id.textTutorRate);
        textHeaderSubject = (TextView) view.findViewById(R.id.textHeaderSubject);
        textPrice = (TextView) view.findViewById(R.id.textPrice);
        textWorkPlace = (TextView) view.findViewById(R.id.textWorkPlace);

        textHeaderTitle = (TextView) getActivity().findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(R.string.txt_tutor_profile);

        scrollView = (ScrollView) view.findViewById(R.id.scrollView);

        imgTutorPic = (ImageView) view.findViewById(R.id.imgTutorPic);

        textHeaderExperience.setOnClickListener(this);
        textHeaderAvailability.setOnClickListener(this);
        textHeaderAbout.setOnClickListener(this);
        textHeaderSubject.setOnClickListener(this);

        btnContactTutorInner = (Button) view.findViewById(R.id.btnContactTutorInner);
        btnContactTutor = (Button) view.findViewById(R.id.btnContactTutor);

        btnContactTutorInner.setOnClickListener(this);
        btnContactTutor.setOnClickListener(this);

        imgFilter = (ImageView) getActivity().findViewById(R.id.imgFilter);
        imgFilter.setVisibility(View.GONE);

        llAboutTutor = (LinearLayout) view.findViewById(R.id.llAboutTutor);
        llExperience = (LinearLayout) view.findViewById(R.id.llExperience);
        llAvailability = (LinearLayout) view.findViewById(R.id.llAvailability);
        llSubject = (LinearLayout) view.findViewById(R.id.llSubject);
        llSub = (LinearLayout) view.findViewById(R.id.llSub);

        getTutorDetailsApiCall();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textHeaderAbout:
                focusOnView(textHeaderAbout);
                if (llAboutTutor.getVisibility() == View.VISIBLE) {
                    hideAboutTutor();
                } else {
                    showAboutTutor();
                    hideExperience();
                    hideAvailability();
                    hideSubject();
                }
                break;
            case R.id.textHeaderExperience:
                focusOnView(textHeaderExperience);
                if (llExperience.getVisibility() == View.VISIBLE) {
                    hideExperience();
                } else {
                    showExperience();
                    hideAboutTutor();
                    hideAvailability();
                    hideSubject();
                }
                break;
            case R.id.textHeaderAvailability:
                focusOnView(textHeaderAvailability);
                if (llAvailability.getVisibility() == View.VISIBLE) {
                    hideAvailability();
                } else {
                    showAvailability();
                    hideExperience();
                    hideAboutTutor();
                    hideSubject();
                }
                break;
            case R.id.textHeaderSubject:
                focusOnView(textHeaderSubject);
                if (llSubject.getVisibility() == View.VISIBLE) {
                    hideSubject();
                } else {
                    hideExperience();
                    hideAboutTutor();
                    hideAvailability();
                    showSubject();
                }
                break;
            case R.id.btnContactTutorInner:
//                sendMail();
                startActivity(new Intent(context, ContactSellerActivity.class)
                        .putExtra("uid", uid)
                        .putExtra("api", Constant.URL_ADD_TUTOR_MESSAGE)
                        .putExtra("title", "Contact Tutor"));
                break;
            case R.id.btnContactTutor:
//                sendMail();
                startActivity(new Intent(context, ContactSellerActivity.class)
                        .putExtra("uid", uid)
                        .putExtra("api", Constant.URL_ADD_TUTOR_MESSAGE)
                        .putExtra("title", "Contact Tutor"));
                break;
            default:
                break;
        }
    }

    public void sendMail() {
//        Intent emailIntent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:" + mailID));
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", mailID, null));
        emailIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, mailID);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "From School Reviewer");
        try {
            startActivity(emailIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showSubject() {
        llSubject.setVisibility(View.VISIBLE);
        textHeaderSubject.setCompoundDrawablesWithIntrinsicBounds(R.drawable.sell_arrow_down, 0, R.drawable.subject, 0);
    }

    private void hideSubject() {
        llSubject.setVisibility(View.GONE);
        textHeaderSubject.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_arrow, 0, R.drawable.subject, 0);
    }

    private void showAboutTutor() {
        llAboutTutor.setVisibility(View.VISIBLE);
        textHeaderAbout.setCompoundDrawablesWithIntrinsicBounds(R.drawable.sell_arrow_down, 0, R.drawable.about, 0);
    }

    private void showAvailability() {
        llAvailability.setVisibility(View.VISIBLE);
        textHeaderAvailability.setCompoundDrawablesWithIntrinsicBounds(R.drawable.sell_arrow_down, 0, R.drawable.rate, 0);
    }

    private void hideAvailability() {
        llAvailability.setVisibility(View.GONE);
        textHeaderAvailability.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_arrow, 0, R.drawable.rate, 0);
    }

    private void showExperience() {
        llExperience.setVisibility(View.VISIBLE);
        textHeaderExperience.setCompoundDrawablesWithIntrinsicBounds(R.drawable.sell_arrow_down, 0, R.drawable.qualification, 0);
    }

    private void hideExperience() {
        llExperience.setVisibility(View.GONE);
        textHeaderExperience.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_arrow, 0, R.drawable.qualification, 0);
    }

    private void hideAboutTutor() {
        llAboutTutor.setVisibility(View.GONE);
        textHeaderAbout.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_arrow, 0, R.drawable.about, 0);
    }

    private void getTutorDetailsApiCall() {

        stringHashMap.put("uid", uid);
        stringHashMap.put("lang", "en");

        new AllAPICall(context, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 10) {
                        JSONObject objMessage = jsonObject.getJSONObject("message");

                        textTutorName.setText("Tutor: " + objMessage.getString("firstname"));
                        textHeaderAbout.setText("About " + objMessage.getString("firstname"));

                        textExperience.setText(Html.fromHtml(objMessage.getString("quals")));
                        textAvailability.setText(Html.fromHtml(objMessage.getString("availability").replace("Â", "")));

                        String subjects[] = objMessage.getString("subjects").split(",");
                        for (String subject : subjects) {
                            TextView textView = new TextView(context);
                            textView.setText(subject.trim());
                            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.arrow_txt, 0, 0, 0);
                            textView.setCompoundDrawablePadding(15);
                            textView.setTextColor(Color.BLACK);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            params.setMargins(10, 10, 10, 10);
                            textView.setLayoutParams(params);
                            Typeface font = Typeface.createFromAsset(context.getAssets(), "openSans-Semibold.ttf");
                            textView.setTypeface(font);
                            llSub.addView(textView);
                        }

                        textDescription.setText(Html.fromHtml(objMessage.getString("description")));
                        textDistance.setText(bundle.getString("distance") + " miles");

                        textTutorRate.setText(Constant.POUND + objMessage.getString("minprice") + " p/h");

                        textPrice.setText(Constant.POUND + objMessage.getString("minprice") + " p/h - " + Constant.POUND + objMessage.getString("maxprice") + " p/h");

                        String workICanPlace = "Place I can work : " + objMessage.getString("towns");

                        Spannable wordtoSpan = new SpannableString(workICanPlace);
                        wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_header)), 18, workICanPlace.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                        textWorkPlace.setText(wordtoSpan);

                        mailID = objMessage.getString("email");
                        Glide.with(context).load(Constant.BASE_IMAGE_URL + objMessage.optJSONArray("imageurl").getJSONObject(0).getString("url")).fitCenter().placeholder(R.drawable.app_icon).priority(Priority.HIGH).into(imgTutorPic);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_GET_TUTOR);
    }
}