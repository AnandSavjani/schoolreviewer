package com.schoolreviewer.tutor;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;
import com.schoolreviewer.bean.TutorBean;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.MyUtility;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TutorSearchResultFragment extends BaseFragment implements View.OnClickListener, DrawerLayout.DrawerListener, AdapterView.OnItemClickListener {

    private Bundle bundle;
    private String place, subject;
    private int distance;
    private ListView lvSearchResult;
    private TextView textHeaderTitle;
    private Button btnApplyFilter;
    private RelativeLayout rlSliderLayout;
    private DrawerLayout drawerLayout;
    private String filter = "distance asc";
    private HashMap<String, String> stringHashMap;
    private int page = 1;
    private ArrayAdapter<String> subjectAdapter;
    private TutorSearchResultAdapter tutorSearchResultAdapter;
    private List<String> listSubjects;
    private AutoCompleteTextView actSubject;
    private MyUtility myUtility;
    private List<TutorBean> listTutors;
    int totalpages;
    private ImageView imgFilter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return View.inflate(getActivity(), R.layout.fragment_tutor_search, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initControl(view);
    }

    private void initControl(View view) {
        stringHashMap = new HashMap<>();
        listSubjects = new ArrayList<>();
        listTutors = new ArrayList<>();

        bundle = getArguments();
        myUtility = new MyUtility(getActivity());

        place = bundle.getString("place");
        subject = bundle.getString("subject");
        distance = bundle.getInt("distance", 0);
        listSubjects = (List<String>) bundle.getSerializable("listSubjects");

        drawerLayout = (DrawerLayout) view.findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerLayout.setDrawerListener(this);

        rlSliderLayout = (RelativeLayout) view.findViewById(R.id.right_drawer);

        actSubject = (AutoCompleteTextView) view.findViewById(R.id.actSubject);
        subjectAdapter = new ArrayAdapter<>(getActivity(), R.layout.textview_row, listSubjects);
        actSubject.setAdapter(subjectAdapter);
        actSubject.setThreshold(1);

        lvSearchResult = (ListView) view.findViewById(R.id.lvSearchResult);
        lvSearchResult.setDivider(null);
        tutorSearchResultAdapter = new TutorSearchResultAdapter();
        lvSearchResult.setAdapter(tutorSearchResultAdapter);
        lvSearchResult.setOnItemClickListener(this);

        textHeaderTitle = (TextView) getActivity().findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(getResources().getString(R.string.txt_search_result));

        imgFilter = (ImageView) getActivity().findViewById(R.id.imgFilter);
        imgFilter.setVisibility(View.VISIBLE);
        imgFilter.setOnClickListener(this);

        btnApplyFilter = (Button) view.findViewById(R.id.btnApplyFilter);
        btnApplyFilter.setOnClickListener(this);

        if (myUtility.checkInternetConnection())
            searchTutorApiCall();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgFilter:
                openMenu();
                break;
            case R.id.btnApplyFilter:
                closeMenu();
                if (!TextUtils.isEmpty(actSubject.getText().toString())) {
                    subject = actSubject.getText().toString();
                    page = 1;
                    listTutors.clear();
                    if (myUtility.checkInternetConnection())
                        searchTutorApiCall();
                }
                break;
        }
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
    }

    @Override
    public void onDrawerOpened(View drawerView) {
    }

    @Override
    public void onDrawerClosed(View drawerView) {
    }

    @Override
    public void onDrawerStateChanged(int newState) {
    }

    private void closeMenu() {
        drawerLayout.closeDrawer(rlSliderLayout);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        myUtility.hideSoftKeyboard();
    }

    private void openMenu() {
        drawerLayout.openDrawer(rlSliderLayout);
    }

    private void searchTutorApiCall() {
        stringHashMap.clear();
        stringHashMap.put("pageno", "" + page);
        stringHashMap.put("pagesize", "20");
        stringHashMap.put("order", filter);
        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("lang", "en");

        if (!TextUtils.isEmpty(place))
            stringHashMap.put("place", place);
        if (!TextUtils.isEmpty(subject))
            stringHashMap.put("subject", subject);
        if (distance > 0) {
            stringHashMap.put("distance", "" + distance);
        }

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 30) {
                        JSONObject objMessage = jsonObject.getJSONObject("message");
                        objMessage.getInt("actualend");
                        totalpages = objMessage.getInt("nopages");

                        JSONArray arrayResult = objMessage.getJSONArray("results");
                        if (arrayResult.length() > 0) {
                            for (int i = 0; i < arrayResult.length(); i++) {
                                TutorBean tutorBean = new TutorBean();
                                tutorBean.setUid(arrayResult.getJSONObject(i).getString("uid"));
                                tutorBean.setName(arrayResult.getJSONObject(i).getString("name"));
                                tutorBean.setSubjects(arrayResult.getJSONObject(i).getString("strapline"));
                                tutorBean.setTown(arrayResult.getJSONObject(i).getString("town"));
                                tutorBean.setDistance(arrayResult.getJSONObject(i).getString("distance"));
                                tutorBean.setImageUrl(arrayResult.getJSONObject(i).getJSONArray("imageurl").getJSONObject(0).getString("url"));
                                listTutors.add(tutorBean);
                            }
                        }

                        int position = lvSearchResult.getLastVisiblePosition();
                        lvSearchResult.setSelectionFromTop(position, 0);

                        lvSearchResult.setOnScrollListener(new AbsListView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(AbsListView view, int scrollState) {
                                int threshold = 1;
                                int count = lvSearchResult.getCount();
                                lvSearchResult.setSmoothScrollbarEnabled(true);
                                if (scrollState == SCROLL_STATE_IDLE && totalpages != page && totalpages != 0) {
                                    if (lvSearchResult.getLastVisiblePosition() >= count - threshold) {
                                        page = page + 1;
                                        if (myUtility.checkInternetConnection()) {
                                            searchTutorApiCall();
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                            }
                        });
                    } else {
                        utility.showMessageDialog("Unfortunately there are no matches for your search request at the moment, as we await the database to populate. Please try again another time.");
                        getFragmentManager().popBackStack();
                    }
                    tutorSearchResultAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_SEARCH_TUTOR);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        bundle = new Bundle();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        TutorDetailsFragment tutorDetailsFragment = new TutorDetailsFragment();
        fragmentTransaction.replace(R.id.flTutorSearch, tutorDetailsFragment, "fragment").addToBackStack("tutorlist");
        bundle.putString("uid", listTutors.get(position).getUid());
        bundle.putString("distance", listTutors.get(position).getDistance());
        tutorDetailsFragment.setArguments(bundle);
        fragmentTransaction.commit();
    }

    class TutorSearchResultAdapter extends BaseAdapter {
        ViewHolder viewHolder;

        @Override
        public int getCount() {
            return listTutors.size();
        }

        @Override
        public TutorBean getItem(int position) {
            return listTutors.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = View.inflate(getActivity(), R.layout.row_tutors, null);
                viewHolder.textTutorName = (TextView) convertView.findViewById(R.id.textTutorName);
                viewHolder.textTutorBio = (TextView) convertView.findViewById(R.id.textTutorBio);
                viewHolder.textTutorDistance = (TextView) convertView.findViewById(R.id.textTutorDistance);
                viewHolder.imgTutorPic = (ImageView) convertView.findViewById(R.id.imgTutorPic);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.textTutorName.setText(getItem(position).getName());
            viewHolder.textTutorBio.setText(getItem(position).getSubjects().trim());
            viewHolder.textTutorDistance.setText(getItem(position).getDistance() + " Miles");

            Glide.with(getActivity()).load(Constant.BASE_IMAGE_URL + getItem(position).getImageUrl()).centerCrop().placeholder(R.drawable.app_icon).priority(Priority.HIGH).into(viewHolder.imgTutorPic);
            return convertView;
        }
    }

    class ViewHolder {
        private TextView textTutorName, textTutorBio, textTutorDistance;
        private ImageView imgTutorPic;
    }
}
