package com.schoolreviewer.paypal;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.schoolreviewer.BaseActivity;
import com.schoolreviewer.bean.ItemBean;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.HashMap;

public class PayPalIntegrationActivity extends BaseActivity {

    private HashMap<String, String> stringHashMap;
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
    // sandbox
//    private static final String CONFIG_CLIENT_ID = "Aa3vSnWWxsmK1aFk3BEQkL2cTuGapEsba-oeCtvo5x3tfV5AdnOv2qsNCo18CqEIwi4b5OU0Ba1Rt-1k";
    // live
    private static final String CONFIG_CLIENT_ID = "ATRjM2J-dpNWtMJDmeDQsCzvYtaKvOSHHsTez_AJ3-5UltxO0m8s9LrVIVbG-JKas4dQFfVP4dt4tWGY";

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static PayPalConfiguration config;

    private ItemBean itemBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        itemBean = (ItemBean) getIntent().getSerializableExtra("item");

        config = new PayPalConfiguration()
                .environment(CONFIG_ENVIRONMENT)
                .clientId(CONFIG_CLIENT_ID).acceptCreditCards(false).merchantName("SchoolReviewer").
                        merchantPrivacyPolicyUri(Uri.parse("https://www.paypal.com/webapps/mpp/ua/privacy-full"))
                .merchantUserAgreementUri(Uri.parse("https://www.paypal.com/webapps/mpp/ua/useragreement-full"));

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
        onBuyPressed();
    }

    public void onBuyPressed() {

        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        thingToBuy.enablePayPalShippingAddressesRetrieval(true);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(itemBean.getPrice()), "GBP", itemBean.getTitle(),
                paymentIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i("tag_client", confirm.toJSONObject().toString(4));
                        Log.i("tag", confirm.getPayment().toJSONObject().toString(4));

                        JSONObject jsonObject = new JSONObject(confirm.toJSONObject().toString());
                        if (jsonObject.getString("response_type").equals("payment"))
                            addUserItemAsync("Android", jsonObject.getJSONObject("response").getString("create_time"), jsonObject.getJSONObject("response").getString("id"));

                    } catch (JSONException e) {
                        Log.e("tag", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("tag", "The user canceled.");
                finish();
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Toast.makeText(PayPalIntegrationActivity.this, "The specified amount must be greater than zero", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    private void addUserItemAsync(String platform, String createTime, String paypalId) {
        stringHashMap = new HashMap<>();

        stringHashMap.put("platform", platform);
        stringHashMap.put("create_time", createTime);
        stringHashMap.put("id", paypalId);
        stringHashMap.put("productuid", itemBean.getUid());
        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        stringHashMap.put("country", Constant.COUNTRY);

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                Toast.makeText(PayPalIntegrationActivity.this, "You have purchase exam successfully", Toast.LENGTH_SHORT).show();
                finish();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_PAYPAL_EXAM_PURCHASE);
    }
}
