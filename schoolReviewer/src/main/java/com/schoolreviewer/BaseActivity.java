package com.schoolreviewer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.schoolreviewer.me.LoginActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.MyUtility;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class BaseActivity extends AppCompatActivity {

    public SharedPreferences sharedPreferences;
    public MyUtility utility;

    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_STORAGE = 1;
    public HashMap<String, String> stringHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Mint.initAndStartSession(this, "296bb43c");
        sharedPreferences = getSharedPreferences(Constant.LOGIN_PREFERENCE, MODE_PRIVATE);
        utility = new MyUtility(this);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkCameraPermission();
        }
    }

    void checkCameraPermission() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
        } else if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
            requestInternetPermission();
        } else if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestFineAndCoarseLocation();
        } else if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestReadandWrite();
        } else if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            requestCallPermission();
        } else if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.GET_ACCOUNTS)
                != PackageManager.PERMISSION_GRANTED) {
            requestAccountPermission();
        }
    }

    private void requestCameraPermission() {
        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(BaseActivity.this,
                    new String[]{android.Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        }
    }

    private void requestInternetPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.INTERNET)) {
            ActivityCompat.requestPermissions(BaseActivity.this,
                    new String[]{android.Manifest.permission.INTERNET},
                    3);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.INTERNET},
                    3);
        }
    }

    private void requestAccountPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.GET_ACCOUNTS)) {
            ActivityCompat.requestPermissions(BaseActivity.this,
                    new String[]{android.Manifest.permission.GET_ACCOUNTS},
                    5);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.GET_ACCOUNTS},
                    5);
        }
    }

    private void requestCallPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.CALL_PHONE)) {
            ActivityCompat.requestPermissions(BaseActivity.this,
                    new String[]{android.Manifest.permission.CALL_PHONE},
                    3);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE},
                    3);
        }
    }

    private void requestReadandWrite() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            ActivityCompat.requestPermissions(BaseActivity.this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE);
        } else {

            ActivityCompat.requestPermissions(BaseActivity.this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE);
        }
    }

    private void requestFineAndCoarseLocation() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION)) {

            ActivityCompat.requestPermissions(BaseActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    4);
        } else {

            ActivityCompat.requestPermissions(BaseActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    4);
        }
    }

    public void checkUserKeyAsyncCall(final onTaskComplete onTaskComplete) {
        stringHashMap = new HashMap<>();
        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        if (utility.checkInternetConnection())
            new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
                @Override
                public void onComplete(String response) {
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jObjUserDetail = jsonArray.getJSONObject(0);
                        if (jObjUserDetail.getInt("code") == -1 || jObjUserDetail.getInt("code") == -0) {
                            sharedPreferences.edit().clear().apply();
                            startActivityForResult(new Intent(BaseActivity.this, LoginActivity.class).putExtra("header", getResources().getString(R.string.txt_login)), 1);
                        } else {
                            JSONObject jObjMessage = jObjUserDetail.getJSONObject("message");
                            sharedPreferences.edit().putString("email", jObjMessage.getString("email")).apply();
                            sharedPreferences.edit().putString("displayname", jObjMessage.getString("displayname")).apply();
                            onTaskComplete.onComplete("yes");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_GET_USER_DETAILS);
    }
}
