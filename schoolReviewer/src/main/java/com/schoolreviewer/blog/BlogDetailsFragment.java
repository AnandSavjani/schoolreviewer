package com.schoolreviewer.blog;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;
import com.schoolreviewer.bean.BlogBean;
import com.schoolreviewer.util.Constant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BlogDetailsFragment extends BaseFragment {

    private ImageView imageView;
    private TextView textView, textDate, textTitle;
    private BlogBean data;
    private SimpleDateFormat simpleDateFormat, simpleDateFormat2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return View.inflate(context, R.layout.fragment_blog_details, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initControl(view);
    }

    private void initControl(View view) {
        Constant.flag = 0;
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat2 = new SimpleDateFormat("MMM dd, yyyy");

        textView = (TextView) view.findViewById(R.id.textView);
        textDate = (TextView) view.findViewById(R.id.textDate);
        textTitle = (TextView) view.findViewById(R.id.textTitle);

        imageView = (ImageView) view.findViewById(R.id.imageView);

        data = (BlogBean) getArguments().getSerializable("data");
        assert data != null;
        textView.setText(Html.fromHtml(data.getContent()));
        textView.setMovementMethod(LinkMovementMethod.getInstance());

        String date = data.getDate().substring(0, data.getDate().indexOf("T"));
        try {
            Date date1 = simpleDateFormat.parse(date);
            textDate.setText(simpleDateFormat2.format(date1));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        textTitle.setText(data.getTitle());
        Glide.with(this).load(data.getGuid()).centerCrop().priority(Priority.HIGH).placeholder(R.drawable.buy_placeholder).into(imageView);

    }
}
