package com.schoolreviewer.blog;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;


public class BlogBaseFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_blog, null);
        FragmentManager mFragmentManager = getFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        BlogListFragment blogListFragment = new BlogListFragment();
        getFragmentManager().popBackStack();
        mFragmentTransaction.replace(R.id.flBlogContent, blogListFragment).commit();
        return v;
    }
}
