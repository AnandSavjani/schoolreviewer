package com.schoolreviewer.blog;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;

import com.etsy.android.grid.StaggeredGridView;
import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;
import com.schoolreviewer.adapter.BlogAdapter;
import com.schoolreviewer.bean.BlogBean;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class BlogListFragment extends BaseFragment {

    private StaggeredGridView gvBlog;
    private BlogAdapter blogAdapter;
    private TextView textHeaderTitle;
    private int pageNo = 1;
    private boolean isAllPage;
    private ArrayList<BlogBean> arrayBlog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return View.inflate(context, R.layout.fragment_blog_list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initControl(view);
    }

    private void initControl(View view) {
        arrayBlog = new ArrayList<>();
        Constant.flag = 0;

        textHeaderTitle = (TextView) getActivity().findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(R.string.lbl_blog);

        gvBlog = (StaggeredGridView) view.findViewById(R.id.gvBlog);

        blogAdapter = new BlogAdapter(context, arrayBlog);
        gvBlog.setAdapter(blogAdapter);

        gvBlog.setOnScrollListener(onScrollListener);
        gvBlog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                BlogDetailsFragment blogDetailsFragment = new BlogDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", arrayBlog.get(position));
                blogDetailsFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.flBlogList, blogDetailsFragment);
                fragmentTransaction.addToBackStack("blog");
                fragmentTransaction.commit();
            }
        });

        callBlogListingAPI(1, true);
    }

    public void callBlogListingAPI(int pageNo, boolean isProgress) {
        if (utility.checkInternetConnection()) {
            new AllAPICall(getActivity(), null, null, true, new onTaskComplete() {
                @Override
                public void onComplete(String response) {
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            String title = jsonArray.getJSONObject(i).getString("title");
                            String content = jsonArray.getJSONObject(i).getString("content");
                            JSONObject featured_image = jsonArray.getJSONObject(i).getJSONObject("featured_image");

                            BlogBean objBlog = new BlogBean();
                            objBlog.setTitle(title);
                            objBlog.setContent(content);
                            objBlog.setGuid(featured_image.getString("guid"));
                            objBlog.setDate(jsonArray.getJSONObject(i).getString("date"));

                            arrayBlog.add(objBlog);
                        }

                        if (jsonArray.length() == 0) {
                            isAllPage = true;
                        }
                        blogAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, isProgress).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_BLOG_LIST + pageNo);
        }
    }

    AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            gvBlog.setSmoothScrollbarEnabled(true);
            if (!isAllPage) {
                pageNo = pageNo + 1;
                if (utility.checkInternetConnection()) {
                    callBlogListingAPI(pageNo, false);
                }
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };
}
