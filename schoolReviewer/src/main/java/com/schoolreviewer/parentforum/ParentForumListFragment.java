package com.schoolreviewer.parentforum;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;
import com.schoolreviewer.adapter.ParentForumAdapter;
import com.schoolreviewer.bean.AnswerBean;
import com.schoolreviewer.bean.SchoolQuestionBean;
import com.schoolreviewer.me.LoginActivity;
import com.schoolreviewer.me.RegisterActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ParentForumListFragment extends BaseFragment implements View.OnClickListener {

    private TextView textNewTopic, textHeaderTitle, textNoResult;
    private ListView lvQuestion;
    private ParentForumAdapter parentForumAdapter;
    private HashMap<String, String> stringHashMap;
    private String uid = "", strCategory = "All";
    private List<String> listCategory;
    private ArrayAdapter<String> categoryAdapter;
    private TextView textCategory, textLogin, textRegister;
    private List<SchoolQuestionBean> listQuestionBean;
    private LinearLayout llLoginForum;
    int page = 1, totalpages = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return View.inflate(context, R.layout.activity_forum_list, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initControl(view);
    }

    private void initControl(View view) {
        Constant.flag = 0;
        listCategory = new ArrayList<>();
        listQuestionBean = new ArrayList<>();
        uid = getArguments().getString("uid");

        llLoginForum = (LinearLayout) view.findViewById(R.id.llLoginForum);

        textNewTopic = (TextView) view.findViewById(R.id.textNewTopic);
        textCategory = (TextView) view.findViewById(R.id.textCategory);
        textHeaderTitle = (TextView) getActivity().findViewById(R.id.textHeaderTitle);

        textLogin = (TextView) view.findViewById(R.id.textLogin);
        textRegister = (TextView) view.findViewById(R.id.textRegister);
        textNoResult = (TextView) view.findViewById(R.id.textNoResult);
        textHeaderTitle.setText(getResources().getString(R.string.lbl_parent_forum));

        textNewTopic.setOnClickListener(this);
        textCategory.setOnClickListener(this);
        textLogin.setOnClickListener(this);
        textRegister.setOnClickListener(this);

        parentForumAdapter = new ParentForumAdapter(context, listQuestionBean);

        lvQuestion = (ListView) view.findViewById(R.id.lvQuestion);
        lvQuestion.setAdapter(parentForumAdapter);

        lvQuestion.setOnItemClickListener(onItemClickListener);
        lvQuestion.setOnScrollListener(onScrollListener);

        if (utility.checkInternetConnection()) {
            schoolCategoryApi();
        }

        ImageView imgFilter = (ImageView) getActivity().findViewById(R.id.imgFilter);
        imgFilter.setImageResource(0);

        categoryAdapter = new ArrayAdapter<String>(context, R.layout.search_spinner_item, listCategory) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View mView = super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                mTextView.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                mTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "openSans-Semibold.ttf"));
                mTextView.setBackgroundColor(ContextCompat.getColor(context, R.color.color_header));
                mTextView.setTextSize(18);
                return mView;
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        if (utility.checkInternetConnection()) {
            strCategory = "All";
            listQuestionBean.clear();
            textCategory.setText(R.string.btn_categories);
            page = 1;
            lvQuestion.setSelectionFromTop(0, 0);
            schoolForumApi(page);
        }
        if (sharedPreferences.contains("key")) {
            llLoginForum.setVisibility(View.GONE);
        } else {
            llLoginForum.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.equals(textNewTopic)) {
            if (sharedPreferences.contains("key")) {
                startActivity(new Intent(context, PostTopicActivity.class).putExtra("uid", uid).putExtra("category", (Serializable) listCategory));
            } else {
                startActivityForResult(new Intent(context, LoginActivity.class).putExtra("header", getResources().getString(R.string.txt_login)), 1);
            }
        } else if (v.equals(textCategory)) {
            showCategory();
        } else if (v.equals(textRegister)) {
            startActivity(new Intent(context, RegisterActivity.class));
        } else if (v.equals(textLogin)) {
            startActivityForResult(new Intent(context, LoginActivity.class).putExtra("header", getResources().getString(R.string.txt_login)), 2);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            startActivityForResult(new Intent(context, PostTopicActivity.class).putExtra("uid", uid).putExtra("category", (Serializable) listCategory), 1);
        }
    }

    private void schoolForumApi(int pageNo) {
        stringHashMap = new HashMap<>();
        stringHashMap.put("uid", uid);
        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("lang", "en");
        stringHashMap.put("pageno", "" + pageNo);
        stringHashMap.put("pagesize", "25");
        stringHashMap.put("order", "sequence desc");
        stringHashMap.put("category", strCategory);

        new AllAPICall(context, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    JSONObject objMessage = jsonObject.getJSONObject("message");
                    page = objMessage.getInt("pageno");
                    totalpages = objMessage.getInt("nopages");
                    if (jsonObject.getInt("code") == 32) {
                        List<AnswerBean> listAnswers;

                        JSONArray arrQuestion = objMessage.getJSONArray("questions");
                        for (int i = 0; i < arrQuestion.length(); i++) {
                            SchoolQuestionBean schoolQuestionBean = new SchoolQuestionBean();
                            JSONObject objQuestion = arrQuestion.getJSONObject(i);

                            schoolQuestionBean.setUid(objQuestion.getString("uid"));
                            schoolQuestionBean.setUidaskedby(objQuestion.getString("uidaskedby"));
                            schoolQuestionBean.setTitle(objQuestion.getString("title"));
                            schoolQuestionBean.setQuestion(objQuestion.getString("question"));
                            schoolQuestionBean.setDisplayname(objQuestion.getString("displayname"));
                            schoolQuestionBean.setAskedat(objQuestion.getString("askedat"));
                            schoolQuestionBean.setCategory(objQuestion.getString("category"));

                            JSONArray arrayAnswer = objQuestion.getJSONArray("answers");
                            listAnswers = new ArrayList<>();
                            for (int j = 0; j < arrayAnswer.length(); j++) {
                                JSONObject objAnswer = arrayAnswer.getJSONObject(j);
                                AnswerBean answerBean = new AnswerBean();

                                answerBean.setUid(objAnswer.getString("uid"));
                                answerBean.setUidansweredby(objAnswer.getString("uidansweredby"));
                                answerBean.setDisplayname(objAnswer.getString("displayname"));
                                answerBean.setAnsweredat(objAnswer.getString("answeredat"));
                                answerBean.setAnswer(objAnswer.getString("answer"));
                                answerBean.setCategory(objAnswer.getString("category"));

                                listAnswers.add(answerBean);
                            }
                            schoolQuestionBean.setListAnswerBean(listAnswers);
                            listQuestionBean.add(schoolQuestionBean);
                        }
                        if (arrQuestion.length() > 0) {
                            textNoResult.setVisibility(View.GONE);
                            lvQuestion.setVisibility(View.VISIBLE);
                        } else {
                            textNoResult.setVisibility(View.VISIBLE);
                            lvQuestion.setVisibility(View.GONE);
                        }
                    } else {
                        Toast.makeText(context, "Please try again later", Toast.LENGTH_SHORT).show();
                    }
                    parentForumAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, Constant.URL_GET_SCHOOL_FORUM);
    }

    private void schoolCategoryApi() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("lang", "en");
        stringHashMap.put("pageno", "0");
        stringHashMap.put("pagesize", "25");
        stringHashMap.put("order", "sequence asc");
        stringHashMap.put("category", "All");

        new AllAPICall(context, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 33) {
                        JSONArray arrCategories = jsonObject.getJSONObject("message").getJSONArray("categories");
                        for (int i = 0; i < arrCategories.length(); i++) {
                            listCategory.add(arrCategories.get(i).toString());
                        }
                    }
                    categoryAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, Constant.URL_GET_SCHOOL_FORUM);
    }

    private void showCategory() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setAdapter(categoryAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        textCategory.setText(listCategory.get(which));
                        strCategory = listCategory.get(which);
                        if (utility.checkInternetConnection()) {
                            listQuestionBean.clear();
                            page = 1;
                            schoolForumApi(page);
                        }
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.color_header);
            }
        });
        alertDialog.show();
    }

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(context, ForumCommentActivity.class);
            intent.putExtra("question", listQuestionBean.get(position));
            intent.putExtra("uidtopic", listQuestionBean.get(position).getUid());
            intent.putExtra("uidSchool", uid);
            startActivity(intent);
        }
    };

    AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            int threshold = 1;
            int count = lvQuestion.getCount();
            lvQuestion.setSmoothScrollbarEnabled(true);
            if (scrollState == SCROLL_STATE_IDLE && totalpages != page && totalpages != 0) {
                if (lvQuestion.getLastVisiblePosition() >= count - threshold) {
                    page = page + 1;
                    if (utility.checkInternetConnection()) {
                        schoolForumApi(page);
                    }
                }
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };
}