package com.schoolreviewer.parentforum;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.schoolreviewer.BaseActivity;
import com.schoolreviewer.R;
import com.schoolreviewer.me.LoginActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class PostCommentActivity extends BaseActivity implements View.OnClickListener {

    private TextView textPostTitle, textHeaderTitle;
    private EditText editComment;
    private Button btnPostComment;
    //    private SchoolQuestionBean questionBean;
    private HashMap<String, String> stringHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_comment);
        initControl();
    }

    private void initControl() {
//        questionBean = (SchoolQuestionBean) getIntent().getSerializableExtra("question");
        stringHashMap = new HashMap<>();

        textPostTitle = (TextView) findViewById(R.id.textPostTitle);
        textHeaderTitle = (TextView) findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(getResources().getString(R.string.lbl_parent_forum).toUpperCase());

        textPostTitle.setText(getIntent().getStringExtra("title"));
        textPostTitle.setMovementMethod(new ScrollingMovementMethod());

        btnPostComment = (Button) findViewById(R.id.btnPostComment);
        editComment = (EditText) findViewById(R.id.editComment);

        btnPostComment.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnPostComment)) {
            checkValidation();
        }
    }

    private void checkValidation() {
        if (TextUtils.isEmpty(editComment.getText().toString())) {
            utility.showMessageDialog("Please post your comment");
        } else {
            if (sharedPreferences.contains("key")) {
                if (utility.checkInternetConnection()) {
                    postComment();
                }
            } else {
                startActivity(new Intent(this, LoginActivity.class).putExtra("header", getResources().getString(R.string.txt_login)));
            }
        }
    }

    private void postComment() {
        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        stringHashMap.put("uid", getIntent().getStringExtra("uidasked"));
        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("uidquestion", getIntent().getStringExtra("uid"));
        stringHashMap.put("answer", editComment.getText().toString());
        stringHashMap.put("displayname", "yes");

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 24) {
                        Toast.makeText(PostCommentActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK);
                        finish();
                    } else if (jsonObject.getInt("code") == -1 || jsonObject.getInt("code") == -0) {
                        startActivity(new Intent(PostCommentActivity.this, LoginActivity.class));
                    } else {
                        Toast.makeText(PostCommentActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_ADD_ANSWER);
    }
}
