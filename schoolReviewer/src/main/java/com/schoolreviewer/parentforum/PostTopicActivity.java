package com.schoolreviewer.parentforum;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.schoolreviewer.BaseActivity;
import com.schoolreviewer.R;
import com.schoolreviewer.adapter.HintAdapter;
import com.schoolreviewer.me.LoginActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PostTopicActivity extends BaseActivity implements View.OnClickListener {

    private EditText editTitle, editComment;
    private Button btnPostTopic;
    private Spinner spCategory;
    private TextView textHeader;
    private String uid = "";
    private List<String> listCategory;
    private HashMap<String, String> stringHashMap;
    private HintAdapter hintAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_topic);
        initControl();
    }

    private void initControl() {
        uid = getIntent().getStringExtra("uid");
        listCategory = new ArrayList<>();
        listCategory = (List<String>) getIntent().getSerializableExtra("category");
//        listCategory.addAll((List<String>) getIntent().getSerializableExtra("category"));
        listCategory.add("Select Category");
//        listCategory.remove("All");

        textHeader = (TextView) findViewById(R.id.textHeaderTitle);
        textHeader.setText(getResources().getString(R.string.lbl_parent_forum).toUpperCase());

        editTitle = (EditText) findViewById(R.id.editTitle);
        editComment = (EditText) findViewById(R.id.editComment);

        btnPostTopic = (Button) findViewById(R.id.btnPostTopic);
        btnPostTopic.setOnClickListener(this);

        spCategory = (Spinner) findViewById(R.id.spCategory);

        hintAdapter = new HintAdapter(this, R.layout.search_spinner_item, listCategory);
        spCategory.setAdapter(hintAdapter);

        spCategory.setSelection(hintAdapter.getCount());
    }

    private void checkValidation() {
        if (TextUtils.isEmpty(editTitle.getText().toString())) {
            editTitle.setError("Please enter title");
            editTitle.requestFocus();
        } else if (TextUtils.isEmpty(editComment.getText().toString())) {
            editComment.setError("Please enter comment");
            editComment.requestFocus();
        } else {
            if (utility.checkInternetConnection()) {
                if (sharedPreferences.contains("key"))
                    postTopicAPICall();
            }
        }
    }

    private void postTopicAPICall() {

        stringHashMap = new HashMap<>();

        stringHashMap.put("uid", uid);
        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        stringHashMap.put("title", editTitle.getText().toString());
        stringHashMap.put("question", editComment.getText().toString());

        if (spCategory.getSelectedItemPosition() == listCategory.size() - 1) {
            stringHashMap.put("category", "General");
        } else
            stringHashMap.put("category", spCategory.getSelectedItem().toString());

        stringHashMap.put("country", Constant.COUNTRY);

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 23) {
                        finish();
                        setResult(RESULT_OK);
                        Toast.makeText(PostTopicActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    } else {
                        startActivityForResult(new Intent(PostTopicActivity.this, LoginActivity.class).putExtra("header", getResources().getString(R.string.txt_login)), 1);
                        Toast.makeText(PostTopicActivity.this, "Please login for Post Topic", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_ADD_QUESTION);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnPostTopic)) {
            checkValidation();
        }
    }
}
