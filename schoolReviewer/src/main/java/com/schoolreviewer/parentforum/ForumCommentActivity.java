package com.schoolreviewer.parentforum;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.schoolreviewer.BaseActivity;
import com.schoolreviewer.R;
import com.schoolreviewer.adapter.ForumCommentAdapter;
import com.schoolreviewer.bean.AnswerBean;
import com.schoolreviewer.bean.SchoolQuestionBean;
import com.schoolreviewer.me.LoginActivity;
import com.schoolreviewer.me.RegisterActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ForumCommentActivity extends BaseActivity implements View.OnClickListener {

    private TextView textTitle, textHeaderTitle, textNoResult, textUserName, textSchoolType,
            textTime, textComment;
    private ListView lvComments;
    private ForumCommentAdapter forumCommentAdapter;
    private Button btnLeaveComment;
    private SchoolQuestionBean schoolQuestionBean;
    private LinearLayout llLoginForum;
    private TextView textLogin, textRegister;
    private DateFormat inputFormatter1, outputFormatter1;
    private String schoolUid, uidtopic;
    private List<AnswerBean> listAnswerBean;
    private ImageView imgFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum_comment);
        initControl();
    }

    private void initControl() {
        schoolUid = getIntent().getStringExtra("uidSchool");
        uidtopic = getIntent().getStringExtra("uidtopic");

        listAnswerBean = new ArrayList<>();
//        schoolQuestionBean = (SchoolQuestionBean) getIntent().getSerializableExtra("question");

        inputFormatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        outputFormatter1 = new SimpleDateFormat("dd MMM", Locale.getDefault());

        textTitle = (TextView) findViewById(R.id.textTitle);
        textHeaderTitle = (TextView) findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(getResources().getString(R.string.lbl_parent_forum).toUpperCase());

        textNoResult = (TextView) findViewById(R.id.textNoResult);
        textLogin = (TextView) findViewById(R.id.textLogin);
        textRegister = (TextView) findViewById(R.id.textRegister);
        textUserName = (TextView) findViewById(R.id.textUserName);
        textSchoolType = (TextView) findViewById(R.id.textSchoolType);
        textTime = (TextView) findViewById(R.id.textTime);
        textComment = (TextView) findViewById(R.id.textComment);

        textTitle.setMovementMethod(new ScrollingMovementMethod());

        textLogin.setOnClickListener(this);
        textRegister.setOnClickListener(this);

        btnLeaveComment = (Button) findViewById(R.id.btnLeaveComment);
        btnLeaveComment.setOnClickListener(this);

        imgFlag = (ImageView) findViewById(R.id.imgFlag);


        imgFlag.setOnClickListener(this);

        lvComments = (ListView) findViewById(R.id.rvComments);
        llLoginForum = (LinearLayout) findViewById(R.id.llLoginForum);

        forumCommentAdapter = new ForumCommentAdapter(ForumCommentActivity.this, listAnswerBean);
        lvComments.setAdapter(forumCommentAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.contains("key")) {
            llLoginForum.setVisibility(View.GONE);
            btnLeaveComment.setVisibility(View.VISIBLE);
        } else {
            llLoginForum.setVisibility(View.VISIBLE);
            btnLeaveComment.setVisibility(View.GONE);
        }

        if (utility.checkInternetConnection()) {
            listAnswerBean.clear();
            getQuestionComment();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnLeaveComment)) {
            if (sharedPreferences.contains("key")) {
                Intent intent = new Intent(this, PostCommentActivity.class);
                intent.putExtra("uidasked", schoolQuestionBean.getUidaskedby());
                intent.putExtra("uid", schoolQuestionBean.getUid());
                intent.putExtra("title", schoolQuestionBean.getTitle());
                startActivity(intent);
            } else {
                startActivity(new Intent(this, LoginActivity.class).putExtra("header", getResources().getString(R.string.txt_login)));
            }
        } else if (v.equals(textRegister)) {
            startActivity(new Intent(this, RegisterActivity.class));
        } else if (v.equals(textLogin)) {
            startActivityForResult(new Intent(this, LoginActivity.class).putExtra("header", getResources().getString(R.string.txt_login)), 2);
        }
    }

    public void setListViewHeight(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0) {
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ActionBar.LayoutParams.WRAP_CONTENT));
            }
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void getQuestionComment() {
        HashMap<String, String> stringHashMap = new HashMap<>();

        stringHashMap.put("uidtopic", uidtopic);
        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("lang", "en");
        stringHashMap.put("pageno", "1");
        stringHashMap.put("pagesize", "2500");
        stringHashMap.put("order", "sequence desc");

        if (schoolUid.equalsIgnoreCase(""))
            stringHashMap.put("uid", "national");
        else
            stringHashMap.put("uid", schoolUid);

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    JSONObject objMessage = jsonObject.getJSONObject("message");
                    if (jsonObject.getInt("code") == 32) {

                        JSONArray arrQuestion = objMessage.getJSONArray("questions");
                        for (int i = 0; i < arrQuestion.length(); i++) {
                            schoolQuestionBean = new SchoolQuestionBean();
                            JSONObject objQuestion = arrQuestion.getJSONObject(i);

                            schoolQuestionBean.setUid(objQuestion.getString("uid"));
                            schoolQuestionBean.setUidaskedby(objQuestion.getString("uidaskedby"));
                            schoolQuestionBean.setTitle((objQuestion.getString("title")));

                            textTitle.setText(Html.fromHtml(objQuestion.getString("title")));
                            textComment.setText(Html.fromHtml(objQuestion.getString("question")));
                            textUserName.setText(objQuestion.getString("displayname"));
                            textSchoolType.setText(objQuestion.getString("category"));

                            try {
                                Date date1 = inputFormatter1.parse(objQuestion.getString("askedat"));
                                String output1 = outputFormatter1.format(date1);
                                textTime.setText(output1);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            JSONArray arrayAnswer = objQuestion.getJSONArray("answers");
                            for (int j = 0; j < arrayAnswer.length(); j++) {
                                JSONObject objAnswer = arrayAnswer.getJSONObject(j);
                                AnswerBean answerBean = new AnswerBean();

                                answerBean.setUid(objAnswer.getString("uid"));
                                answerBean.setUidansweredby(objAnswer.getString("uidansweredby"));
                                answerBean.setDisplayname(objAnswer.getString("displayname"));
                                answerBean.setAnsweredat(objAnswer.getString("answeredat"));
                                answerBean.setAnswer(objAnswer.getString("answer"));
                                answerBean.setCategory(objAnswer.getString("category"));

                                listAnswerBean.add(answerBean);
                            }
                        }

                        forumCommentAdapter.notifyDataSetChanged();
                        setListViewHeight(lvComments);

                        if (listAnswerBean.isEmpty()) {
                            lvComments.setVisibility(View.GONE);
                            textNoResult.setVisibility(View.VISIBLE);
                        } else {
                            lvComments.setVisibility(View.VISIBLE);
                            textNoResult.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_GET_SCHOOL_FORUM);
    }
}
