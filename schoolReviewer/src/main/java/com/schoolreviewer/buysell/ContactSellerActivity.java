package com.schoolreviewer.buysell;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.schoolreviewer.BaseActivity;
import com.schoolreviewer.R;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ContactSellerActivity extends BaseActivity implements View.OnClickListener {

    private EditText editName, editEmail, editContactNumber, editMessage;
    private CheckBox cbAgree;
    private Button btnSendMessage;
    private TextView textHeaderTitle;
    private HashMap<String, String> stringHashMap;
    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_seller);
        initControl();
    }

    private void initControl() {
        title = getIntent().getStringExtra("title");

        editName = (EditText) findViewById(R.id.editName);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editContactNumber = (EditText) findViewById(R.id.editContactNumber);
        editMessage = (EditText) findViewById(R.id.editMessage);

        cbAgree = (CheckBox) findViewById(R.id.cbAgree);

        textHeaderTitle = (TextView) findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(title);

        btnSendMessage = (Button) findViewById(R.id.btnSendMessage);
        btnSendMessage.setOnClickListener(this);

        if (utility.checkInternetConnection())
            checkUserKeyAsyncCall(new onTaskComplete() {
                @Override
                public void onComplete(String response) {
                    editName.setText(sharedPreferences.getString("displayname", ""));
                    editEmail.setText(sharedPreferences.getString("email", ""));
                }
            });


        if (title.contains("SELLER")) {
            cbAgree.setText(R.string.lbl_agree_share);
        } else {
            cbAgree.setText("I agree to share these details with the tutor");
        }
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnSendMessage)) {
            checkValidation();
        }
    }

    private void checkValidation() {
        if (TextUtils.isEmpty(editName.getText().toString())) {
            editName.setError("Please enter name");
            editName.requestFocus();
        } else if (TextUtils.isEmpty(editEmail.getText().toString())) {
            editEmail.setError("Please enter email");
            editEmail.requestFocus();
        } else if (TextUtils.isEmpty(editMessage.getText().toString())) {
            editMessage.requestFocus();
            editMessage.setError("Please enter message");
        } else if (!cbAgree.isChecked()) {
            utility.setAlertMessage("Please agree to share these details");
        } else {
            if (utility.checkInternetConnection())
                contactSellerApiCall();
        }
    }

    private void contactSellerApiCall() {
        stringHashMap = new HashMap<>();

        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("email", editEmail.getText().toString());
        stringHashMap.put("phone", editContactNumber.getText().toString());
        stringHashMap.put("message", editMessage.getText().toString());
        stringHashMap.put("uid", getIntent().getStringExtra("uid"));

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 36) {
                        Toast.makeText(ContactSellerActivity.this, "Message send successfully!", Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        Toast.makeText(ContactSellerActivity.this, "Please try again later", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getIntent().getStringExtra("api"));
    }
}
