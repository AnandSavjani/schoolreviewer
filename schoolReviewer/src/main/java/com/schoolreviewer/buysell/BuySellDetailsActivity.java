package com.schoolreviewer.buysell;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.schoolreviewer.BaseActivity;
import com.schoolreviewer.R;
import com.schoolreviewer.adapter.ViewPageImageAdapter;
import com.schoolreviewer.bean.ItemBean;
import com.schoolreviewer.me.LoginActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class BuySellDetailsActivity extends BaseActivity implements View.OnClickListener {

    private TextView textTitle, textCondition, textSeller,
            textDescription, textProductPrice, textHeaderTitle;
    private Button btnContactSeller;
    private ItemBean itemBean;
    private AutoScrollViewPager vpImageSlider;
    private ViewPageImageAdapter vpAdapter;
    private LinearLayout llSell;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_sell_details);
        initControl();
    }

    private void initControl() {
        itemBean = (ItemBean) getIntent().getSerializableExtra("item");

        textTitle = (TextView) findViewById(R.id.textTitle);
        textCondition = (TextView) findViewById(R.id.textCondition);
        textSeller = (TextView) findViewById(R.id.textSeller);
        textDescription = (TextView) findViewById(R.id.textDescription);
        textProductPrice = (TextView) findViewById(R.id.textProductPrice);
        textHeaderTitle = (TextView) findViewById(R.id.textHeaderTitle);

        textHeaderTitle.setText(R.string.lbl_buy_sell);

        btnContactSeller = (Button) findViewById(R.id.btnContactSeller);

        btnContactSeller.setOnClickListener(this);

        vpImageSlider = (AutoScrollViewPager) findViewById(R.id.vpImageSlider);

        textTitle.setText(itemBean.getTitle());
        textCondition.setText("Condition: " + itemBean.getCondition());
        textSeller.setText("Seller: " + itemBean.getSeller());
        textDescription.setText(Html.fromHtml(itemBean.getDescription()));
        textProductPrice.setText(Constant.POUND + itemBean.getPrice());

        llSell = (LinearLayout) findViewById(R.id.llSell);
        if (getIntent().getBooleanExtra("isFromUser", true)) {
            llSell.setVisibility(View.GONE);
        }

        initImageSlider();
    }

    void initImageSlider() {
        vpAdapter = new ViewPageImageAdapter(this, itemBean.getListImages(), true);
        vpImageSlider.setInterval(5000);
        vpImageSlider.startAutoScroll();
        vpImageSlider.setCurrentItem(0, true);
        vpImageSlider.setCycle(true);
        vpImageSlider.setDirection(AutoScrollViewPager.RIGHT);
        vpImageSlider.setAutoScrollDurationFactor(3);
        vpImageSlider.setAdapter(vpAdapter);
        if (itemBean.getListImages().isEmpty())
            vpImageSlider.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.image_border));
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnContactSeller)) {
            if (!sharedPreferences.contains("key")) {
                startActivityForResult(new Intent(this, LoginActivity.class).putExtra("header", getResources().getString(R.string.txt_login)), 2);
            } else {
                startActivity(new Intent(BuySellDetailsActivity.this, ContactSellerActivity.class)
                        .putExtra("title", getResources().getString(R.string.btn_contact_seller))
                        .putExtra("api", Constant.URL_ADD_ADVERT_MESSAGE)
                        .putExtra("uid", itemBean.getUid()));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && resultCode == RESULT_OK) {
            startActivityForResult(new Intent(BuySellDetailsActivity.this, ContactSellerActivity.class)
                    .putExtra("title", getResources().getString(R.string.btn_contact_seller))
                    .putExtra("api", Constant.URL_ADD_ADVERT_MESSAGE)
                    .putExtra("uid", itemBean.getUid()), 5);
        } else if (requestCode == 5 && resultCode == RESULT_OK) {
            finish();
        }
    }

    private void paypalItemPurchased(String paypalId) {
        HashMap<String, String> stringHashMap = new HashMap<>();
        stringHashMap.put("platform", "Android");
        stringHashMap.put("create_time", "" + new Date().getTime());
        stringHashMap.put("id", paypalId);
        stringHashMap.put("productuid", itemBean.getUid());
        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        stringHashMap.put("country", Constant.COUNTRY);

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 46) {
                        confirmationDialog("Thank you for your purchase," + itemBean.getSeller() + " will be in touch soon");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_PAYPAL_ITEM_PURCHASE);
    }

    public void confirmationDialog(String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setPositiveButton("OK", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        if (!isFinishing() && message.length() > 0) {
            adb.show();
        }
    }
}
