package com.schoolreviewer.buysell;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.schoolreviewer.BaseActivity;
import com.schoolreviewer.R;
import com.schoolreviewer.SplashActivity;
import com.schoolreviewer.bean.ImagesBean;
import com.schoolreviewer.bean.ItemBean;
import com.schoolreviewer.me.LoginActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.OpenGallery;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SellItemActivity extends BaseActivity implements View.OnClickListener, View.OnTouchListener {

    private EditText editProductTitle, editPrice, editDescription, editContactNumber, editName, editPostcode, editTown;
    private Spinner spCategory, spCondition;
    private Button btnUploadPhoto, btnCancel, btnListItem, btnDeleteItem, btnItemSold;
    private TextView textHeaderTitle;
    private HashMap<String, String> stringHashMap;
    private String uid = "";
    private ArrayAdapter<String> categoryAdapter, conditionAdapter;
    private List<String> listCategory;
    private ImageView imgProduct1, imgProduct2, imgProduct3, imgClose1, imgClose2, imgClose3;
    private HashMap<String, File> fileHashMap;
    private LinearLayout llEditItem;
    private boolean isUpdate = false;
    private ItemBean itembean;
    private List<ImagesBean> listImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_item);
        initControl();
    }

    private void initControl() {
        isUpdate = getIntent().getBooleanExtra("isupdate", false);

        listImages = new ArrayList<>();
        listCategory = new ArrayList<>();
        fileHashMap = new HashMap<>();

        uid = getIntent().getStringExtra("uid");
        listCategory.addAll(SplashActivity.listCategory);
        listCategory.remove("All");

        llEditItem = (LinearLayout) findViewById(R.id.llEditItem);

        editProductTitle = (EditText) findViewById(R.id.editProductTitle);
        editPrice = (EditText) findViewById(R.id.editPrice);
        editDescription = (EditText) findViewById(R.id.editDescription);
        editContactNumber = (EditText) findViewById(R.id.editContactNumber);
        editName = (EditText) findViewById(R.id.editName);
        editPostcode = (EditText) findViewById(R.id.editPostcode);
        editTown = (EditText) findViewById(R.id.editTown);

        textHeaderTitle = (TextView) findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(getResources().getString(R.string.btn_sell_an_item).toUpperCase());

        spCategory = (Spinner) findViewById(R.id.spCategory);
        spCondition = (Spinner) findViewById(R.id.spCondition);

        categoryAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, listCategory);
        conditionAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.array_conditions));

        spCategory.setAdapter(categoryAdapter);
        spCondition.setAdapter(conditionAdapter);

        spCategory.setOnTouchListener(this);
        spCondition.setOnTouchListener(this);

        btnUploadPhoto = (Button) findViewById(R.id.btnUploadPhoto);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnListItem = (Button) findViewById(R.id.btnListItem);
        btnDeleteItem = (Button) findViewById(R.id.btnDeleteItem);
        btnItemSold = (Button) findViewById(R.id.btnItemSold);

        btnUploadPhoto.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnListItem.setOnClickListener(this);
        btnDeleteItem.setOnClickListener(this);
        btnItemSold.setOnClickListener(this);

        imgProduct1 = (ImageView) findViewById(R.id.imgProduct1);
        imgProduct2 = (ImageView) findViewById(R.id.imgProduct2);
        imgProduct3 = (ImageView) findViewById(R.id.imgProduct3);

        imgClose1 = (ImageView) findViewById(R.id.imgClose1);
        imgClose2 = (ImageView) findViewById(R.id.imgClose2);
        imgClose3 = (ImageView) findViewById(R.id.imgClose3);

        imgClose1.setOnClickListener(this);
        imgClose2.setOnClickListener(this);
        imgClose3.setOnClickListener(this);

        if (isUpdate) {
            llEditItem.setVisibility(View.VISIBLE);
            textHeaderTitle.setText(R.string.lbl_edit_item);
            btnListItem.setText(R.string.btn_relist_item);
            feelUpAllValues();
        }
        if (sharedPreferences.contains("displayname"))
            editName.setText(sharedPreferences.getString("displayname", ""));
    }

    private void feelUpAllValues() {
        itembean = (ItemBean) getIntent().getSerializableExtra("itembean");
        editProductTitle.setText(itembean.getTitle());
        spCondition.setSelection(conditionAdapter.getPosition(itembean.getCondition()));
        editPrice.setText(itembean.getPrice());
        spCategory.setSelection(categoryAdapter.getPosition(itembean.getCategory()));
        editDescription.setText(Html.fromHtml(itembean.getDescription()));
        editName.setText(itembean.getSeller());
        editContactNumber.setText(itembean.getContactphone());

        listImages = itembean.getListImages();

        setImagesValues();

        if (itembean.getSold().equals("1")) {
            btnItemSold.setText(R.string.btn_mark_item_unsold);
        } else {
            btnItemSold.setText(R.string.btn_mark_item);
        }
    }

    private void setImagesValues() {
        imgClose1.setVisibility(View.INVISIBLE);
        imgClose2.setVisibility(View.INVISIBLE);
        imgClose3.setVisibility(View.INVISIBLE);

        Glide.with(this).load(R.drawable.buy_placeholder).centerCrop().into(imgProduct1);
        Glide.with(this).load(R.drawable.buy_placeholder).centerCrop().into(imgProduct2);
        Glide.with(this).load(R.drawable.buy_placeholder).centerCrop().into(imgProduct3);
        try {
            if (listImages.size() == 1) {
                imgClose1.setVisibility(View.VISIBLE);
            } else if (listImages.size() == 2) {
                imgClose1.setVisibility(View.VISIBLE);
                imgClose2.setVisibility(View.VISIBLE);
            } else if (listImages.size() == 3) {
                imgClose1.setVisibility(View.VISIBLE);
                imgClose2.setVisibility(View.VISIBLE);
                imgClose3.setVisibility(View.VISIBLE);
            }

            Glide.with(this).load(listImages.get(0).getUrl()).placeholder(R.drawable.app_icon).priority(Priority.HIGH).centerCrop().into(imgProduct1);
            Glide.with(this).load(listImages.get(1).getUrl()).placeholder(R.drawable.app_icon).priority(Priority.HIGH).centerCrop().into(imgProduct2);
            Glide.with(this).load(listImages.get(2).getUrl()).placeholder(R.drawable.app_icon).priority(Priority.HIGH).centerCrop().into(imgProduct3);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnUploadPhoto)) {
            if (listImages.size() >= 3 || fileHashMap.size() >= 3) {
                utility.showMessageDialog("Maximum 3 photos allowed");
            } else
                startActivityForResult(new Intent(SellItemActivity.this, OpenGallery.class), 1);
        } else if (v.equals(btnCancel)) {
            finish();
        } else if (v == btnListItem) {
            checkValidation();
        } else if (v == btnDeleteItem) {
            deleteItemConfirmDialog("Are you sure you want to delete this item ?");
        } else if (v == btnItemSold) {
            if (utility.checkInternetConnection()) {
                if (itembean.getSold().equals("1")) {
                    markUnmarkAsSoldAsyncCall(Constant.URL_UPDATE_ITEM_AS_UNSOLD);
                } else {
                    markUnmarkAsSoldAsyncCall(Constant.URL_UPDATE_ITEM_AS_SOLD);
                }
            }
        } else if (v == imgClose1) {
            if (utility.checkInternetConnection())
                deleteConfirmationDialog(0, itembean.getListImages().get(0).getUid(), imgClose1, imgProduct1);

        } else if (v == imgClose2) {
            if (utility.checkInternetConnection())
                deleteConfirmationDialog(1, itembean.getListImages().get(1).getUid(), imgClose2, imgProduct2);

        } else if (v == imgClose3) {
            if (utility.checkInternetConnection())
                deleteConfirmationDialog(2, itembean.getListImages().get(2).getUid(), imgClose3, imgProduct3);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1) {
            if (utility.resize(data.getStringExtra("picturepath")) != null) {
                if (listImages.size() == 0) {
                    if (fileHashMap.size() == 0) {
                        fileHashMap.put("image1", utility.resize(data.getStringExtra("picturepath")));
                        Glide.with(this).load(fileHashMap.get("image1")).into(imgProduct1);
                    } else if (fileHashMap.size() == 1) {
                        fileHashMap.put("image2", utility.resize(data.getStringExtra("picturepath")));
                        Glide.with(this).load(fileHashMap.get("image2")).into(imgProduct2);
                    } else if (fileHashMap.size() == 2) {
                        fileHashMap.put("image3", utility.resize(data.getStringExtra("picturepath")));
                        Glide.with(this).load(fileHashMap.get("image3")).into(imgProduct3);
                    }
                } else if (listImages.size() == 1) {
                    fileHashMap.put("image2", utility.resize(data.getStringExtra("picturepath")));
                    Glide.with(this).load(fileHashMap.get("image2")).into(imgProduct2);
                } else if (listImages.size() == 2) {
                    fileHashMap.put("image3", utility.resize(data.getStringExtra("picturepath")));
                    Glide.with(this).load(fileHashMap.get("image3")).into(imgProduct3);
                }
                if (itembean != null) {
                    ImagesBean imagesBean = new ImagesBean();
                    imagesBean.setUrl(data.getStringExtra("picturepath"));
                    imagesBean.setUid("null");
                    listImages.add(imagesBean);
                }
            } else
                utility.showMessageDialog("Unable to fetch image.Please select another image");
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (!fileHashMap.isEmpty() || fileHashMap != null) {
            if (fileHashMap.size() == 1)
                Glide.with(this).load(fileHashMap.get("image1")).placeholder(R.drawable.buy_placeholder).fitCenter().into(imgProduct1);
            if (fileHashMap.size() == 2)
                Glide.with(this).load(fileHashMap.get("image2")).placeholder(R.drawable.buy_placeholder).fitCenter().into(imgProduct2);
            if (fileHashMap.size() == 3)
                Glide.with(this).load(fileHashMap.get("image3")).placeholder(R.drawable.buy_placeholder).fitCenter().into(imgProduct3);
        }
    }

    //methods

    private void addImageAsync(String productId) {
        stringHashMap = new HashMap<>();

        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        stringHashMap.put("uid", productId);
        stringHashMap.put("country", Constant.COUNTRY);

        new AllAPICall(this, stringHashMap, fileHashMap, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                if (!isUpdate)
                    Toast.makeText(SellItemActivity.this, "Item added successfully", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(SellItemActivity.this, "Item updated successfully", Toast.LENGTH_SHORT).show();
                finish();
            }
        }, false).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, Constant.URL_ADD_IMAGE_FOR_SELL);
    }

    private void deleteImage(final int pos, String imgUid) {
        stringHashMap = new HashMap<>();

        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        stringHashMap.put("uid", imgUid);
        stringHashMap.put("country", Constant.COUNTRY);

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 45) {
                        listImages.remove(pos);
                        setImagesValues();
                        Toast.makeText(SellItemActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SellItemActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_DELETE_IMAGE_FOR_SELL);
    }

    private void updateItemAsync() {
        utility.showProgress();
        stringHashMap = new HashMap<>();

        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        stringHashMap.put("uid", itembean.getUid());
        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("sellerphone", editContactNumber.getText().toString());
        stringHashMap.put("sellername", editName.getText().toString());
        stringHashMap.put("description", editDescription.getText().toString());
        stringHashMap.put("price", editPrice.getText().toString());
        stringHashMap.put("condition", spCondition.getSelectedItem().toString());
        stringHashMap.put("category", spCategory.getSelectedItem().toString());
        stringHashMap.put("title", editProductTitle.getText().toString());
        stringHashMap.put("currency", "gbp");
        stringHashMap.put("postcode", editPostcode.getText().toString());
        stringHashMap.put("town", editTown.getText().toString());

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 40) {
                        if (!fileHashMap.isEmpty()) {
                            addImageAsync(itembean.getUid());
                        } else {
                            Toast.makeText(SellItemActivity.this, "Item Updated Successfully", Toast.LENGTH_SHORT).show();
                            utility.hideProgress();
                            finish();
                        }
                    } else if (jsonObject.getInt("code") == -1) {
                        startActivity(new Intent(SellItemActivity.this, LoginActivity.class).putExtra("header", "Buy & Sell"));
                    } else {
                        Toast.makeText(SellItemActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, false).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, Constant.URL_UPDATE_ITEM_SALE);
    }

    private void addSellItemAsync() {
        stringHashMap = new HashMap<>();

        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        stringHashMap.put("uidschool", uid);
        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("sellerphone", editContactNumber.getText().toString());
        stringHashMap.put("sellername", editName.getText().toString());
        stringHashMap.put("paypal", "1");
        stringHashMap.put("description", editDescription.getText().toString());
        stringHashMap.put("price", editPrice.getText().toString());
        stringHashMap.put("condition", spCondition.getSelectedItem().toString());
        stringHashMap.put("category", spCategory.getSelectedItem().toString());
        stringHashMap.put("title", editProductTitle.getText().toString());
        stringHashMap.put("currency", "gbp");
        stringHashMap.put("postcode", editPostcode.getText().toString());
        stringHashMap.put("town", editTown.getText().toString());

        utility.showProgress();

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 39) {
                        if (utility.checkInternetConnection()) {
                            if (!fileHashMap.isEmpty()) {
                                addImageAsync(jsonObject.getJSONObject("message").getString("uid"));
                            } else {
                                Toast.makeText(SellItemActivity.this, "Item added successfully", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }
                    } else if (jsonObject.getInt("code") == -1) {
                        utility.hideProgress();
                        startActivity(new Intent(SellItemActivity.this, LoginActivity.class).putExtra("header", "Buy & Sell"));
                    } else {
                        Toast.makeText(SellItemActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                        utility.hideProgress();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, false).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, Constant.URL_ADD_ITEM_FOR_SALE);
    }

    private void deleteItemAsyncCall() {
        stringHashMap = new HashMap<>();

        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        stringHashMap.put("uid", uid);
        stringHashMap.put("country", Constant.COUNTRY);

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 43) {
                        Toast.makeText(SellItemActivity.this, "Item removed successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_DELETE_ITEM);
    }

    private void markUnmarkAsSoldAsyncCall(String url) {
        stringHashMap = new HashMap<>();
        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        stringHashMap.put("uid", uid);
        stringHashMap.put("country", Constant.COUNTRY);

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    if (jsonObject.getInt("code") == 41 || jsonObject.getInt("code") == 42) {
                        Toast.makeText(SellItemActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
    }

    private void checkValidation() {

        if (TextUtils.isEmpty(editProductTitle.getText().toString())) {
            editProductTitle.setError("Please enter title");
            editProductTitle.requestFocus();
        } else if (TextUtils.isEmpty(editPrice.getText().toString())) {
            editPrice.setError("Please enter price");
            editPrice.requestFocus();
        } else if (TextUtils.isEmpty(editName.getText().toString())) {
            editName.setError("Please enter name");
            editName.requestFocus();
        } else if (TextUtils.isEmpty(editContactNumber.getText().toString())) {
            editContactNumber.setError("Please enter contact number");
            editContactNumber.requestFocus();
        } else {
            if (utility.checkInternetConnection()) {
                if (!isUpdate)
                    addSellItemAsync();
                else
                    updateItemAsync();
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        utility.hideSoftKeyboard();
        return false;
    }

    private void deleteConfirmationDialog(final int pos, final String imgUid, final ImageView imgClose, final ImageView imgProduct) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(getResources().getString(R.string.app_name));
        adb.setMessage("Are you sure you want to remove this image ?");
        adb.setPositiveButton("Yes", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!isFinishing()) {
                    dialog.dismiss();
                    deleteImage(pos, imgUid);
                }
            }
        });
        adb.setNegativeButton("No", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        if (!isFinishing()) {
            adb.show();
        }
    }

    private void deleteItemConfirmDialog(String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setPositiveButton("Yes", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (utility.checkInternetConnection())
                    deleteItemAsyncCall();

                dialog.dismiss();
            }
        });
        adb.setNegativeButton("No", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        adb.show();
    }
}