package com.schoolreviewer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.schoolreviewer.me.LoginActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.MyUtility;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class BaseFragment extends Fragment {

    public MyUtility utility;
    public HashMap<String, String> stringHashMap;
    public SharedPreferences sharedPreferences;
    public Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        utility = new MyUtility(getActivity());
        context = getActivity();
        sharedPreferences = context.getSharedPreferences(Constant.LOGIN_PREFERENCE, Context.MODE_PRIVATE);
    }


    public void checkUserKeyAsyncCall(final onTaskComplete onTaskComplete) {
        stringHashMap = new HashMap<>();
        stringHashMap.put("key", sharedPreferences.getString("key", ""));
        if (utility.checkInternetConnection())
            new AllAPICall(context, stringHashMap, null, new onTaskComplete() {
                @Override
                public void onComplete(String response) {
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jObjUserDetail = jsonArray.getJSONObject(0);
                        if (jObjUserDetail.getInt("code") == -1 || jObjUserDetail.getInt("code") == -0) {
                            sharedPreferences.edit().clear().apply();
                            startActivityForResult(new Intent(context, LoginActivity.class).putExtra("header", getResources().getString(R.string.txt_login)), 1);
                        } else {
                            JSONObject jObjMessage = jObjUserDetail.getJSONObject("message");
                            sharedPreferences.edit().putString("email", jObjMessage.getString("email")).apply();
                            sharedPreferences.edit().putString("displayname", jObjMessage.getString("displayname")).apply();
                            onTaskComplete.onComplete("yes");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_GET_USER_DETAILS);
    }
}
