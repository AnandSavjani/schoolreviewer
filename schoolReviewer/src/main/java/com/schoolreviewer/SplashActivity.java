package com.schoolreviewer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.schoolreviewer.me.LoginActivity;
import com.schoolreviewer.me.RegisterActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.FacebookActivity;
import com.schoolreviewer.util.GCMClientManager;
import com.schoolreviewer.util.GplusActivity;
import com.schoolreviewer.util.LinkedinDialog;
import com.schoolreviewer.util.MyUtility;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout llLoginSplash, llContinue;
    private boolean isTrue;
    private FrameLayout flSplash;
    private Button btnRegister, btnLogin;
    private ImageView imgSocialFb, imgSocialLinkedIn, imgSocialGplus;
    public SharedPreferences sharedPreferences;
    public MyUtility utility;
    public static List<String> listCategory;
    private GCMClientManager pushClientManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPreferences = getSharedPreferences(Constant.LOGIN_PREFERENCE, MODE_PRIVATE);
        utility = new MyUtility(this);
        listCategory = new ArrayList<>();

//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        categoryAsyncCall();
    }

    private void categoryAsyncCall() {
        if (utility.checkInternetConnection()) {
            pushClientManager = new GCMClientManager(this);
            pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
                @Override
                public void onSuccess(String registrationId, boolean isNewRegistration) {
                    sharedPreferences.edit().putString("regid", registrationId).apply();
                    Log.i("regist", registrationId);
                }

                @Override
                public void onFailure(String ex) {
                    super.onFailure(ex);
                }
            });
            getCategoryApiCall();
        } else {
            showOkDialog(getResources().getString(R.string.msg_internet_connection));
        }
    }

    private void initControl() {
        flSplash = (FrameLayout) findViewById(R.id.flSplash);

        llLoginSplash = (LinearLayout) findViewById(R.id.llLoginSplash);
        llContinue = (LinearLayout) findViewById(R.id.llContinue);

        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        btnRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);

        imgSocialFb = (ImageView) findViewById(R.id.imgSocialFb);
        imgSocialLinkedIn = (ImageView) findViewById(R.id.imgSocialLinkedIn);
        imgSocialGplus = (ImageView) findViewById(R.id.imgSocialGplus);

        imgSocialFb.setOnClickListener(this);
        imgSocialLinkedIn.setOnClickListener(this);
        imgSocialGplus.setOnClickListener(this);

        llContinue.setOnClickListener(this);
    }

    private void onLinkedInClick() {
        if (utility.checkInternetConnection()) {
            Intent intentLinked = new Intent(this, LinkedinDialog.class);
            startActivityForResult(intentLinked, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent arg2) {
        if (requestCode == 1 && responseCode == RESULT_OK) {
            Intent splashIntent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(splashIntent);
            finish();
        } else if (requestCode == 5 && responseCode == RESULT_OK) {
            Intent splashIntent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(splashIntent);
            finish();
        }
    }

    public void onFbClick() {
        if (utility.checkInternetConnection()) {
            Intent intentFb = new Intent(this, FacebookActivity.class);
            startActivityForResult(intentFb, 1);
        }
    }

    public void onGplusClick() {
        if (utility.checkInternetConnection()) {
            Intent intentGplus = new Intent(this, GplusActivity.class);
            startActivityForResult(intentGplus, 1);
        }
    }


    @Override
    public void onClick(View v) {
        if (v.equals(llContinue)) {
            Intent splashIntent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(splashIntent);
            finish();
        } else if (v.equals(btnRegister)) {
            startActivityForResult(new Intent(this, RegisterActivity.class), 5);
        } else if (v.equals(btnLogin)) {
            startActivityForResult(new Intent(this, LoginActivity.class).putExtra("header", getResources().getString(R.string.txt_login)), 1);
        } else if (v.equals(imgSocialFb)) {
            onFbClick();
        } else if (v.equals(imgSocialGplus)) {
            onGplusClick();
        } else if (v.equals(imgSocialLinkedIn)) {
            onLinkedInClick();
        }
    }

    private void getCategoryApiCall() {
        HashMap<String, String> stringHashMap = new HashMap<>();

        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("lang", "en");
        stringHashMap.put("pageno", "1");
        stringHashMap.put("pagesize", "25");
        stringHashMap.put("order", "sequence asc");
        stringHashMap.put("category", "All");

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 35) {
                        JSONArray arrayCategory = jsonObject.getJSONObject("message").getJSONArray("categories");
                        for (int i = 0; i < arrayCategory.length(); i++) {
                            listCategory.add(arrayCategory.get(i).toString());
                        }
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (sharedPreferences.contains("key")) {
                                Intent splashIntent = new Intent(SplashActivity.this, MainActivity.class);
                                startActivity(splashIntent);
                                finish();
                            } else {
                                initControl();
                                isTrue = true;
                                llLoginSplash.setVisibility(View.VISIBLE);
                                AnimationSet set = new AnimationSet(true);

                                Animation animation = new AlphaAnimation(0.0f, 1.0f);
                                animation.setDuration(500);
                                set.addAnimation(animation);

                                animation = new TranslateAnimation(
                                        Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                                        Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
                                );
                                animation.setDuration(100);
                                set.addAnimation(animation);

                                LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);
                                llLoginSplash.setLayoutAnimation(controller);
                            }
                        }
                    }, Constant.SPLASH_TIME_OUT);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_GET_SCHOOL_ITEMS);

    }

    public void showOkDialog(String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                categoryAsyncCall();
            }
        });
        adb.show();
    }
}