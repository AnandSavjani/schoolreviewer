package com.schoolreviewer.me;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.schoolreviewer.R;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.MyUtility;


public class ForgotPasswordActivity extends Activity {

    WebView webView;
    MyUtility utility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initControl();
    }

    private void initControl() {
        webView = (WebView) findViewById(R.id.webView);
        utility = new MyUtility(this);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(false);
        webView.setWebViewClient(new myWebViewClient());
        webView.loadUrl(Constant.URL_FORGOT_PASSWORD);
    }

    private class myWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            utility.showProgress();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return false;
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            utility.hideProgress();
        }
    }
}
