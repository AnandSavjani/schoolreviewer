package com.schoolreviewer.me;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;
import com.schoolreviewer.adapter.MyAnswerAdapter;
import com.schoolreviewer.adapter.MyQuestionAdapter;
import com.schoolreviewer.bean.AnswerBean;
import com.schoolreviewer.bean.MyQuestionBean;
import com.schoolreviewer.bean.ReviewBean;
import com.schoolreviewer.bean.SubscriptionBean;
import com.schoolreviewer.home.UserReviewFragment;
import com.schoolreviewer.parentforum.ForumCommentActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class AccountFragment extends BaseFragment implements OnClickListener, TextWatcher, OnItemClickListener, OnTouchListener {

    private FragmentTransaction mFragmentTransaction;
    private ArrayList<SubscriptionBean> arrSubscriptionList = new ArrayList<SubscriptionBean>();
    private ArrayList<ReviewBean> arrReviewList;
    private ArrayList<MyQuestionBean> listAnswerQue;
    private ArrayList<AnswerBean> listAnswer;
    private ArrayList<MyQuestionBean> listMyQuestion;

    private EditText editUserName, editEmail, editPasswordForMe, editNewPassword, editConfirmNewPassword;
    private TextView textHeaderTitle, textHeaderMyReview,
            textNoSubscription, textNoReviews, textAccountDetail, textHeaderPassword,
            textHeaderSubscription, textHeaderBuySell, textHeaderForumQuestion,
            textHeaderForumAnswer, textNoAnswer, textNoQuestion;

    private LinearLayout llSubscriptionDetailOfMe, llPasswordDetailsOfMe,
            llAccountDetailsOfMe, llReviewDetailsOfMe, llQuestionOfMe, llAnswerOfMe;
    private SeekBar sbStrengthOfMe;
    private Button btnSaveMyChanges, btnUpdatePassword;

    private ListView lvSubscriptionList, lvMyReview, lvMyAnswer, lvMyQuestion;
    private boolean isShowPasswordDetails = false, isShowSubscriptionDetails = false,
            isShowMyReviewDetails = false, isShowAccountDetails = false,
            isShowMyQuestion = false, isShowMyAnswer = false;
    static String sSessionKey = null;

    private SubscriptionAdapter mSubAdapter;
    private ReviewAdapter mReviewAdapter;
    private MyAnswerAdapter myAnswerAdapter;
    private MyQuestionAdapter myQuestionAdapter;
    private Bundle mBundle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return LayoutInflater.from(getActivity()).inflate(R.layout.fragment_account, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBundle = getArguments();
        initControl(view);
    }

    public void setupUI(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    utility.hideSoftKeyboard();
                    return false;
                }
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    void initControl(View view) {
        mFragmentTransaction = getFragmentManager().beginTransaction();

        textHeaderTitle = (TextView) getActivity().findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(getResources().getString(R.string.txt_my_account));

        textNoSubscription = (TextView) view.findViewById(R.id.textNoSubscription);
        textNoReviews = (TextView) view.findViewById(R.id.textNoReviews);
        textNoAnswer = (TextView) view.findViewById(R.id.textNoAnswer);
        textNoQuestion = (TextView) view.findViewById(R.id.textNoQuestion);

        textAccountDetail = (TextView) view.findViewById(R.id.textAccountDetail);
        textHeaderPassword = (TextView) view.findViewById(R.id.textHeaderPassword);
        textHeaderSubscription = (TextView) view.findViewById(R.id.textHeaderSubscription);
        textHeaderMyReview = (TextView) view.findViewById(R.id.textHeaderMyReview);
        textHeaderBuySell = (TextView) view.findViewById(R.id.textHeaderBuySell);
        textHeaderForumQuestion = (TextView) view.findViewById(R.id.textHeaderForumQuestion);
        textHeaderForumAnswer = (TextView) view.findViewById(R.id.textHeaderForumAnswer);

        editUserName = (EditText) view.findViewById(R.id.editUserName);
        editEmail = (EditText) view.findViewById(R.id.editEmail);
        editPasswordForMe = (EditText) view.findViewById(R.id.editPasswordForMe);
        editNewPassword = (EditText) view.findViewById(R.id.editNewPassword);
        editConfirmNewPassword = (EditText) view.findViewById(R.id.editConfirmNewPassword);

        editNewPassword.addTextChangedListener(this);

        llAccountDetailsOfMe = (LinearLayout) view.findViewById(R.id.llAccountDetailsOfMe);
        llPasswordDetailsOfMe = (LinearLayout) view.findViewById(R.id.llPasswordDetailsOfMe);
        llSubscriptionDetailOfMe = (LinearLayout) view.findViewById(R.id.llSubscriptionDetailOfMe);
        llReviewDetailsOfMe = (LinearLayout) view.findViewById(R.id.llReviewDetailsOfMe);
        llQuestionOfMe = (LinearLayout) view.findViewById(R.id.llQuestionOfMe);
        llAnswerOfMe = (LinearLayout) view.findViewById(R.id.llAnswerOfMe);

        textAccountDetail.setOnClickListener(this);
        textHeaderPassword.setOnClickListener(this);
        textHeaderSubscription.setOnClickListener(this);
        textHeaderMyReview.setOnClickListener(this);
        textHeaderBuySell.setOnClickListener(this);
        textHeaderForumQuestion.setOnClickListener(this);
        textHeaderForumAnswer.setOnClickListener(this);

        sbStrengthOfMe = (SeekBar) view.findViewById(R.id.sbStrengthOfMe);
        sbStrengthOfMe.setOnTouchListener(this);

        btnSaveMyChanges = (Button) view.findViewById(R.id.btnSaveMyChanges);
        btnUpdatePassword = (Button) view.findViewById(R.id.btnUpdatePassword);
        btnSaveMyChanges.setOnClickListener(this);
        btnUpdatePassword.setOnClickListener(this);

        lvSubscriptionList = (ListView) view.findViewById(R.id.lvSubsciptionList);
        lvMyReview = (ListView) view.findViewById(R.id.lvMyReview);
        lvMyAnswer = (ListView) view.findViewById(R.id.lvMyAnswer);
        lvMyQuestion = (ListView) view.findViewById(R.id.lvMyQuestion);

        lvMyReview.setOnItemClickListener(this);
        lvMyAnswer.setOnItemClickListener(this);
        lvMyQuestion.setOnItemClickListener(this);

        utility.hideSoftKeyboard();
        if (!sharedPreferences.contains("social")) {
            editUserName.setEnabled(true);
            editEmail.setEnabled(true);
            editPasswordForMe.setEnabled(true);
            editNewPassword.setEnabled(true);
            editConfirmNewPassword.setEnabled(true);
            btnSaveMyChanges.setEnabled(true);
            btnUpdatePassword.setEnabled(true);
        }
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        if (sharedPreferences.contains("key")) {
            sSessionKey = sharedPreferences.getString("key", null);
            if (utility.checkInternetConnection()) {
                getAccountDetailAsyncCall();
            }
        } else {
            redirectToLoginFragment();
        }
        setupUI(view.findViewById(R.id.flAccountFragment));

    }

    @Override
    public void onClick(View mView) {
        switch (mView.getId()) {
            case R.id.textAccountDetail:
                if (!isShowAccountDetails)
                    showAccountDetails(true);
                else
                    showAccountDetails(false);
                break;

            case R.id.textHeaderPassword:
                if (!isShowPasswordDetails)
                    showPasswordDetails(true);
                else
                    showPasswordDetails(false);
                break;
            case R.id.textHeaderSubscription:
                if (!isShowSubscriptionDetails)
                    showSubscriptionDetails(true);
                else
                    showSubscriptionDetails(false);
                break;

            case R.id.textHeaderMyReview:
                if (!isShowMyReviewDetails)
                    showMyReviewDetails(true);
                else
                    showMyReviewDetails(false);
                break;

            case R.id.textHeaderForumQuestion:
                if (!isShowMyQuestion)
                    showQuestionForum(true);
                else
                    showQuestionForum(false);
                break;

            case R.id.textHeaderForumAnswer:
                if (!isShowMyAnswer)
                    showAnswerForum(true);
                else
                    showAnswerForum(false);
                break;

            case R.id.btnSaveMyChanges:
                utility.hideSoftKeyboard();
                if (checkValidationForDetails()) {
                    if (utility.checkInternetConnection()) {
                        updateAccountDetailsAsyncCall();
                    }
                }
                break;

            case R.id.btnUpdatePassword:
                if (utility.checkInternetConnection()) {
                    if (checkValidationForPassword()) {
                        updatePasswordAsyncCall();
                    }
                }
                break;

            case R.id.textHeaderBuySell:
                if (utility.checkInternetConnection()) {
                    startActivity(new Intent(getActivity(), MyBuySellActivity.class));
                }
                break;
            default:
                break;
        }
    }

    private void showAnswerForum(boolean isVisible) {
        if (isVisible) {
            llAnswerOfMe.setVisibility(View.VISIBLE);
            isShowMyAnswer = true;
            textHeaderForumAnswer.setCompoundDrawablesWithIntrinsicBounds(R.drawable.parents_forum, 0, R.drawable.up_arrow, 0);
        } else {
            llAnswerOfMe.setVisibility(View.GONE);
            isShowMyAnswer = false;
            textHeaderForumAnswer.setCompoundDrawablesWithIntrinsicBounds(R.drawable.parents_forum, 0, R.drawable.down_arrow, 0);
        }
    }

    private void showQuestionForum(boolean isVisible) {
        if (isVisible) {
            llQuestionOfMe.setVisibility(View.VISIBLE);
            isShowMyQuestion = true;
            textHeaderForumQuestion.setCompoundDrawablesWithIntrinsicBounds(R.drawable.parents_forum, 0, R.drawable.up_arrow, 0);
        } else {
            llQuestionOfMe.setVisibility(View.GONE);
            isShowMyQuestion = false;
            textHeaderForumQuestion.setCompoundDrawablesWithIntrinsicBounds(R.drawable.parents_forum, 0, R.drawable.down_arrow, 0);
        }
    }

    private void showMyReviewDetails(boolean isVisible) {
        if (isVisible) {
            llReviewDetailsOfMe.setVisibility(View.VISIBLE);
            isShowMyReviewDetails = true;
            textHeaderMyReview.setCompoundDrawablesWithIntrinsicBounds(R.drawable.reviews, 0, R.drawable.up_arrow, 0);
        } else {
            llReviewDetailsOfMe.setVisibility(View.GONE);
            isShowMyReviewDetails = false;
            textHeaderMyReview.setCompoundDrawablesWithIntrinsicBounds(R.drawable.reviews, 0, R.drawable.down_arrow, 0);
        }
    }

    private void showSubscriptionDetails(boolean isVisible) {
        if (isVisible) {
            llSubscriptionDetailOfMe.setVisibility(View.VISIBLE);
            isShowSubscriptionDetails = true;
            textHeaderSubscription.setCompoundDrawablesWithIntrinsicBounds(R.drawable.schools_follow, 0, R.drawable.up_arrow, 0);
        } else {
            llSubscriptionDetailOfMe.setVisibility(View.GONE);
            isShowSubscriptionDetails = false;
            textHeaderSubscription.setCompoundDrawablesWithIntrinsicBounds(R.drawable.schools_follow, 0, R.drawable.down_arrow, 0);
        }

    }

    private void showPasswordDetails(boolean isVisible) {
        if (isVisible) {
            llPasswordDetailsOfMe.setVisibility(View.VISIBLE);
            isShowPasswordDetails = true;
            textHeaderPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.passwd, 0, R.drawable.up_arrow, 0);
        } else {
            llPasswordDetailsOfMe.setVisibility(View.GONE);
            isShowPasswordDetails = false;
            textHeaderPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.passwd, 0, R.drawable.down_arrow, 0);
        }
    }

    private void showAccountDetails(boolean isVisible) {
        if (isVisible) {
            textAccountDetail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.account_details, 0, R.drawable.up_arrow, 0);
            llAccountDetailsOfMe.setVisibility(View.VISIBLE);
            isShowAccountDetails = true;
        } else {
            textAccountDetail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.account_details, 0, R.drawable.down_arrow, 0);
            llAccountDetailsOfMe.setVisibility(View.GONE);
            isShowAccountDetails = false;
        }
    }

    boolean checkValidationForDetails() {
        if (utility.validateText(editUserName, "Please enter user name")) {
            if (utility.validateEmail(editEmail, "Provide valid email address")) {
                return true;
            }
        }
        return false;
    }

    boolean checkValidationForPassword() {
        if (utility.validateText(editPasswordForMe, "Please enter old password")) {
            if (utility.validateText(editNewPassword, "Please enter new password")) {
                if (utility.validateText(editConfirmNewPassword, "Please enter confirm password")) {
                    if (editNewPassword.getText().toString().equals(editConfirmNewPassword.getText().toString())) {
                        return true;
                    } else {
                        utility.showMessageDialog(getResources().getString(R.string.msg_password_not_match));
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void afterTextChanged(Editable arg0) {
    }

    @Override
    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
    }

    @Override
    public void onTextChanged(CharSequence arg0, int charLength, int arg2, int count) {
        setProgress();
        sbStrengthOfMe.setVisibility(View.VISIBLE);
        if (arg0.length() == 1 && count == 1) {
            sbStrengthOfMe.setProgress(10);
            setProgressBarColor(getResources().getColor(R.color.color_red));
        }
        setProgress();
        if (charLength == 0 && count == 0) {
            sbStrengthOfMe.setProgress(0);
        }
    }

    void setProgress() {
        int valueOfProgress = utility.measurePasswordStrength(editNewPassword.getText().toString());

        if (valueOfProgress == 0) {
            sbStrengthOfMe.setProgress(10);
            setProgressBarColor(getResources().getColor(R.color.color_red));
        } else if (valueOfProgress == 1) {
            sbStrengthOfMe.setProgress(50);
            setProgressBarColor(getResources().getColor(R.color.color_orange));
        } else if (valueOfProgress == 2) {
            sbStrengthOfMe.setProgress(sbStrengthOfMe.getMax());
            setProgressBarColor(getResources().getColor(R.color.color_green));
        }
    }

    private void setProgressBarColor(int newColor) {
        LayerDrawable ld = (LayerDrawable) sbStrengthOfMe.getProgressDrawable();
        ClipDrawable d1 = (ClipDrawable) ld.findDrawableByLayerId(android.R.id.progress);
        d1.setColorFilter(newColor, Mode.SRC_OVER);

    }

    void redirectToLoginFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("header", textHeaderTitle.getText().toString());
        bundle.putString("tab", "me");
        bundle.putString("context", "fragment");
        LoginFragment fLogin = new LoginFragment();
        mFragmentTransaction = getFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.flMeContent, fLogin);
        fLogin.setArguments(bundle);
        mFragmentTransaction.commit();
    }

    private void getAccountDetailAsyncCall() {
        utility.showProgress();
        stringHashMap = new HashMap<>();
        stringHashMap.put("key", sSessionKey);

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjUserDetail = jsonArray.getJSONObject(0);
                    if (jObjUserDetail.getInt("code") == -1 || jObjUserDetail.getInt("code") == -0) {
                        sharedPreferences.edit().clear().apply();
                        redirectToLoginFragment();
                    } else if (response != null) {
                        JSONObject jObjMessage = jObjUserDetail.getJSONObject("message");
                        editUserName.setText(jObjMessage.getString("displayname"));
                        editEmail.setText(jObjMessage.getString("email"));

                        sharedPreferences.edit().putString("email", jObjMessage.getString("email")).apply();
                        sharedPreferences.edit().putString("displayname", jObjMessage.getString("displayname")).apply();
                        if (mBundle.getBoolean("show"))
                            utility.showMessageDialog("Welcome " + jObjMessage.getString("displayname"));
                        if (utility.checkInternetConnection())
                            myReviewAsyncCall();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getSubscriptionAsyncCall();
            }
        }, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_GET_USER_DETAILS);
    }

    private void updatePasswordAsyncCall() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("key", sSessionKey);
        stringHashMap.put("password", editNewPassword.getText().toString());
        stringHashMap.put("current", editPasswordForMe.getText().toString());

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjPassword = jsonArray.getJSONObject(0);
                    if (jObjPassword.getString("code").equals("14")) {
                        utility.showMessageDialog(jObjPassword.getString("message"));
                        showPasswordDetails(false);
                        makeFieldEmpty();
                    } else {
                        utility.showMessageDialog(jObjPassword.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_CHANGE_PASSWORD);
    }

    private void updateAccountDetailsAsyncCall() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("key", sSessionKey);
        stringHashMap.put("displayname", editUserName.getText().toString());
        stringHashMap.put("email", editEmail.getText().toString());

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_UPDATE_USER_DETAILS);

    }

    private void getSubscriptionAsyncCall() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("key", sSessionKey);

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    arrSubscriptionList.clear();
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjSubscription = jsonArray.getJSONObject(0);
                    if (jObjSubscription.getInt("code") != -0) {
                        JSONObject jObjForSubscription = jObjSubscription.getJSONObject("message");
                        JSONArray jArrayschool = jObjForSubscription.getJSONArray("subscriptions");

                        for (int j = 0; j < jArrayschool.length(); j++) {
                            JSONObject jObjSubDetails = jArrayschool.getJSONObject(j);

                            SubscriptionBean sBean = new SubscriptionBean();
                            sBean.setUid(jObjSubDetails.getString("uid"));
                            sBean.setUidschool(jObjSubDetails.getString("uidschool"));
                            sBean.setSchoolname(jObjSubDetails.getString("schoolname"));
                            arrSubscriptionList.add(sBean);
                        }

                        if (sharedPreferences.contains("key")) {
                            if (jArrayschool.length() <= 0) {
                                textNoSubscription.setVisibility(View.VISIBLE);
                                lvSubscriptionList.setVisibility(View.GONE);
                            } else {
                                mSubAdapter = new SubscriptionAdapter();
                                lvSubscriptionList.setAdapter(mSubAdapter);
                                lvSubscriptionList.setVisibility(View.VISIBLE);
                                textNoSubscription.setVisibility(View.GONE);
                                setListViewHeightBasedOnChildren(lvSubscriptionList);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                myQuestionAsyncCall();
            }
        }, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_GET_USER_SUBSCRIPTION);
    }

    private static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.setLayoutParams(new ViewGroup.LayoutParams(0, 0));
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + 10;
        listView.setLayoutParams(params);
    }

    public static void setListViewHeight(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0) {
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));
            }
            view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private class SubscriptionAdapter extends BaseAdapter {
        private LayoutInflater mLayoutInflater = null;

        private SubscriptionAdapter() {
            mLayoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return arrSubscriptionList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int pos) {
            return pos;
        }

        @Override
        public View getView(int position, View mView, ViewGroup viewgroup) {
            ViewHolder mHolder;
            final SubscriptionBean sBean = arrSubscriptionList.get(position);
            if (mView == null) {
                mHolder = new ViewHolder();
                mView = mLayoutInflater.inflate(R.layout.row_subscription, null);
                mHolder.textSchoolName = (TextView) mView.findViewById(R.id.textSchoolNameForSubscription);
                mHolder.btnUnSubscribe = (Button) mView.findViewById(R.id.btnUnsubscribe);
                mView.setTag(mHolder);
            } else
                mHolder = (ViewHolder) mView.getTag();

            if (sBean != null) {
                mHolder.textSchoolName.setText(sBean.getSchoolname());
                mHolder.btnUnSubscribe.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        if (utility.checkInternetConnection())
                            UnsubscribeAsyncCall(sBean.getUid());
                    }
                });
            }
            return mView;
        }
    }

    private void UnsubscribeAsyncCall(String uid) {
        stringHashMap = new HashMap<>();
        stringHashMap.put("key", sSessionKey);
        stringHashMap.put("uid", uid);

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjUnsubscribe = jsonArray.getJSONObject(0);
                    if (jObjUnsubscribe.getString("code").equals("17")) {
                        arrSubscriptionList.clear();
                        getSubscriptionAsyncCall();
                    } else {
                        redirectToLoginFragment();
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_UNSUBSCRIBE);
    }

    private void myReviewAsyncCall() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("key", sSessionKey);
        arrReviewList = new ArrayList<>();

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjResponse = jsonArray.getJSONObject(0);
                    JSONObject jObjMessage = jObjResponse.getJSONObject("message");
                    JSONArray jArrayReview = jObjMessage.getJSONArray("reviews");
                    for (int i = 0; i < jArrayReview.length(); i++) {
                        ReviewBean reviewBean = new ReviewBean();
                        JSONObject jObjReview = jArrayReview.getJSONObject(i);
                        reviewBean.setTitle(jObjReview.getString("title"));
                        reviewBean.setUid(jObjReview.getString("uidschool"));
                        reviewBean.setReviewedat(jObjReview.getString("reviewedat"));

                        float fScore = Float.parseFloat(jObjReview.getString("score"));
                        reviewBean.setScoreOfReview(fScore);
                        reviewBean.setDisplayname(jObjReview.getString("schoolname"));
                        reviewBean.setRelationship("");
                        reviewBean.setFurther(jObjReview.getString("further"));
                        reviewBean.setReviewyear(jObjReview.getString("reviewyear"));
                        reviewBean.setLoH(jObjReview.getString("LoH"));
                        reviewBean.setQoT(jObjReview.getString("QoT"));
                        reviewBean.setAoS(jObjReview.getString("AoS"));
                        reviewBean.setSaS(jObjReview.getString("SaS"));
                        reviewBean.setSports(jObjReview.getString("sports"));
                        reviewBean.setMusic(jObjReview.getString("music"));
                        reviewBean.setFood(jObjReview.getString("food"));
                        reviewBean.setReply(jObjReview.getString("reply"));
                        reviewBean.setFromFragment("me");
                        arrReviewList.add(reviewBean);
                    }

                    if (sharedPreferences.contains("key")) {
                        if (jArrayReview.length() > 0) {
                            mReviewAdapter = new ReviewAdapter();
                            lvMyReview.setAdapter(mReviewAdapter);
                            setListViewHeight(lvMyReview);
                            textHeaderMyReview.setText(getResources().getString(R.string.txt_my_review) + " (" + jArrayReview.length() + ")");
                        }

                        if (jArrayReview.length() <= 0) {
                            lvMyReview.setVisibility(View.GONE);
                            textNoReviews.setVisibility(View.VISIBLE);
                        } else {
                            lvMyReview.setVisibility(View.VISIBLE);
                            textNoReviews.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_GET_USER_REVIEW);
    }

    private void myQuestionAsyncCall() {
        listAnswer = new ArrayList<>();
        listAnswerQue = new ArrayList<>();
        listMyQuestion = new ArrayList<>();

        stringHashMap = new HashMap<>();
        stringHashMap.put("key", sSessionKey);

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getString("status").equalsIgnoreCase("ok")) {
                        JSONObject msgObj = jsonObject.getJSONObject("message");
                        JSONArray questionArray = msgObj.getJSONArray("questions");

                        for (int i = 0; i < questionArray.length(); i++) {
                            JSONObject questionObj = questionArray.getJSONObject(i);
                            MyQuestionBean myQuestionBean = new MyQuestionBean();

                            myQuestionBean.setUid(questionObj.getString("uid"));
                            myQuestionBean.setUidschool(questionObj.getString("uidschool"));
                            myQuestionBean.setSchoolname(questionObj.getString("schoolname"));
                            myQuestionBean.setTitle(questionObj.getString("title"));
                            myQuestionBean.setAskedat(questionObj.getString("askedat"));
                            myQuestionBean.setQuestion(questionObj.getString("question"));
                            myQuestionBean.setQuestionyear(questionObj.getString("questionyear"));
                            myQuestionBean.setNumberviews(questionObj.getString("numberviews"));
                            myQuestionBean.setNumberlikes(questionObj.getString("numberlikes"));
                            myQuestionBean.setNumberdislikes(questionObj.getString("numberdislikes"));
                            myQuestionBean.setRelationship(questionObj.getString("relationship"));
                            myQuestionBean.setTimenow(questionObj.getString("timenow"));
                            listMyQuestion.add(myQuestionBean);
                        }

                        JSONArray answerArray = msgObj.getJSONArray("answers");
                        for (int j = 0; j < answerArray.length(); j++) {
                            JSONObject objAnswer = answerArray.getJSONObject(j);
                            MyQuestionBean myQuestionAnsBean = new MyQuestionBean();
                            AnswerBean answerBean = new AnswerBean();

                            myQuestionAnsBean.setUid(objAnswer.getString("uid"));
                            myQuestionAnsBean.setUidschool(objAnswer.getString("uidschool"));
                            myQuestionAnsBean.setSchoolname(objAnswer.getString("schoolname"));
                            myQuestionAnsBean.setTitle(objAnswer.getString("title"));
                            myQuestionAnsBean.setAskedat(objAnswer.getString("askedat"));
                            myQuestionAnsBean.setQuestion(objAnswer.getString("question"));
                            myQuestionAnsBean.setQuestionyear(objAnswer.getString("questionyear"));
                            myQuestionAnsBean.setNumberviews(objAnswer.getString("numberviews"));
                            myQuestionAnsBean.setNumberlikes(objAnswer.getString("numberlikes"));
                            myQuestionAnsBean.setNumberdislikes(objAnswer.getString("numberdislikes"));
                            myQuestionAnsBean.setRelationship(objAnswer.getString("relationship"));
                            myQuestionAnsBean.setTimenow(objAnswer.getString("timenow"));

                            answerBean.setAnsweredat(objAnswer.getString("answeredat"));
                            answerBean.setAnswer(objAnswer.getString("answer"));
                            answerBean.setAnsweryear(objAnswer.getString("answeryear"));
                            answerBean.setNumberlikes(objAnswer.getString("answernumberlikes"));
                            answerBean.setNumberdislikes(objAnswer.getString("answernumberdislikes"));
                            answerBean.setRelationship(objAnswer.getString("answerrelationship"));
                            answerBean.setTimenow(objAnswer.getString("timenow"));
                            answerBean.setUidschool(objAnswer.getString("uidschool"));
                            answerBean.setUid(objAnswer.getString("uid"));

                            listAnswerQue.add(myQuestionAnsBean);
                            listAnswer.add(answerBean);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (!listMyQuestion.isEmpty()) {
                    myQuestionAdapter = new MyQuestionAdapter(getActivity(), listMyQuestion);
                    lvMyQuestion.setAdapter(myQuestionAdapter);
                    setListViewHeightBasedOnChildren(lvMyQuestion);
                    textNoQuestion.setVisibility(View.GONE);
                    lvMyQuestion.setVisibility(View.VISIBLE);
                } else {
                    textNoQuestion.setVisibility(View.VISIBLE);
                    lvMyQuestion.setVisibility(View.GONE);
                }

                if (!listAnswer.isEmpty()) {
                    myAnswerAdapter = new MyAnswerAdapter(getActivity(), listAnswer, listAnswerQue);
                    lvMyAnswer.setAdapter(myAnswerAdapter);
                    setListViewHeight(lvMyAnswer);
                    textNoAnswer.setVisibility(View.GONE);
                    lvMyAnswer.setVisibility(View.VISIBLE);
                } else {
                    textNoAnswer.setVisibility(View.VISIBLE);
                    lvMyAnswer.setVisibility(View.GONE);
                }
                utility.hideProgress();
            }
        }, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_USER_QUESTION);
    }

    private class ReviewAdapter extends BaseAdapter {
        private ViewHolder mViewHolder;
        private Date mDate = null;
        private SimpleDateFormat serverFormat;

        @Override
        public int getCount() {
            return arrReviewList.size();
        }

        @Override
        public ReviewBean getItem(int position) {
            return arrReviewList.get(position);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int position, View mView, ViewGroup arg2) {
            mViewHolder = new ViewHolder();
            if (mView == null) {
                mView = View.inflate(getActivity(), R.layout.row_reviews, null);
                mViewHolder.textTitle = (TextView) mView.findViewById(R.id.textTitle);
                mViewHolder.textDisplayname = (TextView) mView.findViewById(R.id.textDisplayName);
                mViewHolder.rbScoreOfReview = (RatingBar) mView.findViewById(R.id.rbReviewRating);
                mViewHolder.textDisplayname.setTextColor(Color.BLACK);
                mView.setTag(mViewHolder);
            } else {
                mViewHolder = (ViewHolder) mView.getTag();
            }

            mViewHolder.textTitle.setText(arrReviewList.get(position).getDisplayname().toUpperCase());
            int totalCount = arrReviewList.get(position).getDisplayname().length();
            if (totalCount > 25) {
                mViewHolder.textTitle.setSingleLine(false);
                mViewHolder.textTitle.setLines(2);
                mViewHolder.textTitle.setGravity(Gravity.CENTER_VERTICAL);
            } else {
                mViewHolder.textTitle.setSingleLine(true);
                mViewHolder.textTitle.setLines(1);
            }
            String date = arrReviewList.get(position).getReviewedat();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            try {
                mDate = format.parse(date);
                serverFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            mViewHolder.textDisplayname.setText(serverFormat.format(mDate));
            mViewHolder.rbScoreOfReview.setRating(arrReviewList.get(position).getScoreOfReview());
            LayerDrawable stars = (LayerDrawable) mViewHolder.rbScoreOfReview.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.color_yellow), Mode.SRC_ATOP);
            return mView;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        makeFieldEmpty();
    }

    void makeFieldEmpty() {
        editPasswordForMe.setText("");
        editNewPassword.setText("");
        editConfirmNewPassword.setText("");
    }

    private class ViewHolder {
        private TextView textSchoolName, textTitle, textDisplayname;
        private Button btnUnSubscribe;
        private RatingBar rbScoreOfReview;
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        Bundle bundle = new Bundle();
        switch (arg0.getId()) {
            case R.id.lvMyReview:
                UserReviewFragment fUserReview = new UserReviewFragment();
                mFragmentTransaction = getFragmentManager().beginTransaction();
                bundle.putSerializable("review", arrReviewList.get(position));
                mFragmentTransaction.replace(R.id.flAccountFragment, fUserReview);
                fUserReview.setArguments(bundle);
                mFragmentTransaction.addToBackStack(null).commit();
                break;

            case R.id.lvMyQuestion:
                Intent intent = new Intent(getActivity(), ForumCommentActivity.class);
                intent.putExtra("uidSchool", listMyQuestion.get(position).getUidschool());
                intent.putExtra("uidtopic", listMyQuestion.get(position).getUid());
                startActivity(intent);

                break;

            case R.id.lvMyAnswer:
//                MyQuestionFragment answerFragment = new MyQuestionFragment();
//                bundle.putSerializable("question", listAnswerQue.get(position));
//                bundle.putSerializable("answer", listAnswer.get(position));
//                bundle.putBoolean("isQuestion", false);
//                mFragmentTransaction = getFragmentManager().beginTransaction();
//                mFragmentTransaction.replace(R.id.flAccountFragment, answerFragment);
//                answerFragment.setArguments(bundle);
//                mFragmentTransaction.addToBackStack(null);
//                mFragmentTransaction.commitAllowingStateLoss();

                Intent intent2 = new Intent(getActivity(), ForumCommentActivity.class);
                intent2.putExtra("uidSchool", listAnswer.get(position).getUidschool());
                intent2.putExtra("uidtopic", listAnswer.get(position).getUid());
                startActivity(intent2);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v.equals(sbStrengthOfMe) && event.getAction() == MotionEvent.ACTION_DOWN)
            return true;
        return true;
    }
}