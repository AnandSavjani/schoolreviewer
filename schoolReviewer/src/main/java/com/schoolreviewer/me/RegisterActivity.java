package com.schoolreviewer.me;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.schoolreviewer.BaseActivity;
import com.schoolreviewer.R;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class RegisterActivity extends BaseActivity implements OnClickListener, OnTouchListener, TextWatcher {

    private TextView textHeaderTitle;
    private SeekBar sbStrength;
    private EditText editDisplayName, editEmailAddress, editPassword, editConfirmPassword;
    private Button btnRegister;
    int valueOfProgress;
    private HashMap<String, String> stringHashMap;
    private FrameLayout llHeaderLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_register);
        initControl();
    }

    void initControl() {
        llHeaderLayout = (FrameLayout) findViewById(R.id.llHeaderLayout);
        llHeaderLayout.setVisibility(View.VISIBLE);

        textHeaderTitle = (TextView) findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(R.string.txt_register);

        editDisplayName = (EditText) findViewById(R.id.editDisplayName);
        editEmailAddress = (EditText) findViewById(R.id.editEmailAddress);
        editPassword = (EditText) findViewById(R.id.editPassword);
        editConfirmPassword = (EditText) findViewById(R.id.editConfirmPassword);

        sbStrength = (SeekBar) findViewById(R.id.sbStrength);
        sbStrength.setThumb(null);

        sbStrength.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                return true;
            }
        });

        sbStrength.setProgressDrawable(getResources().getDrawable(R.drawable.progress_seekbar));

        editDisplayName.setOnTouchListener(this);
        editConfirmPassword.setOnTouchListener(this);
        editEmailAddress.setOnTouchListener(this);
        editPassword.setOnTouchListener(this);

        editPassword.addTextChangedListener(this);
        editConfirmPassword.setOnTouchListener(this);

        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:
                if (checkValidation()) {
                    utility.hideSoftKeyboard();
                    if (utility.checkInternetConnection()) {
                        registerAsyncCall();
                    }
                }
                break;

            default:
                break;
        }

    }

    @Override
    public boolean onTouch(View view, MotionEvent arg1) {
        switch (view.getId()) {
            case R.id.editDisplayName:
                editDisplayName.setFocusableInTouchMode(true);
                editDisplayName.requestFocusFromTouch();
                break;
            case R.id.editEmailAddress:
                editEmailAddress.setFocusableInTouchMode(true);
                editEmailAddress.requestFocusFromTouch();
                break;
            case R.id.editPassword:
                editPassword.setFocusableInTouchMode(true);
                editPassword.requestFocusFromTouch();
                break;
            case R.id.editConfirmPassword:
                editConfirmPassword.setFocusableInTouchMode(true);
                editConfirmPassword.requestFocusFromTouch();

            default:
                break;
        }
        return false;
    }

    void setProgress() {
        valueOfProgress = utility.measurePasswordStrength(editPassword.getText().toString());

        if (valueOfProgress == 0) {
            sbStrength.setProgress(10);
            setProgressBarColor(getResources().getColor(R.color.color_red));
        } else if (valueOfProgress == 1) {
            sbStrength.setProgress(50);
            setProgressBarColor(getResources().getColor(R.color.color_orange));
        } else if (valueOfProgress == 2) {
            sbStrength.setProgress(sbStrength.getMax());
            setProgressBarColor(getResources().getColor(R.color.color_green));
        }
    }

    private boolean checkValidation() {
        if (utility.validateText(editDisplayName, getResources().getString(R.string.msg_display_name))) {
            if (utility.validateEmail(editEmailAddress, getResources().getString(R.string.msg_provide_email))) {
                if (utility.validateText(editPassword, getResources().getString(R.string.msg_valid_password))) {
                    if (utility.validateText(editConfirmPassword, getResources().getString(R.string.msg_confirm_password))) {
                        if (!editPassword.getText().toString().trim().equals(editConfirmPassword.getText().toString().trim())) {
                            utility.showMessageDialog(getResources().getString(R.string.msg_password_not_match));
                        } else {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void afterTextChanged(Editable arg0) {
    }

    @Override
    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
    }

    @Override
    public void onTextChanged(CharSequence arg0, int charLength, int arg2, int count) {
        if (arg0.length() == 1 && count == 1) {
            sbStrength.setProgress(10);
            setProgressBarColor(getResources().getColor(R.color.color_red));
        }
        setProgress();
        if (charLength == 0 && count == 0) {
            sbStrength.setProgress(0);
        }
    }

    public void setProgressBarColor(int newColor) {
        LayerDrawable ld = (LayerDrawable) sbStrength.getProgressDrawable();
        ClipDrawable d1 = (ClipDrawable) ld.findDrawableByLayerId(android.R.id.progress);
        d1.setColorFilter(newColor, Mode.SRC_IN);
    }

    private void registerAsyncCall() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("displayname", editDisplayName.getText().toString());
        stringHashMap.put("password", editPassword.getText().toString());
        stringHashMap.put("email", editEmailAddress.getText().toString());
        stringHashMap.put("lang", "en");

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjUserDetail = jsonArray.getJSONObject(0);

                    if (jObjUserDetail.getInt("code") == 19) {
                        showMessageDialog(getResources().getString(R.string.msg_register)
                                + getResources().getString(R.string.msg_register_complete));
                    } else {
                        utility.showMessageDialog(getResources().getString(R.string.msg_not_register));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_REGISTER);

    }

    private void showMessageDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                setResult(RESULT_OK);
                finish();
            }
        });
        builder.show();
    }
}
