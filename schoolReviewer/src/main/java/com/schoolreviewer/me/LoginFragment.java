package com.schoolreviewer.me;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;
import com.schoolreviewer.home.WriteReviewFragmentActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.FacebookActivity;
import com.schoolreviewer.util.GplusActivity;
import com.schoolreviewer.util.LinkedinDialog;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginFragment extends BaseFragment implements OnClickListener, OnTouchListener {

    private EditText editUserName, editPassword;
    private Button btnLogin, btnRegister;
    public ImageView imgSocialFb, imgSocialLinkedIn, imgSocialGplus;
    private TextView textHeaderTitle, textForgotPassword;
    private HashMap<String, String> stringHashMap;
    private SharedPreferences preferenceLogin, preferenceRemember;
    private String tabName = "";
    private WriteReviewFragmentActivity reviewActivity;
    private String sContext = "";
    private CheckBox cbRemember;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_login, null);
        initControl(mView);
        mView.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
                return false;
            }
        });
        return mView;
    }

    private void initControl(View mView) {

        textHeaderTitle = (TextView) getActivity().findViewById(R.id.textHeaderTitle);
        Bundle bundle = this.getArguments();
        textHeaderTitle.setText(bundle.getString("header"));

        tabName = bundle.getString("tab");
        sContext = bundle.getString("context");

        textForgotPassword = (TextView) mView.findViewById(R.id.textForgotPassword);
        textForgotPassword.setOnClickListener(this);

        editPassword = (EditText) mView.findViewById(R.id.editPassword);
        editUserName = (EditText) mView.findViewById(R.id.editUserName);
        editUserName.setOnTouchListener(this);
        editPassword.setOnTouchListener(this);

        btnLogin = (Button) mView.findViewById(R.id.btnLogin);
        btnRegister = (Button) mView.findViewById(R.id.btnRegisterOption);
        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);

        imgSocialGplus = (ImageView) mView.findViewById(R.id.imgSocialGplus);
        imgSocialLinkedIn = (ImageView) mView.findViewById(R.id.imgSocialLinkedIn);
        imgSocialFb = (ImageView) mView.findViewById(R.id.imgSocialFb);
        imgSocialGplus.setOnClickListener(this);
        imgSocialFb.setOnClickListener(this);
        imgSocialLinkedIn.setOnClickListener(this);

        preferenceLogin = getActivity().getSharedPreferences(Constant.LOGIN_PREFERENCE, Context.MODE_PRIVATE);
        preferenceRemember = getActivity().getSharedPreferences(Constant.REMEMBER_PREFERENCE, Context.MODE_PRIVATE);

        cbRemember = (CheckBox) mView.findViewById(R.id.cbRemember);

        if (preferenceRemember.contains("name") && preferenceRemember.contains("password")) {
            editUserName.setText(preferenceRemember.getString("name", ""));
            editPassword.setText(preferenceRemember.getString("password", ""));
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.textForgotPassword:
                if (utility.checkInternetConnection())
                    startActivity(new Intent(getActivity(), ForgotPasswordActivity.class));
                break;
            case R.id.btnLogin:
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editPassword.getWindowToken(), 0);
                if (checkValidation()) {
                    if (utility.checkInternetConnection()) {
                        preferenceLogin.edit().clear().apply();
                        loginAsyncCall();
                    }
                }
                break;
            case R.id.btnRegisterOption:
                utility.hideSoftKeyboard();
                registerFragment();
                break;

            case R.id.imgSocialGplus:
                if (utility.checkInternetConnection()) {
                    if (sContext.equals("activity")) {
                        reviewActivity = (WriteReviewFragmentActivity) getActivity();
                        reviewActivity.onGplusClick();
                    } else {
                        Intent iGplus = new Intent(getActivity(), GplusActivity.class);
                        startActivityForResult(iGplus, 1);
                    }
                }
                break;
            case R.id.imgSocialFb:
                if (utility.checkInternetConnection()) {
                    if (sContext.equals("activity")) {
                        reviewActivity = (WriteReviewFragmentActivity) getActivity();
                        reviewActivity.onFbClick();
                    } else {
                        Intent inFb = new Intent(getActivity(), FacebookActivity.class);
                        startActivityForResult(inFb, 1);

                    }
                }
                break;
            case R.id.imgSocialLinkedIn:
                if (utility.checkInternetConnection()) {
                    if (sContext.equals("activity")) {
                        reviewActivity = (WriteReviewFragmentActivity) getActivity();
                        reviewActivity.onLinkedInClick();
                    } else {
                        Intent myIntent = new Intent(getActivity(), LinkedinDialog.class);
                        startActivityForResult(myIntent, 1);
                    }
                }
                break;
            default:
                break;
        }
    }

    private boolean checkValidation() {
        if (utility.validateText(editUserName, getActivity().getResources().getString(R.string.msg_user_name))) {
            if (utility.validateText(editPassword, getActivity().getResources().getString(R.string.msg_password))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onTouch(View mView, MotionEvent arg1) {
        switch (mView.getId()) {
            case R.id.editUserName:
                editUserName.setFocusableInTouchMode(true);
                editUserName.requestFocusFromTouch();
                break;

            case R.id.editPassword:
                editPassword.setFocusableInTouchMode(true);
                editPassword.requestFocusFromTouch();
                break;

            default:
                break;
        }
        return false;
    }

    private void registerFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("header", textHeaderTitle.getText().toString());
        RegisterFragment fRegister = new RegisterFragment();
        FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.flLoginFragment, fRegister);
        fRegister.setArguments(bundle);
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();
    }

    private void redirectToAccount() {
        Bundle mBundle = new Bundle();
        AccountFragment fAccount = new AccountFragment();
        FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.flMeContent, fAccount);
        mBundle.putBoolean("show", true);
        fAccount.setArguments(mBundle);
        mFragmentTransaction.commit();
        getFragmentManager().beginTransaction().remove(LoginFragment.this).commit();
    }


    private void loginAsyncCall() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("user", editUserName.getText().toString());
        stringHashMap.put("password", editPassword.getText().toString());
        stringHashMap.put("platform", "android");
        stringHashMap.put("devicetoken", preferenceLogin.getString("regid", ""));

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjLogin = jsonArray.getJSONObject(0);

                    if (jObjLogin.getInt("code") != 1) {
                        utility.showMessageDialog(jObjLogin.getString("message"));
                    } else {
                        preferenceLogin = getActivity().getSharedPreferences(Constant.LOGIN_PREFERENCE, Context.MODE_PRIVATE);
                        Editor mEditor = preferenceLogin.edit();
                        mEditor.putString("key", jObjLogin.getString("key"));
                        mEditor.putString("uiduser", jObjLogin.getString("uiduser"));
                        mEditor.apply();

                        if (cbRemember.isChecked()) {
                            utility.rememberMe(getActivity(), editUserName.getText().toString(), editPassword.getText().toString());
                        } else {
                            utility.removeRememberMe();
                        }
                        if (tabName != null) {
                            if (tabName.equals("me") && preferenceLogin.contains("key")) {
                                redirectToAccount();
                            } else {
                                getFragmentManager().beginTransaction().remove(LoginFragment.this).commit();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_LOGIN);
    }

    @Override
    public void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (preferenceLogin.contains("key") && preferenceLogin.contains("social") && responseCode == Activity.RESULT_OK && requestCode == 1) {
            MeFragment mFragment = new MeFragment();
            FragmentTransaction fTransaction = getFragmentManager().beginTransaction();
            fTransaction.replace(R.id.flLoginFragment, mFragment);
            fTransaction.commit();
        }
    }
}