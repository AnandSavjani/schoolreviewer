package com.schoolreviewer.me;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;

public class MeFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_me, null);

        FragmentManager fManager = getFragmentManager();
        fManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (sharedPreferences.contains("key")) {
            redirectToAccountFragment();
        } else {
            redirectToLoginFragment();
        }
        return v;
    }

    private void redirectToAccountFragment() {
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("show", false);
        AccountFragment fAccount = new AccountFragment();
        FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
        fAccount.setArguments(mBundle);
        mFragmentTransaction.replace(R.id.flMeContent, fAccount).commit();
    }

    private void redirectToLoginFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("header", getResources().getString(R.string.txt_my_account));
        bundle.putString("tab", "me");
        bundle.putString("context", "fragment");
        LoginFragment fLogin = new LoginFragment();
        FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.flMeContent, fLogin);
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fLogin.setArguments(bundle);
        mFragmentTransaction.commit();
    }
}