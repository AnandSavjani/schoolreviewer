package com.schoolreviewer.me;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.schoolreviewer.R;
import com.schoolreviewer.bean.AnswerBean;
import com.schoolreviewer.bean.MyQuestionBean;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.MyUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyQuestionFragment extends Fragment implements View.OnClickListener {

    private View mView;
    private TextView textMQueTitle, textMQue, textMQTime, textHeaderTitle, textAns, textAnswerBy, textAnsTime;
    private Button btnAnswer;
    private SharedPreferences preferences;
    private MyQuestionBean myQuestionBean;
    private Bundle bundle;
    private MyUtility myUtility;
    private LinearLayout llMyanswer;
    private AnswerBean answerBean;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = View.inflate(getActivity(), R.layout.fragment_my_question, null);
        initControl(mView);
        return mView;
    }

    private void initControl(View view) {
        bundle = getArguments();
        myUtility = new MyUtility(getActivity());

        textMQueTitle = (TextView) view.findViewById(R.id.textMQueTitle);
        textMQue = (TextView) view.findViewById(R.id.textMQue);
        textMQTime = (TextView) view.findViewById(R.id.textMQTime);
        textHeaderTitle = (TextView) view.findViewById(R.id.textHeaderTitle);
        textAns = (TextView) view.findViewById(R.id.textAns);
        textAnsTime = (TextView) view.findViewById(R.id.textAnsTime);

        llMyanswer = (LinearLayout) view.findViewById(R.id.llMyanswer);

        btnAnswer = (Button) mView.findViewById(R.id.btnAnswer);
        btnAnswer.setOnClickListener(this);
        myQuestionBean = (MyQuestionBean) bundle.getSerializable("question");
        if (bundle.getBoolean("isQuestion")) {
            textHeaderTitle.setText(getActivity().getResources().getString(R.string.txt_question));
            llMyanswer.setVisibility(View.GONE);
        } else {
            answerBean = (AnswerBean) bundle.getSerializable("answer");
            textHeaderTitle.setText(getActivity().getResources().getString(R.string.txt_answer));
            llMyanswer.setVisibility(View.VISIBLE);

            textAns.setText(Html.fromHtml(answerBean.getAnswer()));
//            textAnswerBy.setText(answerBean.getRelationship() + " | " + answerBean.getAnsweryear());
            textAnsTime.setText("Answered : " + myUtility.dateDifference(answerBean.getAnsweredat(), answerBean.getTimenow()));

        }
        textMQueTitle.setText(myQuestionBean.getTitle());
        textMQue.setText(Html.fromHtml(myQuestionBean.getQuestion()));
        textMQTime.setText("Asked : " + myUtility.dateDifference(myQuestionBean.getAskedat(), myQuestionBean.getTimenow()));
        if (!myQuestionBean.getQuestionyear().equals("")) {
            String sReviewYear = myQuestionBean.getQuestionyear();
            sReviewYear = sReviewYear.replace("Year ", "");
            myQuestionBean.setQuestionyear("Year : " + sReviewYear);
        }

        preferences = getActivity().getSharedPreferences(Constant.LOGIN_PREFERENCE, Context.MODE_PRIVATE);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnAnswer) {
//            Intent intent = new Intent(getActivity(), AskQuestionActivity.class);
//            intent.putExtra("uid", myBean.getUidschool());
//            intent.putExtra("question", false);
//            startActivity(intent);
        }
    }

    class MyAnswerAsync extends AsyncTask<Void, Void, String> {

        List<NameValuePair> questionParam;
        MyQuestionBean myQuestionBean;
        ArrayList<MyQuestionBean> listMyQuestion;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            questionParam = new ArrayList<>();
            listMyQuestion = new ArrayList<>();
        }

        @Override
        protected String doInBackground(Void... params) {
            questionParam.add(new BasicNameValuePair("key", preferences.getString("key", null)));
//            return queParser.request(Constant.BASE_URL + Constant.URL_USER_QUESTION, questionParam);
            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("ok")) {
                    JSONObject msgObj = jsonObject.getJSONObject("message");

                    JSONArray questionArray = msgObj.getJSONArray("questions");

                    for (int i = 0; i < questionArray.length(); i++) {
                        JSONObject questionObj = questionArray.getJSONObject(i);
                        myQuestionBean = new MyQuestionBean();
                        myQuestionBean.setUid(questionObj.getString("uid"));
                        myQuestionBean.setUidschool(questionObj.getString("uidschool"));
                        myQuestionBean.setSchoolname(questionObj.getString("schoolname"));
                        myQuestionBean.setTitle(questionObj.getString("title"));
                        myQuestionBean.setAskedat(questionObj.getString("askedat"));
                        myQuestionBean.setQuestion(questionObj.getString("question"));
                        myQuestionBean.setQuestionyear(questionObj.getString("questionyear"));
                        myQuestionBean.setNumberviews(questionObj.getString("numberviews"));

                        myQuestionBean.setNumberlikes(questionObj.getString("numberlikes"));

                        myQuestionBean.setNumberdislikes(questionObj.getString("numberdislikes"));
                        myQuestionBean.setRelationship(questionObj.getString("relationship"));
                        listMyQuestion.add(myQuestionBean);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
