package com.schoolreviewer.me;

import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class RegisterFragment extends BaseFragment implements OnClickListener, OnTouchListener, TextWatcher {

    private TextView textHeaderTitle;
    private SeekBar sbStrength;
    private EditText editDisplayName, editEmailAddress, editPassword, editConfirmPassword;
    private Button btnRegister;
    int valueOfProgress;
    private HashMap<String, String> stringHashMap;
    private Bundle mBundle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_register, null);
        initControl(mView);
        mView.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().beginTransaction().remove(RegisterFragment.this).commit();
                }
                return false;
            }
        });
        return mView;
    }

    void initControl(View v) {
        mBundle = this.getArguments();

        textHeaderTitle = (TextView) getActivity().findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(mBundle.getString("header"));

        utility.hideSoftKeyboard();
        editDisplayName = (EditText) v.findViewById(R.id.editDisplayName);
        editEmailAddress = (EditText) v.findViewById(R.id.editEmailAddress);
        editPassword = (EditText) v.findViewById(R.id.editPassword);
        editConfirmPassword = (EditText) v.findViewById(R.id.editConfirmPassword);

        sbStrength = (SeekBar) v.findViewById(R.id.sbStrength);
        sbStrength.setThumb(null);

        sbStrength.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                return true;
            }
        });

        sbStrength.setProgressDrawable(getResources().getDrawable(R.drawable.progress_seekbar));

        editDisplayName.setOnTouchListener(this);
        editConfirmPassword.setOnTouchListener(this);
        editEmailAddress.setOnTouchListener(this);
        editPassword.setOnTouchListener(this);

        editPassword.addTextChangedListener(this);
        editConfirmPassword.setOnTouchListener(this);

        btnRegister = (Button) v.findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
    }

    boolean checkPassword() {
        if (editPassword.getText().toString().trim().equals(editConfirmPassword.getText().toString().trim()))
            return true;
        else
            return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:
                if (checkValidation()) {
                    utility.hideSoftKeyboard();
                    if (utility.checkInternetConnection()) {
                        registerAsyncCall();
                    }
                }
                break;

            default:
                break;
        }

    }

    @Override
    public boolean onTouch(View view, MotionEvent arg1) {
        switch (view.getId()) {
            case R.id.editDisplayName:
                editDisplayName.setFocusableInTouchMode(true);
                editDisplayName.requestFocusFromTouch();
                break;
            case R.id.editEmailAddress:
                editEmailAddress.setFocusableInTouchMode(true);
                editEmailAddress.requestFocusFromTouch();
                break;
            case R.id.editPassword:
                editPassword.setFocusableInTouchMode(true);
                editPassword.requestFocusFromTouch();
                break;
            case R.id.editConfirmPassword:
                editConfirmPassword.setFocusableInTouchMode(true);
                editConfirmPassword.requestFocusFromTouch();

            default:
                break;
        }
        return false;
    }

    void setProgress() {
        valueOfProgress = utility.measurePasswordStrength(editPassword.getText().toString());

        if (valueOfProgress == 0) {
            sbStrength.setProgress(10);
            setProgressBarColor(getResources().getColor(R.color.color_red));
        } else if (valueOfProgress == 1) {
            sbStrength.setProgress(50);
            setProgressBarColor(getResources().getColor(R.color.color_orange));
        } else if (valueOfProgress == 2) {
            sbStrength.setProgress(sbStrength.getMax());
            setProgressBarColor(getResources().getColor(R.color.color_green));
        }
    }

    private boolean checkValidation() {
        if (utility.validateText(editDisplayName, context.getResources().getString(R.string.msg_display_name))) {
            if (utility.validateEmail(editEmailAddress, context.getResources().getString(R.string.msg_provide_email))) {
                if (utility.validateText(editPassword, context.getResources().getString(R.string.msg_valid_password))) {
                    if (utility.validateText(editConfirmPassword, context.getResources().getString(R.string.msg_confirm_password))) {
                        if (!editPassword.getText().toString().trim().equals(editConfirmPassword.getText().toString().trim())) {
                            utility.showMessageDialog(context.getResources().getString(R.string.msg_password_not_match));
                        } else {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    @Override
    public void afterTextChanged(Editable arg0) {
    }

    @Override
    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
    }

    @Override
    public void onTextChanged(CharSequence arg0, int charLength, int arg2, int count) {
        if (arg0.length() == 1 && count == 1) {
            sbStrength.setProgress(10);
            setProgressBarColor(getResources().getColor(R.color.color_red));
        }
        setProgress();
        if (charLength == 0 && count == 0) {
            sbStrength.setProgress(0);
        }
    }

    public void setProgressBarColor(int newColor) {
        LayerDrawable ld = (LayerDrawable) sbStrength.getProgressDrawable();
        ClipDrawable d1 = (ClipDrawable) ld.findDrawableByLayerId(android.R.id.progress);
        d1.setColorFilter(newColor, Mode.SRC_IN);
    }

    private void registerAsyncCall() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("displayname", editDisplayName.getText().toString());
        stringHashMap.put("password", editPassword.getText().toString());
        stringHashMap.put("email", editEmailAddress.getText().toString());
        stringHashMap.put("lang", "en");

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjUserDetail = jsonArray.getJSONObject(0);

                    if (jObjUserDetail.getInt("code") == 19) {
                        utility.showMessageDialog(context.getResources().getString(R.string.msg_register)
                                + context.getResources().getString(R.string.msg_register_complete));
                        redirectToLoginFragment();
                    } else {
                        utility.showMessageDialog(getResources().getString(R.string.msg_not_register));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_REGISTER);
    }

    private void redirectToLoginFragment() {
        MeFragment fMe = new MeFragment();
        FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.flLoginFragment, fMe);
        mFragmentTransaction.commit();
    }
}
