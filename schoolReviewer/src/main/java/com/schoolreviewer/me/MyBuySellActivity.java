package com.schoolreviewer.me;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.schoolreviewer.BaseActivity;
import com.schoolreviewer.R;
import com.schoolreviewer.adapter.MyBuySellAdapter;
import com.schoolreviewer.bean.ImagesBean;
import com.schoolreviewer.bean.ItemBean;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyBuySellActivity extends BaseActivity {

    private HashMap<String, String> stringHashMap;
    private ListView lvItemSale;
    private MyBuySellAdapter mySellAdapter;
    private List<ItemBean> listSellItems, listPurchaseItems;
    private TextView textNoSell, textHeaderTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_buy_sell);
        initControl();
    }

    private void initControl() {
        stringHashMap = new HashMap<>();
        listSellItems = new ArrayList<>();
        listPurchaseItems = new ArrayList<>();

        textNoSell = (TextView) findViewById(R.id.textNoSell);
        textHeaderTitle = (TextView) findViewById(R.id.textHeaderTitle);

        textHeaderTitle.setText(R.string.lbl_my_buy_and_sell);

        mySellAdapter = new MyBuySellAdapter(this, listSellItems);

        lvItemSale = (ListView) findViewById(R.id.lvItemSale);

        lvItemSale.setAdapter(mySellAdapter);

        stringHashMap.put("key", sharedPreferences.getString("key", ""));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (utility.checkInternetConnection()) {
            listSellItems.clear();
            listPurchaseItems.clear();
            getUserItemSell();
            getUserItemPurchased();
        }
    }

    private void getUserItemPurchased() {
        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 38) {
                        JSONArray arrayItems = jsonObject.getJSONObject("message").getJSONArray("items");
                        for (int i = 0; i < arrayItems.length(); i++) {
                            JSONObject objItems = arrayItems.getJSONObject(i);
                            ItemBean itemBean = new ItemBean();
                            itemBean.setUid(objItems.getString("uid"));
                            itemBean.setCategory(objItems.getString("category"));
                            itemBean.setTitle(objItems.getString("title"));
                            itemBean.setSeller(objItems.getString("seller"));
                            itemBean.setUidseller(objItems.getString("uidseller"));
                            itemBean.setCondition(objItems.getString("condition"));
                            itemBean.setPrice(objItems.getString("price"));
                            itemBean.setCurrency(objItems.getString("currency"));
                            itemBean.setDescription(objItems.getString("description"));
                            itemBean.setForcollection("1");
                            itemBean.setPurchase(true);

                            JSONArray arrayImages = objItems.getJSONArray("imageurl");

                            List<ImagesBean> listImages = new ArrayList<>();

                            for (int j = 0; j < arrayImages.length(); j++) {
                                ImagesBean imagesBean = new ImagesBean();
                                imagesBean.setUrl(Constant.BASE_IMAGE_URL + arrayImages.getJSONObject(j).getString("url"));
                                listImages.add(imagesBean);
                            }
                            itemBean.setListImages(listImages);

                            listPurchaseItems.add(itemBean);
                        }

                    } else {
                        Toast.makeText(MyBuySellActivity.this, "Something went wrong.Please try again", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_USER_ITEMS_PURCHASED);
    }

    private void getUserItemSell() {
        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 37) {
                        JSONArray arrayItems = jsonObject.getJSONObject("message").getJSONArray("items");
                        if (arrayItems.length() > 0) {
                            for (int i = 0; i < arrayItems.length(); i++) {
                                JSONObject objItems = arrayItems.getJSONObject(i);
                                ItemBean itemBean = new ItemBean();
                                itemBean.setUid(objItems.getString("uid"));
                                itemBean.setCategory(objItems.getString("category"));
                                itemBean.setSold(objItems.getString("sold"));
                                itemBean.setTitle(objItems.getString("title"));
                                itemBean.setUidseller(objItems.getString("uidseller"));
                                itemBean.setCondition(objItems.getString("condition"));
                                itemBean.setPrice(objItems.getString("price"));
                                itemBean.setCurrency(objItems.getString("currency"));
                                itemBean.setDescription(objItems.getString("description"));
                                itemBean.setForcollection(objItems.getString("forcollection"));
                                itemBean.setFordelivery(objItems.getString("fordelivery"));
                                itemBean.setFormanualpayment(objItems.getString("formanualpayment"));
                                itemBean.setForonlinepayment(objItems.getString("foronlinepayment"));
                                itemBean.setSellerpaypal(objItems.getString("sellerpaypal"));
                                itemBean.setContactemail(objItems.getString("contactemail"));
                                itemBean.setContactphone(objItems.getString("contactphone"));
                                itemBean.setPurchase(false);

                                JSONArray arrayImage = objItems.getJSONArray("imageurl");
                                List<ImagesBean> listImages = new ArrayList<>();

                                for (int j = 0; j < arrayImage.length(); j++) {
                                    ImagesBean imagesBean = new ImagesBean();
                                    imagesBean.setUid(arrayImage.getJSONObject(j).getString("uid"));
                                    imagesBean.setUrl(Constant.BASE_IMAGE_URL + arrayImage.getJSONObject(j).getString("url"));
                                    listImages.add(imagesBean);
                                }
                                itemBean.setListImages(listImages);

                                listSellItems.add(itemBean);
                            }
                            mySellAdapter.notifyDataSetChanged();
                            lvItemSale.setVisibility(View.VISIBLE);
                            textNoSell.setVisibility(View.GONE);
                        } else {
                            lvItemSale.setVisibility(View.GONE);
                            textNoSell.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Toast.makeText(MyBuySellActivity.this, "Something went wrong.Please try again", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_USER_ITEMS_FOR_SALE);
    }
}
