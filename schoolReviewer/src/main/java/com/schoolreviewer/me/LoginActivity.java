package com.schoolreviewer.me;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.schoolreviewer.BaseActivity;
import com.schoolreviewer.R;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.FacebookActivity;
import com.schoolreviewer.util.GplusActivity;
import com.schoolreviewer.util.LinkedinDialog;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends BaseActivity implements OnClickListener {

    private Bundle mBundle;
    private EditText editUserName, editPassword;
    private Button btnLogin, btnRegister;
    private ImageView imgSocialFb, imgSocialGplus, imgSocialLinkedIn;
    private TextView textHeaderTitle, textForgotPassword;
    private HashMap<String, String> stringHashMap;
    private SharedPreferences preferenceLogin, preferenceRemember;
    private CheckBox cbRemember;
    private FrameLayout flHeaderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);
        mBundle = getIntent().getExtras();
        initControl();
    }

    private void initControl() {
        flHeaderLayout = (FrameLayout) findViewById(R.id.llHeaderLayout);
        flHeaderLayout.setVisibility(View.VISIBLE);

        textHeaderTitle = (TextView) findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(mBundle.getString("header"));

        textForgotPassword = (TextView) findViewById(R.id.textForgotPassword);
        textForgotPassword.setOnClickListener(this);

        editPassword = (EditText) findViewById(R.id.editPassword);
        editUserName = (EditText) findViewById(R.id.editUserName);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegister = (Button) findViewById(R.id.btnRegisterOption);
        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);

        imgSocialGplus = (ImageView) findViewById(R.id.imgSocialGplus);
        imgSocialFb = (ImageView) findViewById(R.id.imgSocialFb);
        imgSocialLinkedIn = (ImageView) findViewById(R.id.imgSocialLinkedIn);
        imgSocialGplus.setOnClickListener(this);
        imgSocialFb.setOnClickListener(this);
        imgSocialLinkedIn.setOnClickListener(this);

        preferenceLogin = getSharedPreferences(Constant.LOGIN_PREFERENCE, Context.MODE_PRIVATE);
        preferenceRemember = getSharedPreferences(Constant.REMEMBER_PREFERENCE, Context.MODE_PRIVATE);
        cbRemember = (CheckBox) findViewById(R.id.cbRemember);

        if (preferenceRemember.contains("name") && preferenceRemember.contains("password")) {
            editUserName.setText(preferenceRemember.getString("name", ""));
            editPassword.setText(preferenceRemember.getString("password", ""));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textForgotPassword:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                break;
            case R.id.btnLogin:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editPassword.getWindowToken(), 0);
                if (checkValidation()) {
                    if (utility.checkInternetConnection()) {
                        preferenceLogin.edit().clear().apply();
                        loginAsyncCall();
                    }
                }
                break;
            case R.id.btnRegisterOption:
                utility.hideSoftKeyboard();
                registerFragment();
                break;
            case R.id.imgSocialGplus:
                onGplusClick();
                break;
            case R.id.imgSocialFb:
                onFbClick();
                break;
            case R.id.imgSocialLinkedIn:
                onLinkedInClick();
                break;
            default:
                break;
        }
    }

    private boolean checkValidation() {
        if (utility.validateText(editUserName, getResources().getString(R.string.msg_user_name))) {
            if (utility.validateText(editPassword, getResources().getString(R.string.msg_password))) {
                return true;
            }
        }
        return false;
    }

    private void registerFragment() {
        Bundle b = new Bundle();
        b.putString("header", textHeaderTitle.getText().toString());
        RegisterFragment fRegister = new RegisterFragment();
        FragmentTransaction mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.flLoginFragment, fRegister);
        fRegister.setArguments(b);
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();
    }

    private void loginAsyncCall() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("user", editUserName.getText().toString());
        stringHashMap.put("password", editPassword.getText().toString());
        stringHashMap.put("platform", "android");
        stringHashMap.put("devicetoken", preferenceLogin.getString("regid", ""));

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjLogin = jsonArray.getJSONObject(0);
                    if (jObjLogin.has("key")) {
                        if (jObjLogin.getInt("code") != 1) {
                            utility.showMessageDialog(jObjLogin.getString("message"));
                        } else {
                            if (cbRemember.isChecked()) {
                                utility.rememberMe(LoginActivity.this, editUserName.getText().toString(), editPassword.getText().toString());
                            } else {
                                utility.removeRememberMe();
                            }
                            Editor loginEditor = preferenceLogin.edit();
                            loginEditor.putString("key", jObjLogin.getString("key"));
                            loginEditor.putString("uiduser", jObjLogin.getString("uiduser"));
                            loginEditor.apply();
                            setResult(RESULT_OK);
                            finish();
                        }
                    } else {
                        utility.showMessageDialog(jObjLogin.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_LOGIN);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
    }

    private void onLinkedInClick() {
        if (utility.checkInternetConnection()) {
            Intent intentLinked = new Intent(LoginActivity.this, LinkedinDialog.class);
            startActivityForResult(intentLinked, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent arg2) {
        if (requestCode == 1 && responseCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    public void onFbClick() {
        if (utility.checkInternetConnection()) {
            Intent intentFb = new Intent(this, FacebookActivity.class);
            startActivityForResult(intentFb, 1);
        }
    }

    public void onGplusClick() {
        if (utility.checkInternetConnection()) {
            Intent intentGplus = new Intent(this, GplusActivity.class);
            startActivityForResult(intentGplus, 1);
        }
    }
}