package com.schoolreviewer.adapter;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.schoolreviewer.R;
import com.schoolreviewer.bean.MyQuestionBean;

import java.util.ArrayList;

public class MyQuestionAdapter extends BaseAdapter {
    ViewHolder viewHolder;
    Context context;
    private ArrayList<MyQuestionBean> listQue;

    public MyQuestionAdapter(Context context, ArrayList<MyQuestionBean> listMyQuestion) {
        listQue = listMyQuestion;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listQue.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        viewHolder = new ViewHolder();
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.row_question, null);
            viewHolder.textQueHeader = (TextView) convertView.findViewById(R.id.textQueHeader);
            viewHolder.textQueContent = (TextView) convertView.findViewById(R.id.textQueContent);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textQueHeader.setText(listQue.get(position).getTitle());
        viewHolder.textQueContent.setText(Html.fromHtml(listQue.get(position).getQuestion()));
        return convertView;
    }

    private class ViewHolder {
        private TextView textQueHeader, textQueContent;
    }
}