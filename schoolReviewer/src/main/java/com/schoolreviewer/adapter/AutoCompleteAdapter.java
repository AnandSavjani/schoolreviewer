package com.schoolreviewer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.schoolreviewer.R;
import com.schoolreviewer.bean.AutoCompleteBean;

import java.util.List;

public class AutoCompleteAdapter extends ArrayAdapter<AutoCompleteBean> {

    private List<AutoCompleteBean> listName;
    Context mContext;
    int layoutResourceId;
    ViewHolder mHolder;

    public AutoCompleteAdapter(Context context, int resource, List<AutoCompleteBean> establishName) {
        super(context, resource, establishName);
        mContext = context;
        listName = establishName;
        layoutResourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        mHolder = new ViewHolder();
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(layoutResourceId, null);
            mHolder.textItem = (TextView) convertView.findViewById(R.id.textItem);
            mHolder.imgEsta = (ImageView) convertView.findViewById(R.id.imgEsta);
            convertView.setTag(mHolder);
        } else {
            mHolder = (ViewHolder) convertView.getTag();
        }

        if (listName.get(position).getEstaType().equals("place")) {
            mHolder.textItem.setText(listName.get(position).getEstaName());
            mHolder.imgEsta.setImageDrawable(mContext.getResources().getDrawable(R.drawable.location_icon));
        } else {
            mHolder.textItem.setText(listName.get(position).getEstaName());
            mHolder.imgEsta.setImageDrawable(mContext.getResources().getDrawable(R.drawable.hat_icon));
        }
        return convertView;
    }

    class ViewHolder {
        TextView textItem;
        ImageView imgEsta;
    }
}