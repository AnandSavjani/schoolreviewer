package com.schoolreviewer.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.schoolreviewer.R;
import com.schoolreviewer.bean.ExamQuestionBean;

import java.util.List;

public class ExamQuestionAdapter extends BaseAdapter {

    private Context context;
    private ViewHolder viewHolder;
    private List<ExamQuestionBean> listExamQuestion;
    private String key;

    public ExamQuestionAdapter(Context context, List<ExamQuestionBean> listExamQuestion, String key) {
        this.context = context;
        this.listExamQuestion = listExamQuestion;
        this.key = key;
    }

    @Override
    public int getCount() {
        return listExamQuestion.size();
    }

    @Override
    public ExamQuestionBean getItem(int position) {
        return listExamQuestion.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.row_exam_question, null);
            viewHolder.textAnsQuestion = (TextView) convertView.findViewById(R.id.textAnsQuestion);
            viewHolder.textOpen = (TextView) convertView.findViewById(R.id.textOpen);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // 0 = "VIEW" && 1 = "WATCHED"

        viewHolder.textAnsQuestion.setText(getItem(position).getName());

        if (!key.isEmpty()) {
            if (getItem(position).getHaswatched().equals("0")) {
                viewHolder.textOpen.setText("VIEW");
                viewHolder.textOpen.setBackgroundColor(ContextCompat.getColor(context, R.color.color_green));
            } else {
                viewHolder.textOpen.setText("WATCHED");
                viewHolder.textOpen.setBackgroundColor(ContextCompat.getColor(context, R.color.color_gray));
            }

        } else {
            viewHolder.textOpen.setText("PURCHASE");
            viewHolder.textOpen.setBackgroundColor(ContextCompat.getColor(context, R.color.color_gray));
        }
        return convertView;
    }

    private class ViewHolder {
        private TextView textAnsQuestion, textOpen;
    }
}