package com.schoolreviewer.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

public class HintAdapter extends ArrayAdapter<String> {

    public HintAdapter(Context theContext, int objects, List<String> theLayoutResId) {
        super(theContext, objects, theLayoutResId);
    }

    @Override
    public int getCount() {
        // don't display last item. It is used as hint.
        int count = super.getCount();
        return count > 0 ? count - 1 : count;
    }
}