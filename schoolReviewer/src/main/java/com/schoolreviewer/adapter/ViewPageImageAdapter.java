package com.schoolreviewer.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.schoolreviewer.R;
import com.schoolreviewer.bean.ImagesBean;

import java.util.ArrayList;
import java.util.List;

public class ViewPageImageAdapter extends PagerAdapter {

    private Context mContext;
    private List<ImagesBean> arrImgUrlBean;
    private ArrayList<String> arrImgUrl;
    private ImageView imageView;
    private boolean isBean = false;

    public ViewPageImageAdapter(Context context, List<ImagesBean> arrImg, boolean isBean) {
        mContext = context;
        arrImgUrlBean = arrImg;
        this.isBean = isBean;
    }

    public ViewPageImageAdapter(Context context, ArrayList<String> arrImgUrl) {
        mContext = context;
        this.arrImgUrl = arrImgUrl;
    }

    @Override
    public int getCount() {
        if (isBean)
            return arrImgUrlBean.size();
        else
            return arrImgUrl.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        imageView = new ImageView(mContext);

        if (!isBean)
            Glide.with(mContext).load(arrImgUrl.get(position)).centerCrop().placeholder(R.drawable.buy_placeholder).priority(Priority.HIGH).into(imageView);
        else
            Glide.with(mContext).load(arrImgUrlBean.get(position).getUrl()).centerCrop().placeholder(R.drawable.buy_placeholder).priority(Priority.HIGH).into(imageView);

        LayoutParams imageParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        imageView.setLayoutParams(imageParams);

        LinearLayout layout = new LinearLayout(mContext);
        layout.setOrientation(LinearLayout.VERTICAL);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        layout.setLayoutParams(layoutParams);
        getItemPosition(layout);
        layout.addView(imageView);

        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
