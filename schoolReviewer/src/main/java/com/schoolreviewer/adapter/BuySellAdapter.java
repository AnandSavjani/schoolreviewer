package com.schoolreviewer.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.schoolreviewer.R;
import com.schoolreviewer.bean.ItemBean;
import com.schoolreviewer.buysell.BuySellDetailsActivity;
import com.schoolreviewer.util.Constant;

import java.util.List;

public class BuySellAdapter extends BaseAdapter {

    private Context context;
    private ViewHolder viewHolder;
    private List<ItemBean> listItems;

    public BuySellAdapter(Context context, List<ItemBean> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public ItemBean getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.row_product_category, null);

            viewHolder.imgProduct = (ImageView) convertView.findViewById(R.id.imgProduct);
            viewHolder.textPrice = (TextView) convertView.findViewById(R.id.textPrice);
            viewHolder.textProductDesc = (TextView) convertView.findViewById(R.id.textProductDesc);
            viewHolder.textProductDistributor = (TextView) convertView.findViewById(R.id.textProductDistributor);
            viewHolder.textProductName = (TextView) convertView.findViewById(R.id.textProductName);
            viewHolder.btnViewItem = (TextView) convertView.findViewById(R.id.btnViewItem);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BuySellDetailsActivity.class);
                intent.putExtra("item", getItem(position));
                intent.putExtra("isFromUser", false);
                context.startActivity(intent);
            }
        });

        viewHolder.textPrice.setText(Constant.POUND + getItem(position).getPrice());
        viewHolder.textProductDesc.setText(Html.fromHtml(getItem(position).getDescription()));
        viewHolder.textProductDistributor.setText("Seller : " + getItem(position).getSeller());
        viewHolder.textProductName.setText(Html.fromHtml(getItem(position).getTitle()));

        if (!getItem(position).getListImages().isEmpty())
            Glide.with(context).load(getItem(position).getListImages().get(0).getUrl()).centerCrop().priority(Priority.HIGH).placeholder(R.drawable.buy_placeholder).into(viewHolder.imgProduct);
        else {
            Glide.with(context).load(R.drawable.buy_placeholder).centerCrop().priority(Priority.HIGH).placeholder(R.drawable.buy_placeholder).into(viewHolder.imgProduct);
        }

        return convertView;
    }

    private class ViewHolder {
        private ImageView imgProduct;
        private TextView textProductName, textProductDistributor, textProductDesc, textPrice, btnViewItem;
    }
}
