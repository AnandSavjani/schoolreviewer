package com.schoolreviewer.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.schoolreviewer.R;
import com.schoolreviewer.bean.ExamBean;
import com.schoolreviewer.exam.ExamPaperActivity;
import com.schoolreviewer.util.Constant;

import java.util.List;

public class ExamAdapter extends BaseAdapter {

    private Context context;
    private ViewHolder viewHolder;
    private List<ExamBean> listExams;
    private SharedPreferences sharedPreferences;

    public ExamAdapter(Context context, List<ExamBean> listExams) {
        this.context = context;
        this.listExams = listExams;
        sharedPreferences = context.getSharedPreferences(Constant.LOGIN_PREFERENCE, Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        return listExams.size();
    }

    @Override
    public ExamBean getItem(int position) {
        return listExams.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.row_exam_paper, null);

            viewHolder.textLevel = (TextView) convertView.findViewById(R.id.textLevel);
            viewHolder.textExamDesc = (TextView) convertView.findViewById(R.id.textExamDesc);
            viewHolder.textExamTitle = (TextView) convertView.findViewById(R.id.textExamTitle);
            viewHolder.btnViewPaper = (Button) convertView.findViewById(R.id.btnViewPaper);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.btnViewPaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (sharedPreferences.contains("key"))
//                    context.startActivity(new Intent(context, ExamPaperActivity.class).putExtra("examuid", getItem(position).getUid()));
//                else
//                    context.startActivity(new Intent(context, LoginActivity.class).putExtra("header", context.getResources().getString(R.string.txt_login)));
                context.startActivity(new Intent(context, ExamPaperActivity.class).putExtra("examuid", getItem(position).getUid()));
            }
        });

        viewHolder.textExamTitle.setText(getItem(position).getName());
        viewHolder.textLevel.setText(getItem(position).getLevel());
        viewHolder.textExamDesc.setText(getItem(position).getDescription());

        return convertView;
    }

    private class ViewHolder {
        private TextView textLevel, textExamTitle, textExamDesc;
        private Button btnViewPaper;
    }
}
