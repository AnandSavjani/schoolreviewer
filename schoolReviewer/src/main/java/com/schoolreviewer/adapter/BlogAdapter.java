package com.schoolreviewer.adapter;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.schoolreviewer.R;
import com.schoolreviewer.bean.BlogBean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BlogAdapter extends BaseAdapter {

    Context context;
    private List<BlogBean> listItems;
    private SimpleDateFormat simpleDateFormat, simpleDateFormat2;

    public BlogAdapter(Context context, ArrayList<BlogBean> arrayBlog) {
        this.context = context;
        this.listItems = arrayBlog;
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat2 = new SimpleDateFormat("dd MMM yyyy");
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public BlogBean getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();
        final int pos = position;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.row_blog, null);
            viewHolder.imgBlog = (ImageView) convertView.findViewById(R.id.imgBlog);
            viewHolder.textBlogDesc = (TextView) convertView.findViewById(R.id.textBlogDesc);
            viewHolder.textBlogTitle = (TextView) convertView.findViewById(R.id.textBlogTitle);
            viewHolder.textDate = (TextView) convertView.findViewById(R.id.textDate);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textBlogDesc.setText(Html.fromHtml(listItems.get(position).getContent()));
        viewHolder.textBlogTitle.setText(listItems.get(position).getTitle());
        String date = listItems.get(position).getDate().substring(0, listItems.get(position).getDate().indexOf("T"));
        try {
            Date date1 = simpleDateFormat.parse(date);
            viewHolder.textDate.setText(simpleDateFormat2.format(date1));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Glide.with(context).load(listItems.get(position).getGuid()).centerCrop().priority(Priority.HIGH).placeholder(R.drawable.buy_placeholder).into(viewHolder.imgBlog);
        return convertView;
    }

    private class ViewHolder {
        private ImageView imgBlog;
        private TextView textDate, textBlogTitle, textBlogDesc;
    }
}
