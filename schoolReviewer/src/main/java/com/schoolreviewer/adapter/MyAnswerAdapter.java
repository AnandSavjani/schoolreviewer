package com.schoolreviewer.adapter;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.schoolreviewer.R;
import com.schoolreviewer.bean.AnswerBean;
import com.schoolreviewer.bean.MyQuestionBean;

import java.util.ArrayList;

public class MyAnswerAdapter extends BaseAdapter {

    private ViewHolder viewHolder;
    private ArrayList<AnswerBean> listAnswer;
    private ArrayList<MyQuestionBean> listAnswerQue;
    private Context context;

    public MyAnswerAdapter(Context context, ArrayList<AnswerBean> answerList, ArrayList<MyQuestionBean> questionList) {
        listAnswer = answerList;
        listAnswerQue = questionList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listAnswerQue.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.row_question, null);
            viewHolder = new ViewHolder();
            viewHolder.textQueHeader = (TextView) convertView.findViewById(R.id.textQueHeader);
            viewHolder.textQueContent = (TextView) convertView.findViewById(R.id.textQueContent);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
//        viewHolder.textQueHeader.setMinLines(listAnswerQue.get(position).getTitle().length() / 35);
//        viewHolder.textQueContent.setMinLines(listAnswer.get(position).getAnswer().length() / 30);

        viewHolder.textQueHeader.setText(listAnswerQue.get(position).getTitle());
        viewHolder.textQueContent.setText(Html.fromHtml(listAnswer.get(position).getAnswer()));
        return convertView;
    }

    private class ViewHolder {
        TextView textQueHeader, textQueContent;
    }
}
