package com.schoolreviewer.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.schoolreviewer.R;
import com.schoolreviewer.bean.AnswerBean;
import com.schoolreviewer.home.AbuseReportActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class ForumCommentAdapter extends BaseAdapter {

    private Context context;
    private List<AnswerBean> listAnswerBean;
    private DateFormat inputFormatter1, outputFormatter1;
    private ViewHolder holder;

    public ForumCommentAdapter(Context context, List<AnswerBean> listAnswerBean) {
        this.context = context;
        this.listAnswerBean = listAnswerBean;
        inputFormatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        outputFormatter1 = new SimpleDateFormat("dd MMM", Locale.getDefault());
    }

    @Override
    public int getCount() {
        return listAnswerBean.size();
    }

    @Override
    public AnswerBean getItem(int position) {
        return listAnswerBean.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        holder = new ViewHolder();
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.row_topic_comment, null);
            holder.textUserName = (TextView) convertView.findViewById(R.id.textUserName);
            holder.textSchoolType = (TextView) convertView.findViewById(R.id.textSchoolType);
            holder.textTime = (TextView) convertView.findViewById(R.id.textTime);
            holder.textComment = (TextView) convertView.findViewById(R.id.textComment);
            holder.imgFlag = (ImageView) convertView.findViewById(R.id.imgFlag);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textComment.setText(Html.fromHtml(getItem(position).getAnswer()));
        holder.textSchoolType.setText(getItem(position).getCategory());

        try {
            Date date1 = inputFormatter1.parse(getItem(position).getAnsweredat());
            String output1 = outputFormatter1.format(date1);
            holder.textTime.setText(output1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.imgFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, AbuseReportActivity.class)
                        .putExtra("schooluid", getItem(position).getUidschool())
                        .putExtra("header", "Report Forum")
                        .putExtra("uidanswer", getItem(position).getUidansweredby()));
            }
        });

        holder.textUserName.setText(listAnswerBean.get(position).getDisplayname());
        return convertView;
    }


    private class ViewHolder {
        private TextView textUserName, textSchoolType, textTime, textComment;
        private ImageView imgFlag;
    }
}
