package com.schoolreviewer.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.schoolreviewer.R;
import com.schoolreviewer.bean.ItemBean;
import com.schoolreviewer.buysell.BuySellDetailsActivity;
import com.schoolreviewer.buysell.SellItemActivity;

import java.util.List;

public class MyBuySellAdapter extends BaseAdapter {

    private Context context;
    private ViewHolder viewHolder;
    private List<ItemBean> listItems;

    public MyBuySellAdapter(Context context, List<ItemBean> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public ItemBean getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.row_my_buy_sell, null);
            viewHolder.imgProductIcon = (ImageView) convertView.findViewById(R.id.imgProductIcon);
            viewHolder.textProductName = (TextView) convertView.findViewById(R.id.textProductName);
            viewHolder.textAction = (TextView) convertView.findViewById(R.id.textAction);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (getItem(position).isPurchase()) {
            viewHolder.textAction.setText("VIEW");
        } else {
            viewHolder.textAction.setText("EDIT");
        }

        viewHolder.textAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getItem(position).isPurchase()) {
                    Intent intent = new Intent(context, BuySellDetailsActivity.class);
                    intent.putExtra("isFromUser", true);
                    intent.putExtra("item", getItem(position));
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, SellItemActivity.class);
                    intent.putExtra("uid", getItem(position).getUid());
                    intent.putExtra("isupdate", true);
                    intent.putExtra("itembean", getItem(position));
                    context.startActivity(intent);
                }
            }
        });

        viewHolder.textProductName.setText(getItem(position).getTitle());
        if (getItem(position).getListImages().size() > 0)
            Glide.with(context).load(getItem(position).getListImages().get(0).getUrl()).placeholder(R.drawable.buy_placeholder).priority(Priority.HIGH).centerCrop().into(viewHolder.imgProductIcon);
        else
            Glide.with(context).load(R.drawable.buy_placeholder).centerCrop().priority(Priority.HIGH).placeholder(R.drawable.buy_placeholder).into(viewHolder.imgProductIcon);

        return convertView;
    }

    private class ViewHolder {
        private ImageView imgProductIcon;
        private TextView textProductName, textAction;
    }
}
