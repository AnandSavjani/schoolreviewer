package com.schoolreviewer.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.schoolreviewer.R;
import com.schoolreviewer.bean.SearchItemBean;
import com.schoolreviewer.util.Constant;

import java.util.ArrayList;

public class SearchResultAdapter extends BaseAdapter {
    LayoutInflater mLayoutInflater = null;
    private Context context;
    private ArrayList<SearchItemBean> arrSchoolList;

    public SearchResultAdapter(Context mContext, ArrayList<SearchItemBean> arrSchoolList) {
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        context = mContext;
        this.arrSchoolList = arrSchoolList;
    }

    @Override
    public int getCount() {
        return arrSchoolList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewgroup) {
        ViewHolder mHolder;

        if (view == null) {
            mHolder = new ViewHolder();
            view = mLayoutInflater.inflate(R.layout.row_search_result, viewgroup, false);
            mHolder.textSchoolName = (TextView) view.findViewById(R.id.textSchoolNameForResult);
            mHolder.textSchoolDetails = (TextView) view.findViewById(R.id.textSchoolDetails);
            mHolder.rbScore = (RatingBar) view.findViewById(R.id.rbScore);
            mHolder.imgSchoolImage = (ImageView) view.findViewById(R.id.imgSchoolPic);
            mHolder.textNLT = (TextView) view.findViewById(R.id.textTop100NLT);
            mHolder.textDistance = (TextView) view.findViewById(R.id.textDistance);

            view.setTag(mHolder);
        } else
            mHolder = (ViewHolder) view.getTag();

        mHolder.textSchoolName.setText(arrSchoolList.get(position).getName());
        Typeface tfForName = Typeface.createFromAsset(context.getAssets(), "raleway_extra_bold.otf");
        mHolder.textSchoolName.setTypeface(tfForName);

        mHolder.textSchoolDetails.setText(arrSchoolList.get(position).getNotes());
        Typeface tfForDetail = Typeface.createFromAsset(context.getAssets(), "raleway_medium.otf");
        mHolder.textSchoolDetails.setTypeface(tfForDetail);

        float fScore = Float.parseFloat(arrSchoolList.get(position).getScore());
        LayerDrawable stars = (LayerDrawable) mHolder.rbScore.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.color_yellow), PorterDuff.Mode.SRC_ATOP);

        mHolder.textNLT.setVisibility(View.GONE);
        mHolder.rbScore.setRating(fScore);
        mHolder.textDistance.setText(context.getResources().getString(R.string.txt_distance) + " " + arrSchoolList.get(position).getDistance() + "miles");
        if (!arrSchoolList.get(position).getDistance().equals("0")) {
            mHolder.textDistance.setVisibility(View.VISIBLE);
        }
        if (!arrSchoolList.get(position).getImageurl().equals("")) {
            Glide.with(context).load(Constant.BASE_IMAGE_URL + arrSchoolList.get(position).getImageurl()).centerCrop().placeholder(R.drawable.app_icon).priority(Priority.HIGH).into(mHolder.imgSchoolImage);
        } else {
            mHolder.imgSchoolImage.setImageResource(R.drawable.app_icon);
        }
        return view;
    }

    private class ViewHolder {
        ImageView imgSchoolImage;
        TextView textSchoolName, textSchoolDetails, textNLT, textDistance;
        RatingBar rbScore;
    }
}