package com.schoolreviewer.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.schoolreviewer.R;
import com.schoolreviewer.bean.SchoolQuestionBean;

import java.util.List;

public class ParentForumAdapter extends BaseAdapter {

    private Context context;
    private List<SchoolQuestionBean> listQuestionBean;
    private ViewHolder viewHolder;

    public ParentForumAdapter(Context context, List<SchoolQuestionBean> listQuestionBean) {
        this.context = context;
        this.listQuestionBean = listQuestionBean;
    }

    @Override
    public int getCount() {
        return listQuestionBean.size();
    }

    @Override
    public SchoolQuestionBean getItem(int position) {
        return listQuestionBean.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.row_parent_forum, null);
            viewHolder.textForum = (TextView) convertView.findViewById(R.id.textForum);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (position % 2 == 0) {
            viewHolder.textForum.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
        } else {
            viewHolder.textForum.setBackgroundColor(ContextCompat.getColor(context, R.color.color_offwhite));
        }
        viewHolder.textForum.setText(Html.fromHtml(getItem(position).getTitle()));

        return convertView;
    }

    private class ViewHolder {
        private TextView textForum;
    }
}
