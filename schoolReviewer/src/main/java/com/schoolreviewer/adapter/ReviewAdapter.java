package com.schoolreviewer.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.schoolreviewer.R;
import com.schoolreviewer.bean.ReviewBean;
import com.schoolreviewer.util.MyUtility;

import java.util.ArrayList;

public class ReviewAdapter extends BaseAdapter {

    private ViewHolder mViewHolder;
    private Context context;
    private ArrayList<ReviewBean> arrReviewList;
    private MyUtility utility;

    public ReviewAdapter(Context context, ArrayList<ReviewBean> arrReviewList) {
        this.context = context;
        this.arrReviewList = arrReviewList;
        utility = new MyUtility(context);
    }

    @Override
    public int getCount() {
        return arrReviewList.size();
    }

    @Override
    public ReviewBean getItem(int arg0) {
        return arrReviewList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        mViewHolder = new ViewHolder();
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.row_reviews, null);
            mViewHolder.textTitle = (TextView) convertView.findViewById(R.id.textTitle);
            mViewHolder.textDisplayname = (TextView) convertView.findViewById(R.id.textDisplayName);
            mViewHolder.textRelationship = (TextView) convertView.findViewById(R.id.textRelationOfReview);
            mViewHolder.rbScoreOfReview = (RatingBar) convertView.findViewById(R.id.rbReviewRating);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.textTitle.setText(getItem(position).getTitle().trim());
        mViewHolder.textDisplayname.setText(utility.checkDisplayName(getItem(position).getDisplayname()) + "   ");
        if (arrReviewList.get(position).getReviewyear().equals(""))
            mViewHolder.textRelationship.setText(getItem(position).getRelationship());
        else
            mViewHolder.textRelationship.setText(getItem(position).getRelationship() + " | "
                    + arrReviewList.get(position).getReviewyear());

        mViewHolder.rbScoreOfReview.setRating(getItem(position).getScoreOfReview());
        LayerDrawable stars = (LayerDrawable) mViewHolder.rbScoreOfReview.getProgressDrawable();
        if (context != null) {
            stars.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.color_yellow), PorterDuff.Mode.SRC_IN);
        }
        return convertView;
    }

    private class ViewHolder {
        private TextView textTitle, textDisplayname, textRelationship;
        private RatingBar rbScoreOfReview;
    }
}

