package com.schoolreviewer.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.schoolreviewer.R;
import com.schoolreviewer.me.LoginFragment;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.FacebookActivity;
import com.schoolreviewer.util.FontHelper;
import com.schoolreviewer.util.GplusActivity;
import com.schoolreviewer.util.LinkedinDialog;
import com.schoolreviewer.util.MyUtility;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class WriteReviewFragmentActivity extends FragmentActivity implements OnClickListener, OnFocusChangeListener {

    MyUtility myUtility = new MyUtility(this);
    private TextView textHeaderTitle;
    private EditText editReviewTitle, editReviewContent;
    private Spinner spRelationWithSchool, spYearGroupInReview;
    private RatingBar rbLeadership, rbQualityOfTeaching, rbApproachOfStaff, rbSafety, rbSport, rbMusic, rbOverAll, rbFood;
    Button btnAddReview;
    private ImageView imgAnonymousOff, imgAnonymousOn;
    private String setDisplayName = "yes";
    private SharedPreferences preferenceLogin;
    HashMap<String, String> stringHashMap;
    Bundle mBundle;
    static private String schoolUid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_review);
        initControl();
        preferenceLogin = getSharedPreferences(Constant.LOGIN_PREFERENCE, Context.MODE_PRIVATE);
        Intent in = getIntent();
        mBundle = in.getExtras();
        schoolUid = mBundle.getString("uid");
        if (!preferenceLogin.contains("key")) {
            redirectToLogin();
        }
    }

    @Override
    protected void onActivityResult(int request, int response, Intent data) {
        super.onActivityResult(request, response, data);
        if (request == 1 && response == RESULT_OK)
            finish();
    }

    private void initControl() {
        int color_yellow = getResources().getColor(R.color.color_yellow);

        textHeaderTitle = (TextView) findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(getResources().getString(R.string.txt_write_a_review));

        editReviewTitle = (EditText) findViewById(R.id.editReviewTitle);
        editReviewContent = (EditText) findViewById(R.id.editReviewContent);
        editReviewContent.setOnFocusChangeListener(this);

        spRelationWithSchool = (Spinner) findViewById(R.id.spRelationWithSchool);
        spYearGroupInReview = (Spinner) findViewById(R.id.spYearGroupInReview);

        String arrayRealtionWithSchool[] = getResources().getStringArray(R.array.array_relationship);
        MySpinnerAdapter arrayRelationAdapter = new MySpinnerAdapter(getApplicationContext(), R.layout.search_spinner_item, arrayRealtionWithSchool);
        spRelationWithSchool.setAdapter(arrayRelationAdapter);
        spRelationWithSchool.setSelection(arrayRealtionWithSchool.length - 1);

        String arrayYearOfGroup[] = getResources().getStringArray(R.array.array_year_group_review);
        MySpinnerAdapter arrayYearOfGroupAdapter = new MySpinnerAdapter(getApplicationContext(), R.layout.search_spinner_item, arrayYearOfGroup);
        spYearGroupInReview.setAdapter(arrayYearOfGroupAdapter);
        spYearGroupInReview.setSelection(arrayYearOfGroup.length - 1);

        rbLeadership = (RatingBar) findViewById(R.id.rbLeadership);
        LayerDrawable leaderStars = (LayerDrawable) rbLeadership.getProgressDrawable();
        leaderStars.getDrawable(2).setColorFilter(color_yellow, Mode.SRC_ATOP);

        rbQualityOfTeaching = (RatingBar) findViewById(R.id.rbQualityOfTeaching);
        LayerDrawable qualityStars = (LayerDrawable) rbQualityOfTeaching.getProgressDrawable();
        qualityStars.getDrawable(2).setColorFilter(color_yellow, Mode.SRC_ATOP);

        rbApproachOfStaff = (RatingBar) findViewById(R.id.rbApproachOfStaff);
        LayerDrawable approachStars = (LayerDrawable) rbApproachOfStaff.getProgressDrawable();
        approachStars.getDrawable(2).setColorFilter(color_yellow, Mode.SRC_ATOP);

        rbSafety = (RatingBar) findViewById(R.id.rbSafety);
        LayerDrawable safetyStars = (LayerDrawable) rbSafety.getProgressDrawable();
        safetyStars.getDrawable(2).setColorFilter(color_yellow, Mode.SRC_ATOP);

        rbSport = (RatingBar) findViewById(R.id.rbSport);
        LayerDrawable sportStars = (LayerDrawable) rbSport.getProgressDrawable();
        sportStars.getDrawable(2).setColorFilter(color_yellow, Mode.SRC_ATOP);

        rbMusic = (RatingBar) findViewById(R.id.rbMusic);
        LayerDrawable musicStars = (LayerDrawable) rbMusic.getProgressDrawable();
        musicStars.getDrawable(2).setColorFilter(color_yellow, Mode.SRC_ATOP);

        rbOverAll = (RatingBar) findViewById(R.id.rbOverAll);
        LayerDrawable overAllStars = (LayerDrawable) rbOverAll.getProgressDrawable();
        overAllStars.getDrawable(2).setColorFilter(color_yellow, Mode.SRC_ATOP);

        rbFood = (RatingBar) findViewById(R.id.rbFood);
        LayerDrawable foodStars = (LayerDrawable) rbFood.getProgressDrawable();
        foodStars.getDrawable(2).setColorFilter(color_yellow, Mode.SRC_ATOP);

        btnAddReview = (Button) findViewById(R.id.btnAddReview);
        btnAddReview.setOnClickListener(this);

        imgAnonymousOn = (ImageView) findViewById(R.id.imgAnonymousOn);
        imgAnonymousOff = (ImageView) findViewById(R.id.imgAnonymousOff);

        imgAnonymousOn.setOnClickListener(this);
        imgAnonymousOff.setOnClickListener(this);

        FontHelper.applyFont(this, findViewById(R.id.llRatingLayout), "opensansbold.ttf");
        FontHelper.applyFont(this, findViewById(R.id.llRdetails), "raleway_medium.otf");
    }

    private void redirectToLogin() {
        Bundle bundle = new Bundle();
        bundle.putString("header", textHeaderTitle.getText().toString());
        bundle.putString("tab", "school");
        bundle.putString("context", "activity");
        LoginFragment fLogin = new LoginFragment();
        FragmentTransaction mTransaction = getSupportFragmentManager().beginTransaction();
        mTransaction.replace(R.id.flWriteReview, fLogin);
        fLogin.setArguments(bundle);
        mTransaction.commit();
    }

    private class MySpinnerAdapter extends ArrayAdapter<String> {
        public MySpinnerAdapter(Context mContext, int resource, String[] arrayEstablishType) {
            super(mContext, resource, arrayEstablishType);
        }

        @Override
        public int getCount() {
            return super.getCount() - 1;
        }

        @Override
        public String getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }
    }

    @Override
    public void onClick(View mView) {
        switch (mView.getId()) {
            case R.id.imgAnonymousOn:
                setAnonymousOff();
                break;
            case R.id.imgAnonymousOff:
                setAnonymousOn();
                break;

            case R.id.btnAddReview:
                if (checkValidation() && myUtility.validateText(rbOverAll, getResources().getString(R.string.msg_rate_overall))) {
                    if (myUtility.checkInternetConnection()) {
                        WriteReviewAsyncCall();
                    }
                }
                break;
            default:
                break;
        }
    }

    private void setAnonymousOff() {
        imgAnonymousOn.setVisibility(View.GONE);
        imgAnonymousOff.setVisibility(View.VISIBLE);
        setDisplayName = "yes";
    }

    private void setAnonymousOn() {
        imgAnonymousOn.setVisibility(View.VISIBLE);
        imgAnonymousOff.setVisibility(View.GONE);
        setDisplayName = "no";
    }

    private boolean checkValidation() {
        if (myUtility.validateText(editReviewTitle, getResources().getString(R.string.msg_please_enter_title))) {
            if (myUtility.validateText(editReviewContent, getResources().getString(R.string.msg_please_enter_review))) {
                if (myUtility.validateText(spRelationWithSchool, getResources().getString(R.string.msg_select_relation))) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onFocusChange(View arg0, boolean arg1) {
        myUtility.hideSoftKeyboard();
    }

    private void WriteReviewAsyncCall() {
        stringHashMap = new HashMap<>();

        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("uidschool", schoolUid);
        stringHashMap.put("title", editReviewTitle.getText().toString().trim());
        stringHashMap.put("comments", editReviewContent.getText().toString().trim());
        stringHashMap.put("overall", "" + rbOverAll.getRating());
        stringHashMap.put("loh", "" + rbLeadership.getRating());
        stringHashMap.put("qot", "" + rbQualityOfTeaching.getRating());
        stringHashMap.put("aos", "" + rbApproachOfStaff.getRating());
        stringHashMap.put("sas", "" + rbSafety.getRating());
        stringHashMap.put("sports", "" + rbSport.getRating());
        stringHashMap.put("music", "" + rbMusic.getRating());
        stringHashMap.put("food", "" + rbFood.getRating());

        String yearGroup = "" + spYearGroupInReview.getSelectedItem();

        if (spYearGroupInReview.getSelectedItemPosition() == 13) {
            yearGroup = "";
        }
        stringHashMap.put("reviewyear", yearGroup);
        stringHashMap.put("displayname", setDisplayName);
        stringHashMap.put("relationship", "" + spRelationWithSchool.getSelectedItem().toString());
        stringHashMap.put("key", preferenceLogin.getString("key", ""));

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjResponse = jsonArray.getJSONObject(0);
                    if (jObjResponse.getString("code").equals("20")) {
                        setResult(20);
                        finish();
                    } else
                        redirectToLogin();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_ADD_REVIEW);
    }

    public void onFbClick() {
        if (myUtility.checkInternetConnection()) {
            Intent inFb = new Intent(WriteReviewFragmentActivity.this, FacebookActivity.class);
            startActivityForResult(inFb, 1);
        }
    }

    public void onLinkedInClick() {
        if (myUtility.checkInternetConnection()) {
            Intent myIntent = new Intent(WriteReviewFragmentActivity.this, LinkedinDialog.class);
            startActivityForResult(myIntent, 1);
        }
    }

    public void onGplusClick() {
        if (myUtility.checkInternetConnection()) {
            Intent iGplus = new Intent(WriteReviewFragmentActivity.this, GplusActivity.class);
            startActivityForResult(iGplus, 1);
        }
    }
}