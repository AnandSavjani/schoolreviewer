package com.schoolreviewer.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.schoolreviewer.R;
import com.schoolreviewer.bean.ReviewBean;
import com.schoolreviewer.util.Constant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class UserReviewFragment extends Fragment implements OnClickListener {

    private TextView textReviewTitle, textReviewerName, textRelationship, textReview, textLeaderShipRating, textQualityRating, textApproachRating,
            textSafetyRating, textSportRating, textMusicRating, textReviewdate, textEstablishType, textSchoolReply, textHeaderTitle, textReviewYear,
            textFoodQuality;
    private RatingBar rbOverAllRate;
    private ImageView imgAbuseReport;
    private String maxRating;
    private LinearLayout llUserInfo, llMeDateType, llSchoolReply;
    private ReviewBean rBean;
    private String reviewID, schoolID;
    private SharedPreferences preferenceLogin = null;
    private String uidReviewer, spUidReviewer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return LayoutInflater.from(getActivity()).inflate(R.layout.fragment_review, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initControl(view);

        view.setFocusableInTouchMode(true);
        view.setClickable(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    imgAbuseReport.setVisibility(View.GONE);

                }
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initControl(View view) {
        Bundle bundle = this.getArguments();
        rBean = (ReviewBean) bundle.getSerializable("review");
        schoolID = bundle.getString("schooluid");
        preferenceLogin = getActivity().getSharedPreferences(Constant.LOGIN_PREFERENCE, Context.MODE_PRIVATE);

        llUserInfo = (LinearLayout) view.findViewById(R.id.llUserInfo);
        llMeDateType = (LinearLayout) view.findViewById(R.id.llMeDateType);
        llSchoolReply = (LinearLayout) view.findViewById(R.id.llSchoolReply);

        textReviewTitle = (TextView) view.findViewById(R.id.textReviewTitle);
        textReviewerName = (TextView) view.findViewById(R.id.textReviewerName);
        textRelationship = (TextView) view.findViewById(R.id.textRelationship);
        textReview = (TextView) view.findViewById(R.id.textReview);
        textReviewYear = (TextView) view.findViewById(R.id.textReviewYear);
        textLeaderShipRating = (TextView) view.findViewById(R.id.textLeaderShipRating);
        textQualityRating = (TextView) view.findViewById(R.id.textQualityRating);
        textApproachRating = (TextView) view.findViewById(R.id.textApproachRating);
        textSafetyRating = (TextView) view.findViewById(R.id.textSafetyRating);
        textFoodQuality = (TextView) view.findViewById(R.id.textFoodQuality);
        textSportRating = (TextView) view.findViewById(R.id.textSportRating);
        textMusicRating = (TextView) view.findViewById(R.id.textMusicRating);
        textReviewdate = (TextView) view.findViewById(R.id.textReviewDate);
        textEstablishType = (TextView) view.findViewById(R.id.textEstablishType);
        textSchoolReply = (TextView) view.findViewById(R.id.textSchoolReply);
        rbOverAllRate = (RatingBar) view.findViewById(R.id.rbOverAllRate);

        LayerDrawable stars = (LayerDrawable) rbOverAllRate.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.color_yellow), Mode.SRC_ATOP);

        textHeaderTitle = (TextView) getActivity().findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(getResources().getString(R.string.txt_review));

        imgAbuseReport = (ImageView) getActivity().findViewById(R.id.imgAbuseReport);
        imgAbuseReport.setOnClickListener(this);

        maxRating = getResources().getString(R.string.txt_max_rate);
        if (rBean.getFromFragment().equals("me")) {
            llUserInfo.setVisibility(View.GONE);
            llMeDateType.setVisibility(View.VISIBLE);
            llSchoolReply.setVisibility(View.GONE);
            imgAbuseReport.setVisibility(View.GONE);
        } else {
            llUserInfo.setVisibility(View.VISIBLE);
            llMeDateType.setVisibility(View.GONE);
            imgAbuseReport.setVisibility(View.VISIBLE);
            if (preferenceLogin.contains("uiduser")) {
                uidReviewer = rBean.getUidreviewer();
                spUidReviewer = preferenceLogin.getString("uiduser", "");
                if (uidReviewer.equals(spUidReviewer))
                    imgAbuseReport.setVisibility(View.GONE);
            }
        }

        reviewID = rBean.getUid();
        String reviewTitle = Html.fromHtml(rBean.getTitle()).toString();
        textReviewTitle.setText(reviewTitle);
        textReviewerName.setText(rBean.getDisplayname());
        textEstablishType.setText(rBean.getDisplayname());
        String reviewDescription = Html.fromHtml(rBean.getFurther()).toString();
        textReview.setText(reviewDescription);
        textRelationship.setText(getActivity().getResources().getString(R.string.txt_relationship) + " " + rBean.getRelationship());
        if (!rBean.getReviewyear().equals("")) {
            String sReviewYear = rBean.getReviewyear();
            sReviewYear = sReviewYear.replace("Year ", "");
            textReviewYear.setText(getActivity().getResources().getString(R.string.txt_year) + " " + sReviewYear);
            textReviewYear.setVisibility(View.VISIBLE);
        } else
            textReviewYear.setVisibility(View.GONE);

        textLeaderShipRating.setText(rBean.getLoH() + maxRating);
        textQualityRating.setText(rBean.getQoT() + maxRating);
        textApproachRating.setText(rBean.getAoS() + maxRating);
        textSafetyRating.setText(rBean.getSaS() + maxRating);
        textSportRating.setText(rBean.getSports() + maxRating);
        textMusicRating.setText(rBean.getMusic() + maxRating);
        textFoodQuality.setText(rBean.getFood() + maxRating);

        rbOverAllRate.setRating(rBean.getScoreOfReview());
        if (!rBean.getReply().equals("")) {
            textSchoolReply.setText(rBean.getReply());
            llSchoolReply.setVisibility(View.VISIBLE);
        }
        parseIntoDate();
    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.imgAbuseReport:
                redirectToAbuseReport();
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            Toast.makeText(getActivity(), "Your complaint has been successfully received.", Toast.LENGTH_LONG).show();
        }
    }

    private void redirectToAbuseReport() {
        Intent inAbuse = new Intent(getActivity(), AbuseReportActivity.class);
        inAbuse.putExtra("reviewuid", reviewID);
        inAbuse.putExtra("schooluid", schoolID);
        inAbuse.putExtra("isReview", true);
        inAbuse.putExtra("header", getActivity().getResources().getString(R.string.txt_review));
        startActivityForResult(inAbuse, 0);
    }


    private void parseIntoDate() {
        String date = rBean.getReviewedat().trim();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
        Date d = null;
        try {
            d = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat serverFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        textReviewdate.setText("Date:" + serverFormat.format(d));
    }

}