package com.schoolreviewer.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.schoolreviewer.R;
import com.schoolreviewer.me.LoginActivity;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.MyUtility;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AskQuestionActivity extends FragmentActivity implements View.OnClickListener {

    private EditText editQuestionTitle, editQuestionContent;
    private Spinner spRelationQue, spYearGroupQue;
    private Button btnPostIt;
    private TextView textHeaderTitle, textAnonymous;
    private MyUtility myUtility;
    private CheckBox cbQuestionAnon;
    private String schoolID, questionUID;
    private SharedPreferences userPreferences;
    private boolean isQuestion;
    private RelativeLayout rlHeader;
    private HashMap<String, String> stringHashMap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_question);

        schoolID = getIntent().getStringExtra("uid");
        isQuestion = getIntent().getBooleanExtra("question", false);
        if (!isQuestion) {
            questionUID = getIntent().getStringExtra("uidquestion");
        }
        initControl(isQuestion);
        myUtility = new MyUtility(this);
        userPreferences = getSharedPreferences(Constant.LOGIN_PREFERENCE, Context.MODE_PRIVATE);
        if (!userPreferences.contains("key")) {
            redirectToLogin();
        }
    }

    private void initControl(Boolean isQuestion) {
        editQuestionTitle = (EditText) findViewById(R.id.editQuestionTitle);
        editQuestionContent = (EditText) findViewById(R.id.editQuestionContent);
        spRelationQue = (Spinner) findViewById(R.id.spRelationQue);
        spYearGroupQue = (Spinner) findViewById(R.id.spYearGroupQue);

        textAnonymous = (TextView) findViewById(R.id.textAnonymous);
        rlHeader = (RelativeLayout) findViewById(R.id.rlHeader);
        rlHeader.setBackgroundColor(getResources().getColor(R.color.color_li_orange));

        String arrayRealtion[] = getResources().getStringArray(R.array.array_relationship);
        MySpinnerAdapter arrayRelationAdapter = new MySpinnerAdapter(this, R.layout.search_spinner_item, arrayRealtion);
        spRelationQue.setAdapter(arrayRelationAdapter);
        spRelationQue.setSelection(arrayRealtion.length - 1);

        String arrayYearGroup[] = getResources().getStringArray(R.array.array_year_group_review);
        MySpinnerAdapter arrayYearOfGroupAdapter = new MySpinnerAdapter(this, R.layout.search_spinner_item, arrayYearGroup);
        spYearGroupQue.setAdapter(arrayYearOfGroupAdapter);
        spYearGroupQue.setSelection(arrayYearGroup.length - 1);

        btnPostIt = (Button) findViewById(R.id.btnPostIt);
        cbQuestionAnon = (CheckBox) findViewById(R.id.cbQuestionAnon);

        textHeaderTitle = (TextView) findViewById(R.id.textHeaderTitle);
        if (!isQuestion) {
            textHeaderTitle.setText(getResources().getString(R.string.btn_answer_question));
            editQuestionTitle.setVisibility(View.GONE);
            editQuestionContent.setHint(getResources().getString(R.string.hint_your_answer));
            btnPostIt.setText(getResources().getString(R.string.btn_add_your_answer));
            textAnonymous.setText(getResources().getString(R.string.lbl_answer_anon));
        } else {
            textHeaderTitle.setText(getResources().getString(R.string.btn_ask_question));
            textAnonymous.setText(getResources().getString(R.string.lbl_question_anon));
        }

        btnPostIt.setOnClickListener(this);
    }

    private void redirectToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("header", "Question");
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnPostIt) {
            if (myUtility.checkInternetConnection() && isQuestion) {
                if (checkValidationForQuestion("Provide Question Title", "Provide Your Question", getResources().getString(R.string.msg_select_relation))) {
                    PostAsyncCall("question", Constant.URL_ADD_QUESTION);
                }
            } else if (myUtility.checkInternetConnection() && !isQuestion) {
                if (checkValidationForAnswer("Provide Your Answer", getResources().getString(R.string.msg_select_relation))) {
                    PostAsyncCall("answer", Constant.URL_ADD_ANSWER);
                }
            }
        }
    }

    private boolean checkValidationForQuestion(String title, String subject, String relation) {
        if (myUtility.validateText(editQuestionTitle, title)) {
            if (myUtility.validateText(editQuestionContent, subject))
                if (myUtility.validateText(spRelationQue, relation)) {
                    return true;
                }
        }
        return false;
    }

    private boolean checkValidationForAnswer(String content, String relation) {
        if (myUtility.validateText(editQuestionContent, content)) {
            if (myUtility.validateText(spRelationQue, relation)) {
                return true;
            }
        }
        return false;
    }

    private class MySpinnerAdapter extends ArrayAdapter<String> {
        public MySpinnerAdapter(Context mContext, int resource, String[] arrayEstablishType) {
            super(mContext, resource, arrayEstablishType);
        }

        @Override
        public int getCount() {
            return super.getCount() - 1;
        }

        @Override
        public String getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }
    }

    private void PostAsyncCall(String type, String url) {
        stringHashMap = new HashMap<>();

        stringHashMap.put("key", userPreferences.getString("key", null));
        stringHashMap.put("uid", schoolID);
        stringHashMap.put("country", Constant.COUNTRY);
        if (cbQuestionAnon.isChecked()) {
            stringHashMap.put("displayname", "no");
        } else {
            stringHashMap.put("displayname", "yes");
        }
        stringHashMap.put("relationship", spRelationQue.getSelectedItem().toString());

        if (type.equals("question")) {
            stringHashMap.put("question", editQuestionContent.getText().toString().trim());
            stringHashMap.put("title", editQuestionTitle.getText().toString());
            stringHashMap.put("questionyear", spYearGroupQue.getSelectedItem().toString());
        } else {
            stringHashMap.put("uidquestion", questionUID);
            stringHashMap.put("answer", editQuestionContent.getText().toString());
            stringHashMap.put("answeryear", spYearGroupQue.getSelectedItem().toString());
        }

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                finish();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
    }
}
