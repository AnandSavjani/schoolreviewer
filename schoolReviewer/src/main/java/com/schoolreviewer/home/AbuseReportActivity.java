package com.schoolreviewer.home;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.schoolreviewer.R;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.MyUtility;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AbuseReportActivity extends Activity implements OnClickListener {
    private TextView textHeaderTitle;
    private EditText editReviewerName, editReviewerEmail, editUserNumber, editComment;
    private Button btnCancel, btnSend;
    private MyUtility myUtility;
    private HashMap<String, String> stringHashMap;
    private Bundle mBundle;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_report_abuse);
        initControl();
    }

    private void initControl() {
        myUtility = new MyUtility(this);
        mBundle = getIntent().getExtras();

        textHeaderTitle = (TextView) findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(mBundle.getString("header"));

        editReviewerName = (EditText) findViewById(R.id.editReviewerName);
        editReviewerEmail = (EditText) findViewById(R.id.editReviewerEmail);
        editUserNumber = (EditText) findViewById(R.id.editUserNumber);
        editComment = (EditText) findViewById(R.id.editComment);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnSend = (Button) findViewById(R.id.btnSend);

        btnCancel.setOnClickListener(this);
        btnSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View clickView) {
        switch (clickView.getId()) {
            case R.id.btnCancel:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.btnSend:
                if (checkValidation()) {
                    if (myUtility.checkInternetConnection()) {
                        AbuseAsyncCall();
                    }
                }
                break;
            default:
                break;
        }
    }

    boolean checkValidation() {
        if (myUtility.validName(editReviewerName, getResources().getString(R.string.msg_provide_name))) {
            if (myUtility.validateEmail(editReviewerEmail, getResources().getString(R.string.msg_provide_email))) {
                if (myUtility.validateText(editComment, getResources().getString(R.string.msg_provide_comment))) {
                    return true;
                }
            }
        }
        return false;
    }

    private void AbuseAsyncCall() {
        stringHashMap = new HashMap<>();

        stringHashMap.put("name", editReviewerName.getText().toString().trim());
        stringHashMap.put("email", editReviewerEmail.getText().toString().trim());
        stringHashMap.put("phone", editUserNumber.getText().toString().trim());
        stringHashMap.put("notes", editComment.getText().toString().trim());
        stringHashMap.put("uidschool", mBundle.getString("schooluid"));
        stringHashMap.put("country", Constant.COUNTRY);

        if (mBundle.getBoolean("isQuestion")) {
            stringHashMap.put("uidquestion", mBundle.getString("uidquestion"));
        } else if (mBundle.getBoolean("isReview")) {
            stringHashMap.put("uidreview", mBundle.getString("reviewuid"));
        } else {
            stringHashMap.put("uidanswer", mBundle.getString("reviewuid"));
        }

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonAbuse = jsonArray.getJSONObject(0);
                    String code = jsonAbuse.getString("code");
                    if (code.equals("22"))
                        setResult(RESULT_OK);
                    else
                        setResult(RESULT_CANCELED);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                finish();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_ADD_COMPLAINT);
    }
}