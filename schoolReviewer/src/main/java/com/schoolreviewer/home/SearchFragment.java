package com.schoolreviewer.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;
import com.schoolreviewer.adapter.AutoCompleteAdapter;
import com.schoolreviewer.adapter.CustomAutoCompleteView;
import com.schoolreviewer.bean.AutoCompleteBean;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.FontHelper;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchFragment extends BaseFragment implements OnClickListener, OnSeekBarChangeListener, OnTouchListener,
        OnKeyListener, TextWatcher {

    private View mView;
    private Button btnAdvanceOption, btnSearch, btnReset;
    private Spinner spEstablishType, spType, spDayBording, spGender;
    private EditText editLocation, editKeyword;
    private CustomAutoCompleteView editCityName;
    private TextView textMeter, textHeaderTitle;
    private SeekBar sbDistance;
    private ImageView imgRadio, imgRadioAct, imgScIcon;
    private LinearLayout llAdvanceOption;
    private String strGetDistance = "50", specialNeeds = "no", convertType;
    private Bundle bundle;
    private boolean isAdvanceSearch = false;
    private FrameLayout flSearchFragment;
    private AutoCompleteAdapter mAdapter;
    private List<AutoCompleteBean> listSchool;
    private List<AutoCompleteBean> allList;
    private HashMap<String, String> stringHashMap;
    private AutoCompleteBean autoBean;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_search, null);
        return mView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            mView.destroyDrawingCache();
            mView.clearFocus();
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Constant.flag = 0;
        initControl(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initControl(View v) {
        bundle = new Bundle();
        allList = new ArrayList<>();

        mView.setFocusableInTouchMode(true);
        llAdvanceOption = (LinearLayout) v.findViewById(R.id.llAdvanceOption);
        flSearchFragment = (FrameLayout) v.findViewById(R.id.flSearchFragment);

        btnAdvanceOption = (Button) v.findViewById(R.id.btnAdvanceOption);
        btnSearch = (Button) v.findViewById(R.id.btnSearch);
        btnReset = (Button) v.findViewById(R.id.btnReset);

        btnAdvanceOption.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnReset.setOnClickListener(this);

        spEstablishType = (Spinner) v.findViewById(R.id.spEstablishType);
        spType = (Spinner) v.findViewById(R.id.spType);
        spDayBording = (Spinner) v.findViewById(R.id.spDayBording);
        spGender = (Spinner) v.findViewById(R.id.spGender);

        editCityName = (CustomAutoCompleteView) v.findViewById(R.id.editCityName);
        editLocation = (EditText) v.findViewById(R.id.editLocation);
        editKeyword = (EditText) v.findViewById(R.id.editKeyword);
        editCityName.setText("");
        editLocation.setText("");
        editKeyword.setText("");

        editCityName.setThreshold(1);
        editCityName.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editCityName.setText(allList.get(position).getEstaName());
                utility.hideSoftKeyboard();
                if (utility.checkInternetConnection()) {
                    if (!isAdvanceSearch) {
                        redirectToSchoolProfile(allList.get(position).getEstaName());
                    }
                }
            }
        });

        editCityName.addTextChangedListener(this);
        editCityName.setOnTouchListener(this);
        editLocation.setOnTouchListener(this);
        editKeyword.setOnTouchListener(this);

        editCityName.setOnKeyListener(this);
        editLocation.setOnKeyListener(this);
        editKeyword.setOnKeyListener(this);

        textMeter = (TextView) v.findViewById(R.id.textMeter);
        textHeaderTitle = (TextView) getActivity().findViewById(R.id.textHeaderTitle);

        sbDistance = (SeekBar) v.findViewById(R.id.sbDistance);
        sbDistance.setProgress(0);
        sbDistance.setOnSeekBarChangeListener(this);

        imgRadio = (ImageView) v.findViewById(R.id.imgRadio);
        imgRadioAct = (ImageView) v.findViewById(R.id.imgRadioAct);
        imgScIcon = (ImageView) v.findViewById(R.id.imgScIcon);

        imgRadio.setOnClickListener(this);
        imgRadioAct.setOnClickListener(this);

        String arrayEstablishType[] = getResources().getStringArray(R.array.array_Establishment_Type);
        MySpinnerAdapter arrayEstablishAdapter = new MySpinnerAdapter(getActivity(), R.layout.search_spinner_item, arrayEstablishType);
        spEstablishType.setAdapter(arrayEstablishAdapter);

        spEstablishType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                SetYearGroupData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        String arrayType[] = getResources().getStringArray(R.array.array_type);
        MySpinnerAdapter arrayTypeAdapter = new MySpinnerAdapter(v.getContext(), R.layout.search_spinner_item, arrayType);
        spType.setAdapter(arrayTypeAdapter);

        String arrayDayBoarding[] = getResources().getStringArray(R.array.array_day_boarding);
        MySpinnerAdapter arrayDayBoardingAdapter = new MySpinnerAdapter(v.getContext(), R.layout.search_spinner_item, arrayDayBoarding);
        spDayBording.setAdapter(arrayDayBoardingAdapter);

        String arrayGender[] = getResources().getStringArray(R.array.array_gender);
        MySpinnerAdapter arrayGenderAdapter = new MySpinnerAdapter(v.getContext(), R.layout.search_spinner_item, arrayGender);
        spGender.setAdapter(arrayGenderAdapter);
        SetYearGroupData();

        sbDistance.setProgressDrawable(getResources().getDrawable(R.drawable.progress_distance));

        controlVisiblilty();
        mView.setFocusableInTouchMode(true);
        mView.requestFocus(R.layout.fragment_search);
        FragmentManager fManager = getFragmentManager();
        fManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        mView.setOnKeyListener(this);

        spEstablishType.setOnTouchListener(this);
        spType.setOnTouchListener(this);
        spDayBording.setOnTouchListener(this);
        spGender.setOnTouchListener(this);

        FontHelper.applyFont(getActivity(), btnAdvanceOption, "opensansbold.ttf");
        FontHelper.applyFont(getActivity(), btnSearch, "opensansbold.ttf");
        FontHelper.applyFont(getActivity(), btnReset, "opensansbold.ttf");

        if (utility.checkInternetConnection())
            AutoCompleteAsyncCall("", convertType.trim());
    }

    class MySpinnerAdapter extends ArrayAdapter<String> {
        public MySpinnerAdapter(Context searchFragment, int resource, String[] arrayEstablishType) {
            super(searchFragment, resource, arrayEstablishType);
        }

        @Override
        public int getCount() {
            return super.getCount();
        }

        @Override
        public String getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }
    }

    private void SetYearGroupData() {
        convertType = spEstablishType.getSelectedItem().toString().toLowerCase();
        if (spEstablishType.getSelectedItemPosition() == 1)
            convertType = "preschool";
        if (spEstablishType.getSelectedItemPosition() == 4)
            convertType = "further";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdvanceOption:
                isAdvanceSearch = true;
                showAdvanceOption();
                flSearchFragment.setBackgroundColor(getResources().getColor(android.R.color.white));
                break;

            case R.id.btnSearch:
                if (utility.checkInternetConnection()) {
                    if (checkValidation()) {
                        passDataUsingBundle();
                        utility.hideSoftKeyboard();
                    }
                }
                break;
            case R.id.btnReset:
                initControl(mView);
                btnAdvanceOption.setVisibility(View.VISIBLE);
                imgRadioAct.setVisibility(View.GONE);
                imgRadio.setVisibility(View.VISIBLE);
                specialNeeds = "no";
                break;
            case R.id.imgRadio:
                radioSpecialCheck();
                break;
            case R.id.imgRadioAct:
                radioSpecialCheck();
                break;
            default:
                break;
        }
    }

    private void passDataUsingBundle() {
        strGetDistance = String.valueOf(sbDistance.getProgress());

        bundle.putString("establishment", convertType.trim());
        bundle.putString("placename", editCityName.getText().toString().trim());
        bundle.putString("place", editLocation.getText().toString().trim());
        bundle.putString("distance", strGetDistance);
        bundle.putString("type", spType.getSelectedItem().toString().trim());
        bundle.putString("dayboarding", spDayBording.getSelectedItem().toString().trim());
        if (spGender.getSelectedItemPosition() == 3) {
            bundle.putString("gender", "coed");
        } else
            bundle.putString("gender", spGender.getSelectedItem().toString().trim());

        bundle.putString("keyword", editKeyword.getText().toString().trim());
        bundle.putString("speacialneeds", specialNeeds);

        SearchResultFragment fSearchResult = new SearchResultFragment();
        FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.flSearchFragment, fSearchResult, "fragment");
        mFragmentTransaction.addToBackStack("fragment");
        fSearchResult.setArguments(bundle);
        mFragmentTransaction.commit();
    }


    private void radioSpecialCheck() {
        if (specialNeeds.equals("no")) {
            imgRadioAct.setVisibility(View.VISIBLE);
            imgRadio.setVisibility(View.GONE);
            specialNeeds = "yes";
        } else {
            imgRadioAct.setVisibility(View.GONE);
            imgRadio.setVisibility(View.VISIBLE);
            specialNeeds = "no";
        }
    }

    private void showAdvanceOption() {
        llAdvanceOption.setVisibility(View.VISIBLE);
        textHeaderTitle.setText(getResources().getString(R.string.txt_advance_search));
        btnAdvanceOption.setVisibility(View.GONE);
        imgScIcon.setVisibility(View.GONE);
    }

    private void controlVisiblilty() {
        llAdvanceOption.setVisibility(View.GONE);
        textHeaderTitle.setText(getResources().getString(R.string.txt_title_home));
        isAdvanceSearch = false;
        flSearchFragment.setBackgroundResource(R.drawable.fade_bg);
        imgScIcon.setVisibility(View.VISIBLE);
    }

    @Override
    public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
        textMeter.setText("" + progress + " " + getResources().getString(R.string.txt_miles));
        if (progress == 0) {
            textMeter.setText("" + progress + " " + "mile");
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar arg0) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar arg0) {
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View mView, MotionEvent arg1) {
        switch (mView.getId()) {
            case R.id.editCityName:
                editCityName.setFocusableInTouchMode(true);
                editCityName.requestFocusFromTouch();
                break;
            case R.id.editLocation:
                editLocation.setFocusable(true);
                editLocation.requestFocusFromTouch();
                break;
            case R.id.editKeyword:
                editKeyword.setFocusable(true);
                editKeyword.requestFocusFromTouch();
                break;
            default:
                utility.hideSoftKeyboard();
                break;
        }
        return false;
    }

    public boolean checkValidation() {
        if (isAdvanceSearch && (TextUtils.isEmpty(editCityName.getText()) && TextUtils.isEmpty(editLocation.getText()))) {
            utility.showMessageDialog(getActivity().getResources().getString(R.string.txt_city_message));
            return false;
        } else if (!isAdvanceSearch && TextUtils.isEmpty(editCityName.getText())) {
            utility.showMessageDialog(getActivity().getResources().getString(R.string.txt_place_message));
            return false;
        }
        if ((!TextUtils.isEmpty(editLocation.getText())) && sbDistance.getProgress() == 0) {
            utility.showMessageDialog(getActivity().getResources().getString(R.string.txt_distance_message));
            return false;
        } else
            return true;

    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && isAdvanceSearch && getFragmentManager().getBackStackEntryCount() == 0) {
            btnAdvanceOption.setVisibility(View.VISIBLE);
            controlVisiblilty();
            isAdvanceSearch = false;
            return true;
        } else
            return false;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (utility.checkInternetConnection())
            AutoCompleteAsyncCall(s.toString().toLowerCase(), convertType.trim());
    }

    private void AutoCompleteAsyncCall(String term, String type) {
        stringHashMap = new HashMap<>();
        listSchool = new ArrayList<>();
        autoBean = new AutoCompleteBean();

        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("term", term);
        stringHashMap.put("phase", type);

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjResponse = jsonArray.getJSONObject(0);
                    JSONObject jObjMessage = jObjResponse.getJSONObject("message");

                    JSONArray jArraySchool = jObjMessage.getJSONArray("schools");
                    if (jArraySchool.length() != 0) {
                        for (int i = 0; i < jArraySchool.length(); i++) {
                            autoBean = new AutoCompleteBean();
                            JSONObject jObjValue = jArraySchool.getJSONObject(i);
                            autoBean.setEstaName(jObjValue.getString("value"));
                            autoBean.setEstaType("school");
                            listSchool.add(autoBean);
                        }
                    }
                    JSONArray jArrayPlace = jObjMessage.getJSONArray("places");
                    if (jArrayPlace.length() != 0) {
                        for (int i = 0; i < jArrayPlace.length(); i++) {
                            autoBean = new AutoCompleteBean();
                            JSONObject jObjValue = jArrayPlace.getJSONObject(i);
                            autoBean.setEstaName(jObjValue.getString("value"));
                            autoBean.setEstaType("place");
                            listSchool.add(autoBean);
                        }
                    }

                    if (jObjResponse.getInt("code") == 20) {
                        allList.clear();
                        allList.addAll(listSchool);
                        mAdapter = new AutoCompleteAdapter(getActivity(), R.layout.row_autocomplete, listSchool);

                        mAdapter.notifyDataSetChanged();
                        editCityName.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_AUTO_COMPLETE);
    }

    private void redirectToSchoolProfile(String schoolName) {
        stringHashMap = new HashMap<>();

        stringHashMap.put("order", "strm_name asc");
        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("dayboarding", "any");
        stringHashMap.put("pageno", "1");
        stringHashMap.put("lang", "en");
        stringHashMap.put("specialneeds", "no");
        stringHashMap.put("gender", "any");
        stringHashMap.put("type", "any");
        stringHashMap.put("pagesize", "20");
        stringHashMap.put("placename", schoolName);
        stringHashMap.put("year", "any");
        stringHashMap.put("phase", "all");

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == 8) {
                        if (jsonObject.getJSONObject("message").getInt("norows") == 1) {
                            Bundle bundle = new Bundle();
                            bundle.putString("uid", jsonObject.getJSONObject("message").getJSONArray("results").getJSONObject(0).getString("uid"));
                            bundle.putString("establishment_type", spEstablishType.getSelectedItem().toString().toLowerCase());
                            SchoolProfileFragment fSchoolProfile = new SchoolProfileFragment();
                            FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
                            fSchoolProfile.setArguments(bundle);
                            mFragmentTransaction.replace(R.id.flSearchFragment, fSchoolProfile, "fragment");
                            mFragmentTransaction.addToBackStack("fragment");
                            mFragmentTransaction.commit();
                        } else {
                            btnSearch.performClick();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_SEARCH);
    }
}
