package com.schoolreviewer.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;
import com.schoolreviewer.adapter.SearchResultAdapter;
import com.schoolreviewer.bean.SearchBean;
import com.schoolreviewer.bean.SearchItemBean;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.FontHelper;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SearchResultFragment extends BaseFragment implements OnClickListener, DrawerListener, OnItemClickListener {

    private boolean isMenuOpen;
    private DrawerLayout mDrawerLayout;
    private RelativeLayout rlSliderLayout;
    private Button btnUpdateResult;
    private TextView textHeaderTitle, textFilterHeader;
    private LinearLayout llDrawerEstablishment;
    private ListView lvSchoolList;
    private ProgressDialog pdWaiting;
    private SearchResultAdapter mSearchResultAdapter;
    private Bundle bundleParam;
    private ArrayList<SearchItemBean> arrSchoolList;
    private String establishment, placename, place, type, gender, dayboarding, distance, speacialneeds;
    private String filter = "strm_name asc";
    private int limit = 20;
    private int page = 1, totalpages = 0;
    private CheckBox cbNlt, cbReviewRating, cbEstaType;
    private HashMap<String, String> stringHashMap;
    private ImageView imgFilter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_search_result, null);
        initControls(view);
        Constant.flag = 1;
        return view;
    }

    public void initControls(View view) {
        arrSchoolList = new ArrayList<>();
        isMenuOpen = false;
        utility.hideSoftKeyboard();

        mDrawerLayout = (DrawerLayout) view.findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        rlSliderLayout = (RelativeLayout) view.findViewById(R.id.right_drawer);
        llDrawerEstablishment = (LinearLayout) view.findViewById(R.id.llDrawerEstablishment);

        imgFilter = (ImageView) getActivity().findViewById(R.id.imgFilter);
        imgFilter.setVisibility(View.VISIBLE);
        imgFilter.setOnClickListener(this);

        btnUpdateResult = (Button) view.findViewById(R.id.btnUpdateResult);
        btnUpdateResult.setOnClickListener(this);

        textHeaderTitle = (TextView) getActivity().findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(getResources().getString(R.string.txt_search_result));
        textFilterHeader = (TextView) view.findViewById(R.id.textFilterHeader);
        textFilterHeader.setText(getActivity().getResources().getString(R.string.txt_filter_results_high));

        cbNlt = (CheckBox) view.findViewById(R.id.cbNlt);
        cbReviewRating = (CheckBox) view.findViewById(R.id.cbReviewRating);
        cbEstaType = (CheckBox) view.findViewById(R.id.cbEstaType);

        cbReviewRating.setOnClickListener(this);
        cbEstaType.setOnClickListener(this);
        cbNlt.setOnClickListener(this);

        lvSchoolList = (ListView) view.findViewById(R.id.lvSearchResult);
        lvSchoolList.setOnItemClickListener(this);

        mDrawerLayout.addDrawerListener(this);

        FontHelper.applyFont(getActivity(), view.findViewById(R.id.btnUpdateResult), "opensansbold.ttf");

        view.focusSearch(View.FOCUS_UP);
        getBundleValues();
        page = 1;
        requestForSearchResult(page);
        lvSchoolList.setFocusable(true);
        lvSchoolList.findFocus();

        lvSchoolList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int threshold = 1;
                int count = lvSchoolList.getCount();
                lvSchoolList.setSmoothScrollbarEnabled(true);
                if (scrollState == SCROLL_STATE_IDLE && totalpages != page && totalpages != 0) {
                    if (lvSchoolList.getLastVisiblePosition() >= count - threshold) {
                        page = page + 1;
                        String sPage = String.valueOf(page);
                        if (utility.checkInternetConnection()) {
                            SearchResultAsyncCall(sPage);
                        }
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });


    }

    private void requestForSearchResult(int page) {
        if (utility.checkInternetConnection()) {
            String sPage = String.valueOf(page);
            SearchResultAsyncCall(sPage);
        }
    }

    private void getBundleValues() {
        bundleParam = this.getArguments();
        establishment = bundleParam.getString("establishment").toLowerCase();
        placename = bundleParam.getString("placename").toLowerCase();
        place = bundleParam.getString("place").toLowerCase();
        type = bundleParam.getString("type").toLowerCase();
        gender = bundleParam.getString("gender").toLowerCase();
        gender = gender.replace(" only", "").trim().toLowerCase();
        dayboarding = bundleParam.getString("dayboarding").toLowerCase();
        distance = bundleParam.getString("distance").toLowerCase();
        if (!distance.equals("0")) {
            filter = "distance asc";
        }
        speacialneeds = bundleParam.getString("speacialneeds").toLowerCase();
        Log.i(establishment + placename + place, type + gender + dayboarding);
        Log.i(distance, speacialneeds);
        if (establishment.equalsIgnoreCase("all")) {
            llDrawerEstablishment.setVisibility(View.VISIBLE);
        } else {
            llDrawerEstablishment.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgFilter:
                if (isMenuOpen) {
                    closeMenu();
                } else {
                    openMenu();
                }
                break;

            case R.id.btnUpdateResult:
                closeMenu();
                arrSchoolList.clear();
                lvSchoolList.setSelectionFromTop(0, 0);
                if (cbNlt.isChecked()) {
                    filter = "int_ofsted_overall desc";
                    requestForSearchResult(page);
                } else if (cbReviewRating.isChecked()) {
                    filter = "float_score desc";
                    requestForSearchResult(page);
                } else if (cbEstaType.isChecked()) {
                    filter = "strm_name asc";
                    requestForSearchResult(page);
                } else {
                    filter = "strm_name asc";
                    requestForSearchResult(page);
                }
                break;
            case R.id.cbNlt:
                cbEstaType.setChecked(false);
                cbReviewRating.setChecked(false);
                break;
            case R.id.cbReviewRating:
                cbNlt.setChecked(false);
                cbEstaType.setChecked(false);
                break;
            case R.id.cbEstaType:
                cbReviewRating.setChecked(false);
                cbNlt.setChecked(false);
                break;
            default:
                break;
        }
    }

    private void closeMenu() {
        mDrawerLayout.closeDrawer(rlSliderLayout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        isMenuOpen = false;
    }

    private void openMenu() {
        mDrawerLayout.openDrawer(rlSliderLayout);
        isMenuOpen = true;
        lvSchoolList.setEnabled(false);
    }

    @Override
    public void onDrawerClosed(View arg0) {
        isMenuOpen = false;
        lvSchoolList.setEnabled(true);
    }

    @Override
    public void onDrawerOpened(View arg0) {
        isMenuOpen = true;
    }

    @Override
    public void onDrawerSlide(View arg0, float arg1) {
    }

    @Override
    public void onDrawerStateChanged(int arg0) {
    }


    private void SearchResultAsyncCall(String sPage) {
        showProgress(getActivity());
        lvSchoolList.setEnabled(false);
        lvSchoolList.setScrollContainer(false);
        stringHashMap = new HashMap<>();


        stringHashMap.put("pageno", sPage);
        stringHashMap.put("pagesize", "" + limit);
        stringHashMap.put("order", filter);
        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("phase", establishment);
        stringHashMap.put("type", type);
        stringHashMap.put("gender", gender);
        stringHashMap.put("specialneeds", speacialneeds);
        stringHashMap.put("dayboarding", dayboarding);
        stringHashMap.put("year", "any");

        if (!distance.equals("0") && !place.equals("")) {
            stringHashMap.put("place", place);
            stringHashMap.put("distance", distance);
            stringHashMap.put("placename", placename);
        }
        if (distance.equals("0") && place.equals("")) {
            stringHashMap.put("placename", placename);

        }
        if (!distance.equals("0") && place.equals("")) {
            stringHashMap.put("distance", distance);
            stringHashMap.put("place", placename);
        }
        stringHashMap.put("lang", "en");

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                SearchBean searchBean = new SearchBean();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getString("status").equalsIgnoreCase("OK")) {
                        JSONObject jobjSearchResult = jsonObject.getJSONObject("message");

                        searchBean.setPageno(jobjSearchResult.getString("pageno"));
                        searchBean.setActualstart(jobjSearchResult.getString("actualstart"));
                        searchBean.setNorows(jobjSearchResult.getString("norows"));
                        searchBean.setActualend(jobjSearchResult.getString("actualend"));
                        searchBean.setNopages(jobjSearchResult.getString("nopages"));
                        JSONArray jArrayResultItems = jobjSearchResult.getJSONArray("results");

                        for (int i = 0; i < jArrayResultItems.length(); i++) {
                            try {
                                JSONObject item = jArrayResultItems.getJSONObject(i);
                                SearchItemBean searchitem = new SearchItemBean();
                                searchitem.setUid(item.getString("uid"));
                                searchitem.setName(item.getString("name"));
                                searchitem.setDistance(item.getString("distance"));
                                searchitem.setScore(item.getString("score"));
                                searchitem.setLeaguepos(item.getString("leaguepos"));
                                searchitem.setType(item.getString("type"));
                                searchitem.setPostcode(item.getString("postcode"));
                                searchitem.setImageurl(item.getString("imageurl"));
                                searchitem.setNotes(item.getString("notes"));
                                arrSchoolList.add(searchitem);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        searchBean.setSearchitem(arrSchoolList);
                        String noPages = searchBean.getNopages();

                        if (utility.checkInternetConnection()) {
                            if (jArrayResultItems.length() <= 0) {
                                lvSchoolList.setVisibility(View.GONE);
                                utility.showMessageDialog(context.getResources().getString(R.string.msg_result_not_found));
                                getFragmentManager().popBackStack();
                            } else {
                                totalpages = Integer.valueOf(noPages);
                                int position = lvSchoolList.getLastVisiblePosition();
                                mSearchResultAdapter = new SearchResultAdapter(context, arrSchoolList);
                                mSearchResultAdapter.notifyDataSetChanged();
                                lvSchoolList.setAdapter(mSearchResultAdapter);
                                lvSchoolList.setSelectionFromTop(position, 0);
                            }
                        } else {
                            lvSchoolList.setVisibility(View.GONE);
                        }
                        lvSchoolList.setEnabled(true);
                        lvSchoolList.setScrollContainer(true);
                        hideProgress();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_SEARCH);
    }

    private void showProgress(Context mContext) {
        pdWaiting = new ProgressDialog(mContext);
        pdWaiting.setMessage("Please wait...");
        pdWaiting.setIndeterminate(false);
        pdWaiting.setCancelable(true);
        pdWaiting.setCanceledOnTouchOutside(false);
        pdWaiting.show();
    }

    private void hideProgress() {
        if (pdWaiting.isShowing())
            pdWaiting.dismiss();
    }


    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        if (arg0.getId() == R.id.lvSearchResult) {
            if (utility.checkInternetConnection()) {
                Bundle bundle = new Bundle();
                lvSchoolList.setClickable(false);
                bundle.putString("uid", arrSchoolList.get(position).getUid());
                bundle.putString("establishment_type", establishment);
                SchoolProfileFragment fSchoolProfile = new SchoolProfileFragment();
                FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
                fSchoolProfile.setArguments(bundle);
                mFragmentTransaction.add(R.id.flSearchResult, fSchoolProfile, "fragment");
                mFragmentTransaction.addToBackStack("fragment");
                mFragmentTransaction.commit();

                lvSchoolList.setClickable(true);
            }
        }
    }
}