package com.schoolreviewer.home;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;

public class HomeFragment extends BaseFragment {

    FragmentTransaction mFragmentTrasaction;
    FragmentManager mFragmentManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_home, null);
        mFragmentManager = getFragmentManager();
        mFragmentTrasaction = mFragmentManager.beginTransaction();
        SearchFragment searchFragment = new SearchFragment();
        mFragmentTrasaction.replace(R.id.flHomeContent, searchFragment, "fragment").commit();
        return v;
    }
}
