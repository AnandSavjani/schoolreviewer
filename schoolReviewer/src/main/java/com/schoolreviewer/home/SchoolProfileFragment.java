package com.schoolreviewer.home;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.schoolreviewer.BaseFragment;
import com.schoolreviewer.R;
import com.schoolreviewer.adapter.ReviewAdapter;
import com.schoolreviewer.adapter.ViewPageImageAdapter;
import com.schoolreviewer.bean.FEResultBean;
import com.schoolreviewer.bean.KS2ResultBean;
import com.schoolreviewer.bean.KS4ResultBean;
import com.schoolreviewer.bean.KS5ResultBean;
import com.schoolreviewer.bean.ReviewBean;
import com.schoolreviewer.buysell.BuySellListFragment;
import com.schoolreviewer.me.LoginActivity;
import com.schoolreviewer.parentforum.ParentForumListFragment;
import com.schoolreviewer.util.AllAPICall;
import com.schoolreviewer.util.Constant;
import com.schoolreviewer.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class SchoolProfileFragment extends BaseFragment implements OnClickListener {

    private Button btnWriteReview, btnSubscribe, btnAskQuestion, btnPostcode;

    private TextView textHeaderTitle, textSchoolNameForProfile, textSchoolDescription, textType, textNLT, textManagementGroup, textHeadTeacherName,
            textSchoolAddress, textSchoolPhone, textSchoolEmail, textWebsite, textFax, textNoReview, textHeaderReviews, textSpecialNeeds,
            textInspectDate, textOverallEffect, textBehaviour, textAchievement, textQuality, textLeadership, textNoResult, textBoysGirls, textTotalRoll, textPupilTeacher,
            textHeaderEstablishment, textHeaderAcademic, textHeaderMap, textHeaderBuySell, textHeaderParentForum;

    private TextView textSat2Year1, textSat2Year2, textSat2Year3, textSat2Y1L3, textSat2Y2L3, textSat2Y3L3, textSat2Y1L4, textSat2Y2L4, textSat2Y3L4,
            textSat2Y1L4B, textSat2Y2L4B, textSat2Y3L4B, textSat2Y1L5, textSat2Y2L5, textSat2Y3L5, textSat2PtY1, textSat2PtY2, textSat2PtY3,
            textSat2LvY1, textSat2LvY2, textSat2LvY3;

    private TextView textsat4Year1, textsat4Year2, textsat4Year3, textsat4AchY1, textsat4AchY2, textsat4AchY3, textSat4AvgY1, textSat4AvgY2,
            textSat4AvgY3;

    private TextView textAlevelYear1, textAlevelYear2, textAlevelYear3, textALevelY1, textALevelY2, textALevelY3, textAGradeY1, textAGradeY2,
            textAGradeY3;

    private TextView textFurtherYear1, textFurtherYear2, textFurtherYear3, textFurtherY1, textFurtherY2, textFurtherY3, textFScoreY1, textFScoreY2,
            textFScoreY3;

    private AutoScrollViewPager vpImageSlider;
    private ViewPageImageAdapter vpAdapter;
    private LinearLayout llEstablishmentDetail,
            llAcademicResultDetail, llMapDetail, llReviewDetail, llSat2Result, llSat4Result, llAlevelResult, llOfsted, llFutherResult;

    private RatingBar rbSchoolRating, rbEstablishRating;
    private SharedPreferences preferenceLogin;

    private HashMap<String, String> stringHashMap;
    private ArrayList<KS2ResultBean> arrListKS2;
    private ArrayList<KS4ResultBean> arrListKS4;
    private ArrayList<KS5ResultBean> arrListKS5;
    private ArrayList<FEResultBean> arrListFE;

    private List<LatLng> listLatLng;

    private MapFragment googleMap;
    private GoogleMap mMap;

    private static View view;
    private ArrayList<ReviewBean> arrReviewList;

    private ReviewAdapter reviewAdapter;
    private ListView lvReviews;
    private ArrayList<String> arrImageUrl;
    private GestureDetector tapGestureDetector;

    private boolean isEstablishmentDown = false;
    private boolean isAcademicDown = false;
    private boolean isMapDown = false;
    private boolean isReviewsDown = false;
    private String uid, schoolName, url, email;
    private double latitude, longitude;
    private String establishmentType, reporturl;
    private ScrollView svSchoolProfile;

    String score, address1, address2, address3, phone, type, notes, inspectionbody, ratioboystogirls,
            noroll, pupilteacherratio;
    String qualityofteaching, ofstedOverAll, achievementofpupils, behaviourandsafetyofpupils,
            leadershipandmanagement, dateofLastInspection;

    int totalKS2, totalKS4, totalKS5, totalFE, totalReview = 0;
    StringBuilder strAddress;

    private ImageView imgOfstedUrl, imgISIReportUrl, imgFilter;
    private HeatmapTileProvider mProvider;
    private TileOverlay mOverlay;

    private EditText editPostcode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = View.inflate(getActivity(), R.layout.fragment_school_profile, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initControl(view);
        Constant.flag = 0;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.requestFocus();
        view.setFocusableInTouchMode(true);
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    Constant.flag = 1;
                    Fragment fragment = getFragmentManager().findFragmentById(R.id.flSearchResult);
                    if (fragment instanceof SearchResultFragment)
                        fragment.onResume();
                }
                return false;
            }
        });
    }

    private void initControl(View view) {
        preferenceLogin = getActivity().getSharedPreferences(Constant.LOGIN_PREFERENCE, Context.MODE_PRIVATE);
        arrReviewList = new ArrayList<>();

        reviewAdapter = new ReviewAdapter(getActivity(), arrReviewList);
        svSchoolProfile = (ScrollView) view.findViewById(R.id.svSchoolProfile);
        rbEstablishRating = (RatingBar) view.findViewById(R.id.rbEstablishRating);
        rbSchoolRating = (RatingBar) view.findViewById(R.id.rbSchoolRating);

        LayerDrawable stars = (LayerDrawable) rbEstablishRating.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.color_yellow), Mode.SRC_ATOP);

        LayerDrawable schoolStars = (LayerDrawable) rbSchoolRating.getProgressDrawable();
        schoolStars.getDrawable(2).setColorFilter(getResources().getColor(R.color.color_yellow), Mode.SRC_ATOP);

        lvReviews = (ListView) view.findViewById(R.id.lvReviews);
        lvReviews.setOnItemClickListener(onItemClickListener1);

        textHeaderTitle = (TextView) getActivity().findViewById(R.id.textHeaderTitle);
        textHeaderTitle.setText(getResources().getString(R.string.txt_school_profile));

        textSchoolNameForProfile = (TextView) view.findViewById(R.id.textSchoolNameForProfile);
        textHeaderEstablishment = (TextView) view.findViewById(R.id.textHeaderEstablishment);
        textHeaderAcademic = (TextView) view.findViewById(R.id.textHeaderAcademic);
        textHeaderMap = (TextView) view.findViewById(R.id.textHeaderMap);
        textHeaderBuySell = (TextView) view.findViewById(R.id.textHeaderBuySell);
        textHeaderParentForum = (TextView) view.findViewById(R.id.textHeaderParentForum);

        textSchoolDescription = (TextView) view.findViewById(R.id.textSchoolDescription);
        textType = (TextView) view.findViewById(R.id.textType);
        textNLT = (TextView) view.findViewById(R.id.textNLT);
        textManagementGroup = (TextView) view.findViewById(R.id.textManagementGroup);
        textHeadTeacherName = (TextView) view.findViewById(R.id.textHeadTeacherName);
        textSchoolAddress = (TextView) view.findViewById(R.id.textSchoolAddress);
        textSchoolPhone = (TextView) view.findViewById(R.id.textSchoolPhone);
        textSchoolEmail = (TextView) view.findViewById(R.id.textSchoolEmail);
        textFax = (TextView) view.findViewById(R.id.textFax);
        textWebsite = (TextView) view.findViewById(R.id.textWebsite);
        textNoReview = (TextView) view.findViewById(R.id.textNoReview);
        textHeaderReviews = (TextView) view.findViewById(R.id.textHeaderReviews);
        textSpecialNeeds = (TextView) view.findViewById(R.id.textSpecialNeeds);
        textNoResult = (TextView) view.findViewById(R.id.textNoResult);

        textNoResult.setVisibility(View.GONE);
        textSchoolPhone.setVisibility(View.GONE);
        textFax.setVisibility(View.GONE);
        textSchoolEmail.setVisibility(View.GONE);
        textWebsite.setVisibility(View.GONE);
        textNLT.setVisibility(View.GONE);
        textHeadTeacherName.setVisibility(View.GONE);
        textType.setVisibility(View.GONE);

        textInspectDate = (TextView) view.findViewById(R.id.textInspectDate);
        textOverallEffect = (TextView) view.findViewById(R.id.textOverallEffect);
        textBehaviour = (TextView) view.findViewById(R.id.textBehaviour);
        textAchievement = (TextView) view.findViewById(R.id.textAchievement);
        textQuality = (TextView) view.findViewById(R.id.textQuality);
        textLeadership = (TextView) view.findViewById(R.id.textLeadership);

        textSat2Year1 = (TextView) view.findViewById(R.id.textSat2Year1);
        textSat2Year2 = (TextView) view.findViewById(R.id.textSat2Year2);
        textSat2Year3 = (TextView) view.findViewById(R.id.textSat2Year3);
        textSat2Y1L3 = (TextView) view.findViewById(R.id.textSat2Y1L3);
        textSat2Y2L3 = (TextView) view.findViewById(R.id.textSat2Y2L3);
        textSat2Y3L3 = (TextView) view.findViewById(R.id.textSat2Y3L3);
        textSat2Y1L4 = (TextView) view.findViewById(R.id.textSat2Y1L4);
        textSat2Y2L4 = (TextView) view.findViewById(R.id.textSat2Y2L4);
        textSat2Y3L4 = (TextView) view.findViewById(R.id.textSat2Y3L4);
        textSat2Y1L4B = (TextView) view.findViewById(R.id.textSat2Y1L4B);
        textSat2Y2L4B = (TextView) view.findViewById(R.id.textSat2Y2L4B);
        textSat2Y3L4B = (TextView) view.findViewById(R.id.textSat2Y3L4B);
        textSat2Y1L5 = (TextView) view.findViewById(R.id.textSat2Y1L5);
        textSat2Y2L5 = (TextView) view.findViewById(R.id.textSat2Y2L5);
        textSat2Y3L5 = (TextView) view.findViewById(R.id.textSat2Y3L5);
        textSat2PtY1 = (TextView) view.findViewById(R.id.textSat2PtY1);
        textSat2PtY2 = (TextView) view.findViewById(R.id.textSat2PtY2);
        textSat2PtY3 = (TextView) view.findViewById(R.id.textSat2PtY3);
        textSat2LvY1 = (TextView) view.findViewById(R.id.textSat2LvY1);
        textSat2LvY2 = (TextView) view.findViewById(R.id.textSat2LvY2);
        textSat2LvY3 = (TextView) view.findViewById(R.id.textSat2LvY3);

        textsat4Year1 = (TextView) view.findViewById(R.id.textsat4Year1);
        textsat4Year2 = (TextView) view.findViewById(R.id.textsat4Year2);
        textsat4Year3 = (TextView) view.findViewById(R.id.textsat4Year3);
        textsat4AchY1 = (TextView) view.findViewById(R.id.textsat4AchY1);
        textsat4AchY2 = (TextView) view.findViewById(R.id.textsat4AchY2);
        textsat4AchY3 = (TextView) view.findViewById(R.id.textsat4AchY3);
        textSat4AvgY1 = (TextView) view.findViewById(R.id.textSat4AvgY1);
        textSat4AvgY2 = (TextView) view.findViewById(R.id.textSat4AvgY2);
        textSat4AvgY3 = (TextView) view.findViewById(R.id.textSat4AvgY3);

        textAlevelYear1 = (TextView) view.findViewById(R.id.textAlevelYear1);
        textAlevelYear2 = (TextView) view.findViewById(R.id.textAlevelYear2);
        textAlevelYear3 = (TextView) view.findViewById(R.id.textAlevelYear3);
        textALevelY1 = (TextView) view.findViewById(R.id.textALevelY1);
        textALevelY2 = (TextView) view.findViewById(R.id.textALevelY2);
        textALevelY3 = (TextView) view.findViewById(R.id.textALevelY3);
        textAGradeY1 = (TextView) view.findViewById(R.id.textAGradeY1);
        textAGradeY2 = (TextView) view.findViewById(R.id.textAGradeY2);
        textAGradeY3 = (TextView) view.findViewById(R.id.textAGradeY3);

        textFurtherYear1 = (TextView) view.findViewById(R.id.textFurtherYear1);
        textFurtherYear2 = (TextView) view.findViewById(R.id.textFurtherYear2);
        textFurtherYear3 = (TextView) view.findViewById(R.id.textFurtherYear3);
        textFurtherY1 = (TextView) view.findViewById(R.id.textFurtherY1);
        textFurtherY2 = (TextView) view.findViewById(R.id.textFurtherY2);
        textFurtherY3 = (TextView) view.findViewById(R.id.textFurtherY3);
        textFScoreY1 = (TextView) view.findViewById(R.id.textFScoreY1);
        textFScoreY2 = (TextView) view.findViewById(R.id.textFScoreY2);
        textFScoreY3 = (TextView) view.findViewById(R.id.textFScoreY3);

        textBoysGirls = (TextView) view.findViewById(R.id.textBoysGirls);
        textTotalRoll = (TextView) view.findViewById(R.id.textTotalRoll);
        textPupilTeacher = (TextView) view.findViewById(R.id.textRatio);

        imgOfstedUrl = (ImageView) view.findViewById(R.id.imgOfstedUrl);
        imgISIReportUrl = (ImageView) view.findViewById(R.id.imgISIReportUrl);

        imgFilter = (ImageView) getActivity().findViewById(R.id.imgFilter);
        imgFilter.setVisibility(View.INVISIBLE);

        textSchoolPhone.setOnClickListener(this);
        textWebsite.setOnClickListener(this);
        textSchoolEmail.setOnClickListener(this);
        imgISIReportUrl.setOnClickListener(this);
        imgOfstedUrl.setOnClickListener(this);

        btnWriteReview = (Button) view.findViewById(R.id.btnWriteReview);
        btnSubscribe = (Button) view.findViewById(R.id.btnSubscribe);
        btnAskQuestion = (Button) view.findViewById(R.id.btnAskQuestion);
        btnPostcode = (Button) view.findViewById(R.id.btnPostcode);

        btnWriteReview.setOnClickListener(this);
        btnSubscribe.setOnClickListener(this);
        btnAskQuestion.setOnClickListener(this);
        btnPostcode.setOnClickListener(this);

        llSat2Result = (LinearLayout) view.findViewById(R.id.llSat2Result);
        llSat4Result = (LinearLayout) view.findViewById(R.id.llSat4Result);
        llAlevelResult = (LinearLayout) view.findViewById(R.id.llAlevelResult);
        llOfsted = (LinearLayout) view.findViewById(R.id.llOfsted);
        llFutherResult = (LinearLayout) view.findViewById(R.id.llFutherResult);

        llOfsted.setVisibility(View.GONE);
        llSat2Result.setVisibility(View.GONE);
        llSat4Result.setVisibility(View.GONE);
        llAlevelResult.setVisibility(View.GONE);
        llFutherResult.setVisibility(View.GONE);

        textHeaderEstablishment.setOnClickListener(this);
        textHeaderAcademic.setOnClickListener(this);
        textHeaderMap.setOnClickListener(this);
        textHeaderReviews.setOnClickListener(this);
        textHeaderBuySell.setOnClickListener(this);
        textHeaderParentForum.setOnClickListener(this);

        llEstablishmentDetail = (LinearLayout) view.findViewById(R.id.llEstablishmentDetail);
        llAcademicResultDetail = (LinearLayout) view.findViewById(R.id.llAcademicResultDetail);
        llMapDetail = (LinearLayout) view.findViewById(R.id.llMapDetail);
        llReviewDetail = (LinearLayout) view.findViewById(R.id.llReviewDetail);

        vpImageSlider = (AutoScrollViewPager) view.findViewById(R.id.vpImageSlider);
        vpImageSlider.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                tapGestureDetector.onTouchEvent(arg1);
                return false;
            }
        });

        editPostcode = (EditText) view.findViewById(R.id.editPostcode);

        tapGestureDetector = new GestureDetector(getActivity(), onGestureListener);
        svSchoolProfile.fullScroll(ScrollView.FOCUS_UP);

        googleMap = ((MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map));
        initializeMap();

        Bundle bundle = getArguments();
        uid = bundle.getString("uid");
        establishmentType = bundle.getString("establishment_type");
        hideEstablishmentDetails();
        hideAcademicDetails();
        hideMap();
        hideReviewsDetails();


    }

    @Override
    public void onResume() {
        super.onResume();
        if (utility.checkInternetConnection()) {
            SchoolProfileAsyncCall();
        }
    }

    /*private void focusOnView(final TextView textView) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                svSchoolProfile.smoothScrollTo(0, textView.getBottom());
            }
        });
    }*/

    @Override
    public void onClick(View mView) {
        Intent intentOfstedReport = new Intent(Intent.ACTION_VIEW);
        intentOfstedReport.setData(Uri.parse(reporturl));
        Bundle bundle;
        FragmentManager mFragmentManager;
        FragmentTransaction mFragmentTrasaction;

        switch (mView.getId()) {
            case R.id.llMapDetail:
                view.getParent().requestDisallowInterceptTouchEvent(true);
                break;
            case R.id.textHeaderEstablishment:
                if (isEstablishmentDown) {
                    hideEstablishmentDetails();
                } else {
                    showEstablishmentDetails();
                    hideAcademicDetails();
                    hideMap();
                    hideReviewsDetails();
                }
                break;

            case R.id.textHeaderAcademic:
                if (isAcademicDown) {
                    hideAcademicDetails();
                } else {
                    hideEstablishmentDetails();
                    showAcademicDetails();
                    hideMap();
                    hideReviewsDetails();
                }
                break;

            case R.id.textHeaderMap:
                if (isMapDown) {
                    hideMap();
                } else {
                    hideEstablishmentDetails();
                    hideAcademicDetails();
                    showMap();
                    hideReviewsDetails();
                }
                break;
            case R.id.textHeaderReviews:
                if (isReviewsDown) {
                    hideReviewsDetails();
                } else {
                    hideEstablishmentDetails();
                    hideAcademicDetails();
                    hideMap();
                    showReviewsDetails();
                }
                break;

            case R.id.textHeaderParentForum:
                bundle = new Bundle();
                bundle.putString("uid", uid);

                mFragmentManager = getFragmentManager();
                mFragmentTrasaction = mFragmentManager.beginTransaction();
                ParentForumListFragment parentForumListFragment = new ParentForumListFragment();
                parentForumListFragment.setArguments(bundle);
                mFragmentTrasaction.addToBackStack(null);
                mFragmentTrasaction.replace(R.id.flSchoolProfile, parentForumListFragment, "fragment").commit();
                break;

            case R.id.textHeaderBuySell:
                bundle = new Bundle();
                bundle.putString("uid", uid);
                mFragmentManager = getFragmentManager();
                mFragmentTrasaction = mFragmentManager.beginTransaction();
                BuySellListFragment buySellListFragment = new BuySellListFragment();
                buySellListFragment.setArguments(bundle);
                mFragmentTrasaction.addToBackStack("buy");
                mFragmentTrasaction.replace(R.id.flSchoolProfile, buySellListFragment, "result").commit();
                break;

            case R.id.btnWriteReview:
                Bundle mBundle = new Bundle();
                mBundle.putString("uid", uid);
                Intent inWriteReview = new Intent(getActivity(), WriteReviewFragmentActivity.class);
                inWriteReview.putExtras(mBundle);
                startActivity(inWriteReview);
                break;

            case R.id.btnSubscribe:
                if (utility.checkInternetConnection())
                    SubscribeAsyncCall();
                break;

            case R.id.btnAskQuestion:
                if (utility.checkInternetConnection()) {
                    redirectToAskQuestion();
                }
                break;
            case R.id.textSchoolPhone:
                Intent intentCall = new Intent(Intent.ACTION_CALL);
                intentCall.setData(Uri.parse("tel:" + textSchoolPhone.getText().toString()));
                startActivity(intentCall);
                break;

            case R.id.textWebsite:
                Intent intentSite = new Intent(Intent.ACTION_VIEW);
                intentSite.setData(Uri.parse(url));
                startActivity(intentSite);
                break;

            case R.id.textSchoolEmail:
                Intent intentMail = new Intent(Intent.ACTION_SENDTO);
                intentMail.setType("text/plain");
                intentMail.putExtra(Intent.EXTRA_SUBJECT, "");
                intentMail.putExtra(Intent.EXTRA_TEXT, "");
                intentMail.setData(Uri.parse("mailto:" + email.trim()));
                intentMail.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentMail);
                break;
            case R.id.imgOfstedUrl:
                startActivity(intentOfstedReport);
                break;
            case R.id.imgISIReportUrl:
                startActivity(intentOfstedReport);
                break;
            case R.id.btnPostcode:

                if (TextUtils.isEmpty(editPostcode.getText().toString())) {
                    editPostcode.setError("Please enter post code");
                } else {
                    getLocationAPI(editPostcode.getText().toString());
                    utility.hideSoftKeyboard();
                }
                break;
            default:
                break;
        }
    }

    private void redirectToAskQuestion() {
        Intent mIntent = new Intent(getActivity(), AskQuestionActivity.class);
        mIntent.putExtra("uid", uid);
        mIntent.putExtra("question", true);
        startActivity(mIntent);
    }

    private void hideMap() {
        isMapDown = false;
        llMapDetail.setVisibility(View.GONE);
        textHeaderMap.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_arrow, 0, R.drawable.map, 0);
    }

    private void showMap() {

        isMapDown = true;
        llMapDetail.setVisibility(View.VISIBLE);
        if (mMap != null) {
            LatLng latLng = new LatLng(latitude, longitude);
            MarkerOptions marker = new MarkerOptions().position(latLng).title(schoolName);
            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin));

            mMap.addMarker(marker);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 5));

            llMapDetail.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View mView, MotionEvent event) {
                    svSchoolProfile.requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });
            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng arg0) {
                    svSchoolProfile.requestDisallowInterceptTouchEvent(true);
                }
            });
        }

        textHeaderMap.setCompoundDrawablesWithIntrinsicBounds(R.drawable.sell_arrow_down, 0, R.drawable.map, 0);
    }

    private void showReviewsDetails() {
        isReviewsDown = true;
        reviewAdapter.notifyDataSetChanged();
        llReviewDetail.setVisibility(View.VISIBLE);
        textHeaderReviews.setCompoundDrawablesWithIntrinsicBounds(R.drawable.sell_arrow_down, 0, R.drawable.reviews, 0);
    }

    private void hideReviewsDetails() {
        isReviewsDown = false;
        llReviewDetail.setVisibility(View.GONE);
        textHeaderReviews.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_arrow, 0, R.drawable.reviews, 0);
    }

    private void showEstablishmentDetails() {
        isEstablishmentDown = true;
        llEstablishmentDetail.setVisibility(View.VISIBLE);
        textHeaderEstablishment.setCompoundDrawablesWithIntrinsicBounds(R.drawable.sell_arrow_down, 0, R.drawable.establishment, 0);
    }

    private void hideEstablishmentDetails() {
        isEstablishmentDown = false;
        llEstablishmentDetail.setVisibility(View.GONE);
        textHeaderEstablishment.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_arrow, 0, R.drawable.establishment, 0);
    }

    private void hideAcademicDetails() {
        isAcademicDown = false;
        llAcademicResultDetail.setVisibility(View.GONE);
        textHeaderAcademic.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_arrow, 0, R.drawable.academic_results, 0);
    }

    private void showAcademicDetails() {
        isAcademicDown = true;
        llAcademicResultDetail.setVisibility(View.VISIBLE);
        textHeaderAcademic.setCompoundDrawablesWithIntrinsicBounds(R.drawable.sell_arrow_down, 0, R.drawable.academic_results, 0);
    }

    void imageSlider(ArrayList<String> ImageUrl) {
        vpAdapter = new ViewPageImageAdapter(getActivity(), ImageUrl);
        vpImageSlider.setInterval(5000);
        vpImageSlider.startAutoScroll();
        vpImageSlider.setCurrentItem(0, true);
        vpImageSlider.setCycle(true);
        vpImageSlider.setDirection(AutoScrollViewPager.RIGHT);
        vpImageSlider.setAutoScrollDurationFactor(3);
        vpImageSlider.setAdapter(vpAdapter);
    }


    private void SchoolProfileAsyncCall() {

        strAddress = new StringBuilder();
        stringHashMap = new HashMap<>();
        arrImageUrl = new ArrayList<>();
        arrListKS2 = new ArrayList<>();
        listLatLng = new ArrayList<>();

        stringHashMap.put("uid", uid);
        stringHashMap.put("country", Constant.COUNTRY);
        stringHashMap.put("lang", "en");
        stringHashMap.put("key", preferenceLogin.getString("key", ""));

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                JSONObject jObjDetails = null;
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jObjResponse = jsonArray.getJSONObject(0);
                    if (jObjResponse.getString("status").equalsIgnoreCase("ok")) {

                        JSONObject jObjMessage = jObjResponse.getJSONObject("message");
                        jObjDetails = jObjMessage.getJSONObject("details");
                        schoolName = jObjDetails.getString("name").toUpperCase();
                        if (!jObjDetails.getString("group").equals("")) {
                            textManagementGroup.setVisibility(View.VISIBLE);
                            textManagementGroup.setText("MANAGEMENT GROUP : " + jObjDetails.getString("group"));
                        }

                        if (jObjDetails.getInt("ifollow") == 1) {
                            btnSubscribe.setVisibility(View.GONE);
                        } else btnSubscribe.setVisibility(View.VISIBLE);

                        address1 = jObjDetails.getString("address1");
                        address2 = jObjDetails.getString("address2");
                        address3 = jObjDetails.getString("address3");
                        phone = jObjDetails.getString("phone");
                        email = jObjDetails.getString("email");
                        latitude = jObjDetails.getDouble("lat");
                        longitude = jObjDetails.getDouble("long");
                        type = jObjDetails.getString("type");

                        JSONArray arrayHeatMap = jObjDetails.getJSONArray("heatmap");
                        for (int i = 0; i < arrayHeatMap.length(); i++) {
                            LatLng latLng = new LatLng(arrayHeatMap.getJSONObject(i).getDouble("lat"),
                                    arrayHeatMap.getJSONObject(i).getDouble("lng"));
                            listLatLng.add(latLng);
                        }

                        if (mMap != null) {
                            if (!listLatLng.isEmpty()) {
                                addHeatMap();
                                textHeaderMap.setText(R.string.txt_map);
                            } else {
                                textHeaderMap.setText("Map");
                            }
                        }

                        ratioboystogirls = jObjDetails.getString("ratioboystogirls");
                        noroll = jObjDetails.getString("noroll");
                        pupilteacherratio = jObjDetails.getString("pupilteacherratio");
                        reporturl = jObjDetails.getString("reporturl");
                        inspectionbody = jObjDetails.getString("inspectionbody");

                        JSONObject jObjExtra = jObjMessage.getJSONObject("extras");
                        url = jObjExtra.getString("url");
                        notes = jObjExtra.getString("notes").trim();

                        JSONArray jArrayImageUrl = jObjExtra.getJSONArray("imageurl");
                        for (int i = 0; i < jArrayImageUrl.length(); i++) {
                            JSONObject jObjImgUrl = jArrayImageUrl.getJSONObject(i);
                            arrImageUrl.add(Constant.BASE_IMAGE_URL + jObjImgUrl.getString("url"));
                        }

                        score = jObjMessage.getJSONObject("scores").getString("score");

                        JSONObject jObjOfsted = jObjMessage.getJSONObject("ofsted");
                        ofstedOverAll = jObjOfsted.getString("overall");
                        achievementofpupils = jObjOfsted.getString("acheivementofpupils");
                        behaviourandsafetyofpupils = jObjOfsted.getString("behaviourandsafetyofpupils");
                        leadershipandmanagement = jObjOfsted.getString("leadershipandmanagement");
                        qualityofteaching = jObjOfsted.getString("qualityofteaching");
                        dateofLastInspection = jObjOfsted.getString("dateoflastinspection");

                        JSONArray jArrayKS2 = jObjMessage.getJSONArray("ks2results");
                        totalKS2 = jArrayKS2.length();
                        if (totalKS2 != 0) {
                            for (int i = 0; i < totalKS2; i++) {
                                KS2ResultBean ks2Bean = new KS2ResultBean();
                                JSONObject jObjKS2 = jArrayKS2.getJSONObject(i);
                                ks2Bean.setYear(jObjKS2.getString("year"));
                                ks2Bean.setKS2_PTREADWRITTAMATBX(jObjKS2.getString("KS2_PTREADWRITTAMATBX"));
                                ks2Bean.setKS2_PTREADWRITTAMATX(jObjKS2.getString("KS2_PTREADWRITTAMATX"));
                                ks2Bean.setKS2_PTREADWRITTAMAT4B(jObjKS2.getString("KS2_PTREADWRITTAMAT4B"));
                                ks2Bean.setKS2_PTREADWRITTAMATAX(jObjKS2.getString("KS2_PTREADWRITTAMATAX"));
                                ks2Bean.setKS2_TAPS(jObjKS2.getString("KS2_TAPS"));
                                ks2Bean.setKS2_AVGLEVEL(jObjKS2.getString("KS2_AVGLEVEL"));
                                arrListKS2.add(ks2Bean);
                            }
                        }
                        JSONArray jArrayKS4 = jObjMessage.getJSONArray("ks4results");
                        totalKS4 = jArrayKS4.length();
                        if (totalKS4 != 0) {
                            arrListKS4 = new ArrayList<>();
                            for (int i = 0; i < totalKS4; i++) {
                                KS4ResultBean ks4Bean = new KS4ResultBean();
                                JSONObject jObjKS4 = jArrayKS4.getJSONObject(i);
                                ks4Bean.setYear(jObjKS4.getString("year"));
                                ks4Bean.setKS4_PTGAC5EM_PTQ(jObjKS4.getString("KS4_PBAC5EM"));
                                ks4Bean.setKS4_AVGRDPEPPCP_PTQ(jObjKS4.getString("KS4_AVGRDPEPPCP_PTQ"));
                                arrListKS4.add(ks4Bean);
                            }
                        }

                        JSONArray jArrayKS5 = jObjMessage.getJSONArray("ks5results");
                        totalKS5 = jArrayKS5.length();
                        if (totalKS5 != 0) {
                            arrListKS5 = new ArrayList<>();
                            for (int i = 0; i < totalKS5; i++) {
                                KS5ResultBean ks5Bean = new KS5ResultBean();
                                JSONObject jObjKS5 = jArrayKS5.getJSONObject(i);
                                ks5Bean.setYear(jObjKS5.getString("year"));
                                ks5Bean.setKS5_PTPASS3LV3_ALEVCOH(jObjKS5.getString("KS5_PTPASS3LV3_ALEVCOH"));
                                ks5Bean.setKS5_TALLPPEGRD_ALEVA(jObjKS5.getString("KS5_TALLPPEGRD_ALEVA"));
                                arrListKS5.add(ks5Bean);
                            }
                        }

                        JSONArray jArrayFE = jObjMessage.getJSONArray("feresults");
                        totalFE = jArrayFE.length();
                        if (totalFE != 0) {
                            arrListFE = new ArrayList<FEResultBean>();
                            for (int i = 0; i < totalFE; i++) {
                                FEResultBean feBean = new FEResultBean();
                                JSONObject jObjFE = jArrayFE.getJSONObject(i);
                                feBean.setYear(jObjFE.getString("year"));
                                feBean.setFE_RANK(jObjFE.getString("FE_RANK"));
                                feBean.setFE_OVERALL_SCORE(jObjFE.getString("FE_OVERALL_SCORE"));
                                arrListFE.add(feBean);
                            }

                            textPupilTeacher.setVisibility(View.GONE);
                            textTotalRoll.setVisibility(View.GONE);
                        }

                        JSONArray jArrayReview = jObjMessage.getJSONArray("reviews");
                        totalReview = jArrayReview.length();
                        arrReviewList.clear();
                        if (totalReview > 0) {

                            for (int i = 0; i < jArrayReview.length(); i++) {
                                JSONObject jObjReview = jArrayReview.getJSONObject(i);
                                ReviewBean rBean = new ReviewBean();
                                rBean.setUid(jObjReview.getString("uid"));
                                rBean.setUidreviewer(jObjReview.getString("uidreviewer"));
                                String reviewTitle = Html.fromHtml(jObjReview.getString("title")).toString();
                                rBean.setTitle(reviewTitle);
                                rBean.setDisplayname(utility.checkDisplayName(jObjReview.getString("displayname")));
                                rBean.setReviewedat(jObjReview.getString("reviewedat"));
                                float fScore = Float.parseFloat(jObjReview.getString("score"));
                                rBean.setScoreOfReview(fScore);
                                rBean.setRelationship(jObjReview.getString("relationship"));

                                rBean.setFurther(jObjReview.getString("further"));
                                rBean.setReviewyear(jObjReview.getString("reviewyear"));
                                rBean.setLoH(jObjReview.getString("LoH"));
                                rBean.setQoT(jObjReview.getString("QoT"));
                                rBean.setAoS(jObjReview.getString("AoS"));
                                rBean.setSaS(jObjReview.getString("SaS"));
                                rBean.setSports(jObjReview.getString("sports"));
                                rBean.setMusic(jObjReview.getString("music"));
                                rBean.setFood(jObjReview.getString("food"));
                                rBean.setReply(jObjReview.getString("reply"));
                                rBean.setFromFragment("school");
                                arrReviewList.add(rBean);
                            }
                        }
                    }
                    textSchoolNameForProfile.setText(schoolName);

                    if (jObjDetails.getString("specialneeds").equals("yes"))
                        textSpecialNeeds.setVisibility(View.VISIBLE);
                    else
                        textSpecialNeeds.setVisibility(View.GONE);

                    if (!reporturl.isEmpty() || !inspectionbody.isEmpty()) {
                        switch (inspectionbody) {
                            case "isi":
                                imgISIReportUrl.setVisibility(View.VISIBLE);
                                break;
                            case "ofsted":
                                imgOfstedUrl.setVisibility(View.VISIBLE);
                                break;
                            case "sis":
                                imgISIReportUrl.setVisibility(View.VISIBLE);
                                break;
                            default:
                                imgISIReportUrl.setVisibility(View.VISIBLE);
                                break;
                        }
                    } else {
                        imgOfstedUrl.setVisibility(View.GONE);
                        imgISIReportUrl.setVisibility(View.GONE);
                    }
                    if (totalReview >= 0)
                        textHeaderReviews.setText("Reviews" + " (" + totalReview + ")");

                    if (!address1.equals(""))
                        strAddress.append(address1).append(", ");
                    if (!address2.equals(""))
                        strAddress.append(address2).append(", ");
                    if (!address3.equals(""))
                        strAddress.append(address3).append(", ");

                    if (!jObjDetails.getString("town").equals(""))
                        strAddress.append(jObjDetails.getString("town")).append(", ");
                    if (!jObjDetails.getString("county").equals(""))
                        strAddress.append(jObjDetails.getString("county")).append(", ");
                    if (!jObjDetails.getString("postcode").equals(""))
                        strAddress.append(jObjDetails.getString("postcode")).append(".");

                    textSchoolAddress.setText(strAddress);

                    textHeadTeacherName.setText(jObjDetails.getString("headtitle").toUpperCase() + ": " + jObjDetails.getString("headmrmrs") + " " + jObjDetails.getString("headfirstname") + " " + jObjDetails.getString("headsurname"));
                    if (!jObjDetails.getString("headfirstname").equals("") || !jObjDetails.getString("headsurname").equals("")) {
                        textHeadTeacherName.setVisibility(View.VISIBLE);
                    }
                    if (!ratioboystogirls.equals("")) {
                        textBoysGirls.setText("BOYS / GIRLS : " + ratioboystogirls);
                        textBoysGirls.setVisibility(View.VISIBLE);
                    }
                    if (!noroll.equals("0") && totalFE != 0) {
                        textTotalRoll.setText("TOTAL ON ROLL : " + noroll);
                        textTotalRoll.setVisibility(View.VISIBLE);
                    }
                    if (!pupilteacherratio.equals("")) {
                        textPupilTeacher.setText("PUPIL / TEACHER RATIO : " + pupilteacherratio + " : 1");
                        textPupilTeacher.setVisibility(View.VISIBLE);
                    }

                    if (!phone.equals("")) {
                        textSchoolPhone.setVisibility(View.VISIBLE);
                        textSchoolPhone.setText(phone);
                    }

                    if (!jObjDetails.getString("fax").equals("")) {
                        textFax.setVisibility(View.VISIBLE);
                        textFax.setText(jObjDetails.getString("fax"));
                    }
                    if (!email.equals("")) {
                        textSchoolEmail.setVisibility(View.VISIBLE);
                        textSchoolEmail.setText("Send Email");
                    }

                    if (!type.equals("")) {
                        textType.setVisibility(View.VISIBLE);
                        textType.setText("TYPE: " + type);
                    }
                    if (!ofstedOverAll.equals("0")) {
                        textNLT.setText("OFSTED: " + checkOfsted(ofstedOverAll));
                        textNLT.setVisibility(View.VISIBLE);
                    }
                    if (!url.equals("")) {
                        textWebsite.setText("Visit Website");
                        textWebsite.setVisibility(View.VISIBLE);
                    }

                    textSchoolDescription.setText(notes);
                    float fScore = Float.parseFloat(score);

                    TextView lblRating = (TextView) getActivity().findViewById(R.id.lblRating);
                    if (fScore == 0) {
                        rbEstablishRating.setVisibility(View.GONE);
                        rbSchoolRating.setVisibility(View.GONE);

                        lblRating.setText("RATING: - ");
                    } else {
                        rbEstablishRating.setRating(fScore);
                        rbSchoolRating.setRating(fScore);

                        rbSchoolRating.setVisibility(View.VISIBLE);
                        rbEstablishRating.setVisibility(View.VISIBLE);
                        lblRating.setText("RATING:");
                    }

                    if (totalReview <= 0) {
                        textNoReview.setVisibility(View.VISIBLE);
                        lvReviews.setVisibility(View.GONE);
                    } else {
                        lvReviews.setAdapter(reviewAdapter);
                        reviewAdapter.notifyDataSetChanged();
                        setListViewHeightBasedOnChildren(lvReviews);
                        textNoReview.setVisibility(View.GONE);
                        lvReviews.setVisibility(View.VISIBLE);
                    }
                    if (getActivity() != null) {
                        imageSlider(arrImageUrl);
                    }
                    if (!dateofLastInspection.equals("")) {
                        llOfsted.setVisibility(View.VISIBLE);
                        textInspectDate.setText(utility.parseDate(dateofLastInspection));
                        textOverallEffect.setText(checkOfsted(ofstedOverAll));
                        textBehaviour.setText(checkOfsted(behaviourandsafetyofpupils));
                        textAchievement.setText(checkOfsted(achievementofpupils));
                        textQuality.setText(checkOfsted(qualityofteaching));
                        textLeadership.setText(checkOfsted(leadershipandmanagement));
                    }
                    if (totalKS2 != 0 && (establishmentType.equals("primary") || establishmentType.equals("all"))) {
                        llSat2Result.setVisibility(View.VISIBLE);
                        textSat2Year1.setText(arrListKS2.get(2).getYear());
                        textSat2Year2.setText(arrListKS2.get(1).getYear());
                        textSat2Year3.setText(arrListKS2.get(0).getYear());
                        textSat2Y1L3.setText(arrListKS2.get(2).getKS2_PTREADWRITTAMATBX());
                        textSat2Y2L3.setText(arrListKS2.get(1).getKS2_PTREADWRITTAMATBX());
                        textSat2Y3L3.setText(arrListKS2.get(0).getKS2_PTREADWRITTAMATBX());
                        textSat2Y1L4.setText(arrListKS2.get(2).getKS2_PTREADWRITTAMATX());
                        textSat2Y2L4.setText(arrListKS2.get(1).getKS2_PTREADWRITTAMATX());
                        textSat2Y3L4.setText(arrListKS2.get(0).getKS2_PTREADWRITTAMATX());
                        textSat2Y1L4B.setText(arrListKS2.get(2).getKS2_PTREADWRITTAMAT4B());
                        textSat2Y2L4B.setText(arrListKS2.get(1).getKS2_PTREADWRITTAMAT4B());
                        textSat2Y3L4B.setText(arrListKS2.get(0).getKS2_PTREADWRITTAMAT4B());
                        textSat2Y1L5.setText(arrListKS2.get(2).getKS2_PTREADWRITTAMATAX());
                        textSat2Y2L5.setText(arrListKS2.get(1).getKS2_PTREADWRITTAMATAX());
                        textSat2Y3L5.setText(arrListKS2.get(0).getKS2_PTREADWRITTAMATAX());
                        textSat2PtY1.setText(arrListKS2.get(2).getKS2_TAPS());
                        textSat2PtY2.setText(arrListKS2.get(1).getKS2_TAPS());
                        textSat2PtY3.setText(arrListKS2.get(0).getKS2_TAPS());
                        textSat2LvY1.setText(arrListKS2.get(2).getKS2_AVGLEVEL());
                        textSat2LvY2.setText(arrListKS2.get(1).getKS2_AVGLEVEL());
                        textSat2LvY3.setText(arrListKS2.get(0).getKS2_AVGLEVEL());
                    }
                    if (totalKS4 != 0 && (establishmentType.equals("secondary") || establishmentType.equals("all"))) {
                        llSat4Result.setVisibility(View.VISIBLE);
                        textsat4Year1.setText(arrListKS4.get(2).getYear());
                        textsat4Year2.setText(arrListKS4.get(1).getYear());
                        textsat4Year3.setText(arrListKS4.get(0).getYear());
                        textsat4AchY1.setText(arrListKS4.get(2).getKS4_PTGAC5EM_PTQ());
                        textsat4AchY2.setText(arrListKS4.get(1).getKS4_PTGAC5EM_PTQ());
                        textsat4AchY3.setText(arrListKS4.get(0).getKS4_PTGAC5EM_PTQ());
                        textSat4AvgY1.setText(arrListKS4.get(2).getKS4_AVGRDPEPPCP_PTQ());
                        textSat4AvgY2.setText(arrListKS4.get(1).getKS4_AVGRDPEPPCP_PTQ());
                        textSat4AvgY3.setText(arrListKS4.get(0).getKS4_AVGRDPEPPCP_PTQ());

                    }
                    if (totalKS5 != 0 && (establishmentType.equals("secondary") || establishmentType.equals("all"))) {
                        llAlevelResult.setVisibility(View.VISIBLE);
                        textAlevelYear1.setText(arrListKS5.get(2).getYear());
                        textAlevelYear2.setText(arrListKS5.get(1).getYear());
                        textAlevelYear3.setText(arrListKS5.get(0).getYear());
                        textALevelY1.setText(arrListKS5.get(2).getKS5_PTPASS3LV3_ALEVCOH());
                        textALevelY2.setText(arrListKS5.get(1).getKS5_PTPASS3LV3_ALEVCOH());
                        textALevelY3.setText(arrListKS5.get(0).getKS5_PTPASS3LV3_ALEVCOH());
                        textAGradeY1.setText(arrListKS5.get(2).getKS5_TALLPPEGRD_ALEVA());
                        textAGradeY2.setText(arrListKS5.get(1).getKS5_TALLPPEGRD_ALEVA());
                        textAGradeY3.setText(arrListKS5.get(0).getKS5_TALLPPEGRD_ALEVA());
                    }
                    if (totalFE != 0 && (establishmentType.equals("further") || establishmentType.equals("all"))) {
                        llFutherResult.setVisibility(View.VISIBLE);
                        textFurtherYear1.setText(arrListFE.get(2).getYear());
                        textFurtherYear2.setText(arrListFE.get(1).getYear());
                        textFurtherYear3.setText(arrListFE.get(0).getYear());
                        textFurtherY1.setText(arrListFE.get(2).getFE_RANK());
                        textFurtherY2.setText(arrListFE.get(1).getFE_RANK());
                        textFurtherY3.setText(arrListFE.get(0).getFE_RANK());
                        textFScoreY1.setText(arrListFE.get(2).getFE_OVERALL_SCORE());
                        textFScoreY2.setText(arrListFE.get(1).getFE_OVERALL_SCORE());
                        textFScoreY3.setText(arrListFE.get(0).getFE_OVERALL_SCORE());
                    }
                    if (dateofLastInspection.equals("") && totalKS2 == 0 && totalKS4 == 0 && totalKS5 == 0 && totalFE == 0)
                        textNoResult.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_SCHOOL_DETAILS);
    }

    private void redirectToLogin() {
        Intent inSubscribe = new Intent(getActivity(), LoginActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("header", textHeaderTitle.getText().toString());
        inSubscribe.putExtras(bundle);
        startActivityForResult(inSubscribe, 1);
    }

    private void SubscribeAsyncCall() {
        stringHashMap = new HashMap<>();

        stringHashMap.put("key", preferenceLogin.getString("key", ""));
        stringHashMap.put("uid", uid);
        stringHashMap.put("country", Constant.COUNTRY);

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("code") == -16) {
                        Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    } else if (jsonObject.getInt("code") == 16) {
                        utility.showMessageDialog(jsonObject.getString("message"));
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.txt_msg_login), Toast.LENGTH_SHORT).show();
                        redirectToLogin();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_SUBSCRIBE_TO_SCHOOL);
    }

    private void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0) {
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));
            }
            view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public String checkOfsted(String details) {
        switch (details) {
            case "1":
                return "Inadequate";
            case "2":
                return "Requires improvement";
            case "3":
                return "Good";
            case "4":
                return "Outstanding";
            default:
                return "N/A";
        }
    }

    private void initializeMap() {
        googleMap.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
            }
        });
    }

    GestureDetector.SimpleOnGestureListener onGestureListener = new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            Bundle b = new Bundle();
            Intent inImageView = new Intent(getActivity(), ImageViewActivity.class);
            b.putString("imagepath", arrImageUrl.get(vpImageSlider.getCurrentItem()));
            inImageView.putExtras(b);
            startActivity(inImageView);
            return super.onSingleTapConfirmed(e);
        }
    };

    OnItemClickListener onItemClickListener1 = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
            UserReviewFragment fUserReview = new UserReviewFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("review", arrReviewList.get(position));
            bundle.putString("schooluid", uid);
            FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
            mFragmentTransaction.replace(R.id.flSchoolProfile, fUserReview);
            fUserReview.setArguments(bundle);
            mFragmentTransaction.addToBackStack("review").commit();
        }
    };

    private void addHeatMap() {

        // Create a heat map tile provider, passing it the latlngs of the police stations.
        mProvider = new HeatmapTileProvider.Builder()
                .data(listLatLng)
                .build();

        // Add a tile overlay to the map, using the heat map tile provider.

        mOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
        mOverlay.clearTileCache();
    }

    private void getLocationAPI(String pincode) {
        utility.showProgress();
        String url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + pincode.replace(" ", "+") + "&key=AIzaSyCEZnSq-o_Z8a0yCI-6muzhNUQCj-MDKek&sensor=true";

        RequestQueue mVolleyQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                utility.hideProgress();
//                Log.i("response ", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject objResult = jsonObject.getJSONArray("results").getJSONObject(0);
                    JSONObject objLocation = objResult.getJSONObject("geometry").getJSONObject("location");

                    Log.i("latitude", "" + objLocation.getDouble("lat"));
                    Log.i("latitude", "" + objLocation.getDouble("lng"));

                    if (mMap != null) {
                        showMap();

                        LatLng latLng = new LatLng(objLocation.getDouble("lat"), objLocation.getDouble("lng"));
                        MarkerOptions marker = new MarkerOptions().position(latLng);
                        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin));

                        mMap.addMarker(marker);

                        llMapDetail.setOnTouchListener(new OnTouchListener() {
                            @Override
                            public boolean onTouch(View mView, MotionEvent event) {
                                svSchoolProfile.requestDisallowInterceptTouchEvent(true);
                                return false;
                            }
                        });

                        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng arg0) {
                                svSchoolProfile.requestDisallowInterceptTouchEvent(true);
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                utility.hideProgress();
                error.printStackTrace();
            }
        });

        stringRequest.setShouldCache(false);
        stringRequest.setTag("Volley");
        mVolleyQueue.add(stringRequest);
    }
}