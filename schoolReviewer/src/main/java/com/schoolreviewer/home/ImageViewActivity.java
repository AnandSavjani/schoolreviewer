package com.schoolreviewer.home;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.schoolreviewer.R;

public class ImageViewActivity extends Activity {

    private Intent mIntent;
    private Bundle mBundle;
    private ImageView imgSchoolImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics metrics = getApplicationContext().getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels / 2;

        this.requestWindowFeature(Window.FEATURE_ACTION_BAR);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        LayoutParams params = this.getWindow().getAttributes();
        params.alpha = 1.0f;
        params.dimAmount = 0.5f;
        this.getWindow().setAttributes(params);
        this.getWindow().setLayout(width, height);

        setContentView(R.layout.image_viewer);
        mIntent = getIntent();
        mBundle = mIntent.getExtras();
        mBundle.getString("imagepath");

        imgSchoolImage = (ImageView) findViewById(R.id.imgSchoolImage);
        Glide.with(this).load(Uri.parse(mBundle.getString("imagepath"))).centerCrop().priority(Priority.HIGH).into(imgSchoolImage);
    }
}
